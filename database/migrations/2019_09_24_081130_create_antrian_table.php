<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntrianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helfa_antrian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('number');
            $table->string('hospital_code', 100)->nullable();
            $table->integer('polyclinic_id')->nullable();
            $table->integer('doctor_id')->nullable();
            $table->string('start_hour', 10)->nullable();
            $table->string('end_hour', 10)->nullable();
            $table->dateTime('created_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('antrian');
    }
}
