<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntrianRuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helfa_antrian_skema', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->integer('polyclinic_id')->nullable();
            $table->string('bpjs_type',125)->nullable();
            $table->string('letter',10)->nullable();
            $table->integer('numbering')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
