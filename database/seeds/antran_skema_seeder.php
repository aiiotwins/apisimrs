<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class antran_skema_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('helfa_antrian_skema')->insert([
            ['polyclinic_id' => 4, 'bpjs_type' => 'mandiri', 'letter' => 'A', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 4, 'bpjs_type' => 'dinas', 'letter' => '1', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 8, 'bpjs_type' => 'dinas', 'letter' => '5', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 8, 'bpjs_type' => 'mandiri', 'letter' => 'E', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 6, 'bpjs_type' => 'dinas', 'letter' => '8', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 6, 'bpjs_type' => 'mandiri', 'letter' => 'H', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 31, 'bpjs_type' => 'dinas', 'letter' => 'F', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 31, 'bpjs_type' => 'mandiri', 'letter' => '6', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 5, 'bpjs_type' => 'dinas', 'letter' => '2', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 5, 'bpjs_type' => 'mandiri', 'letter' => 'B', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 7, 'bpjs_type' => 'dinas', 'letter' => '3', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 7, 'bpjs_type' => 'mandiri', 'letter' => 'C', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 25, 'bpjs_type' => 'dinas', 'letter' => '4', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 25, 'bpjs_type' => 'mandiri', 'letter' => 'D', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 34, 'bpjs_type' => 'dinas', 'letter' => '7', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ['polyclinic_id' => 34, 'bpjs_type' => 'mandiri', 'letter' => 'G', 'numbering' => 3, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
        ]);
    }
}
