<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Rinap extends Model {

    protected $table = 'register_rinap';

    protected $fillable = [];

    protected $hidden = [];

    public static function detail($noreg){

    	$select  = 'a.id_pasien,';
		$select .= 'a.norm as no_rekam_medik,';
		$select .= 'e.NAMA AS nama_pasien,';
		$select .= 'e.TGLAHIR AS tanggal_lahir,';
		$select .= 'if(e.KELAMIN = \'L\', \'LAKI-LAKI\', \'PEREMPUAN\') AS jns_kelamin,';
        $select .= 'YEAR(CURDATE()) - YEAR(e.TGLAHIR) as umur,';
		$select .= 'a.noreg as no_pendaftaran,';
		$select .= 'a.tglReg as tanggal_pendaftaran,';
        $select .= '"0" as kelaspelayanan_id,';
        $select .= '"NON KELAS" as kelaspelayanan_nama,';
		$select .= 'a.jns_pembayaran as carabayar_id,';
		$select .= 'g.golpasName as carabayar_nama,';
        $select .= 'e.GOLPASKD as penjamin_id,';
        $select .= 'r.golpasName as penjamin_nama,';
        $select .= '"" as jeniskasuspenyakit_nama,';
		$select .= 'b.ruanganID as ruangan_id,';
		$select .= 'CONCAT(d.ruanganName, \' - \', f.KET) as ruangan_nama,';
		$select .= 'a.dokterDPJP as dokter_id,';
		$select .= 'CONCAT(h.title, \' \', UPPER(h.nama), \' \', UPPER(h.spesialisName)) as dokter_nama,';
		$select .= 'a.userCreated as nama_pegawai,';
        $select .= '"1" as status_ranap,';
		$select .= 'e.ALAMAT1 as alamat,';
		$select .= 'e.SATUANKT as satuan,';
		$select .= 'o.pangkatID as pangkat_id,';
		$select .= 'o.name as pangkat_nama,';
		$select .= 'p.diagnosaAwal as kode_diagnosa,';
		$select .= 'q.nama as nama_diagnosa,';
        $select .= 'e.NIK as NIK,';
        $select .= 'e.AGAMA as id_agama,';
        $select .= 'u.name as nama_agama,';
        $select .= 'e.GOLDARAH as gol_darah,';
        $select .= 'e.TELP as telp_pasien,';
        $select .= 's.statusNikahID as id_status_perkawinan,';
        $select .= 's.name as status_perkawinan,';
        $select .= 'e.SUKU as id_suku,';
        $select .= 'v.name as nama_suku,';
        $select .= 'e.DIDIK as id_pendidikan,';
        $select .= 'w.name as nama_pendidikan,';
        $select .= 'e.PKERJAAN as id_pekerjaan,';
        $select .= 'x.name as nama_pekerjaan,';
        $select .= 'aa.id as id_provinsi,';
        $select .= 'aa.name as nama_provinsi,';
        $select .= 't.id as id_kota,';
        $select .= 't.name as nama_kota,';
        $select .= 'y.id as id_kecamatan,';
        $select .= 'y.name as nama_kecamatan,';
        $select .= 'z.id as id_kelurahan,';
        $select .= 'z.name as nama_kelurahan,';
        $select .= 'e.NEGARA as id_negara,';
        $select .= 'ab.name as nama_negara';

		$user = DB::table('register_rinap as a')
                ->select(DB::raw($select))
                ->leftJoin('rinap_kamar_rawat as b', 'b.id', '=', 'a.id_rinap_kamar_rawat')
                ->leftJoin('mst_tempat_tidur as c', 'b.bedID', '=', 'c.bedID')
                ->leftJoin('mst_ruangan as d', 'c.ruanganID', '=', 'd.ruanganID')
                ->leftJoin('mst_pasien as e', 'a.id_pasien', '=', 'e.id_pasien')
                ->leftJoin('tarif_kelas as f', 'b.kelasID', '=', 'f.KODE')
                ->leftJoin('mst_golpas as g', 'a.jns_pembayaran', '=', 'g.golpasID')
                ->leftJoin('mst_dokter as h', 'h.dokterID', '=', 'a.dokterDPJP')
                ->leftJoin('mst_rs as i', 'i.id', '=', 'a.KODE_RS')
                ->leftJoin('mst_dokter as j', 'j.dokterID', '=', 'a.dokterPengirim')
                ->leftJoin('mst_rs as k', 'k.id', '=', 'd.rs')
                ->leftJoin('rinap_keluarga_pengantar as l', 'l.noreg', '=', 'a.noreg')
                ->leftJoin('rinap_diagnosa_awal as m', 'm.noreg', '=', 'a.noreg')
                ->leftJoin('mst_poli as n', 'n.poliid', '=', 'a.asalPoli')
                ->leftJoin('mst_pangkat as o', 'o.pangkatID', '=', 'e.PANGKT')
                ->leftJoin('rinap_diagnosa_awal as p', 'p.noreg', '=', 'a.noreg')
                ->leftJoin('mst_reficd10 as q', 'q.kode', '=', 'p.diagnosaAwal')
                ->leftJoin('mst_golpas as r', 'r.golpasID', '=', 'e.GOLPASKD')
                ->leftJoin('mst_status_nikah as s', 's.statusNikahID', '=', 'e.KAWIN')
                ->leftJoin('mst_kota as t', 't.id', '=', 'e.ID_KOTA')
                ->leftJoin('mst_agama as u', 'u.agamaID', '=', 'e.AGAMA')
                ->leftJoin('mst_suku as v', 'v.id', '=', 'e.SUKU')
                ->leftJoin('mst_pendidikan as w', 'w.pendidikanID', '=', 'e.DIDIK')
                ->leftJoin('mst_pekerjaan as x', 'x.pekerjaanid', '=', 'e.PKERJAAN')
                ->leftJoin('mst_kecamatan as y', 'y.id_kota', '=', 't.id')
                ->leftJoin('mst_kelurahan as z', 'z.id_kecamatan', '=', 'y.id')
                ->leftJoin('mst_provinsi as aa', 'aa.id', '=', 't.id_provinsi')
                ->leftJoin('mst_negara as ab', 'ab.id', '=', 'e.NEGARA')
                ->where('a.noreg', $noreg)
                ->first();

        $user = ($user) ? $user : FALSE;

        return $user;
	}

    public static function listreg($tgl){

        $select  = 'a.noreg as no_pendaftaran,';
        $select .= 'a.id_pasien,';
        $select .= 'a.norm as no_rekam_medik,';
        $select .= 'UPPER(b.NAMA) as nama_pasien,';
        $select .= 'a.tglReg as tanggal_pendaftaran,';
        $select .= 'b.TGLAHIR as tanggal_lahir,';
        $select .= 'a.poliID as ruangan_id,';
        $select .= 'c.name as ruangan_nama,';
        $select .= 'a.jns_pembayaran as carabayar_id,';
        $select .= 'e.golpasName as carabayar_nama,';
        $select .= 'f.name as carabayar_group,';
        $select .= 'UPPER(a.userCreated) as nama_pegawai,';
        $select .= 'd.name as rs,';
        $select .= 'UPPER(a.status) as status';

        $user = DB::table('register_rinap as a')
                ->select(DB::raw($select))
                ->leftJoin('mst_pasien as b', 'b.id_pasien', '=', 'a.id_pasien')
                ->leftJoin('mst_poli as c', 'c.poliid', '=', 'a.poliID')
                ->leftJoin('mst_rs as d', 'd.id', '=', 'a.KODE_RS')
                ->leftJoin('mst_golpas as e', 'e.golpasID', '=', 'a.jns_pembayaran')
                ->leftJoin('mst_grouppas as f', 'f.id', '=', 'e.group')
                ->where('a.tglReg', $tgl)
                ->get();

        $user = ($user) ? $user : FALSE;

        return $user;
    }

    public static function listbynorm($norm){

        $select  = "a.noreg,";
        $select .= "a.norm,";
        $select .= "b.NAMA AS nama_pasien,";
        $select .= "a.tglReg AS tgl_registrasi,";
        $select .= "f.ruanganName as ruangan,";
        $select .= "a.`status`,";
        $select .= "e.`name` AS rs";

        $user = DB::table('register_rinap as a')
                ->select(DB::raw($select))
                ->leftJoin('mst_pasien as b', 'b.id_pasien', '=', 'a.id_pasien')
                ->leftJoin('rinap_kamar_rawat as c', 'c.id', '=', 'a.id_rinap_kamar_rawat')
                ->leftJoin('mst_tempat_tidur as d', 'd.bedID', '=', 'c.bedID')
                ->leftJoin('mst_rs as e', 'e.id', '=', 'a.KODE_RS')
                ->leftJoin('mst_ruangan as f', 'f.ruanganID', '=', 'c.ruanganID')
                ->where('a.norm', $norm)
                ->orderBy('a.tglReg', 'desc')
                ->get();

        $user = ($user) ? $user : FALSE;

        return $user;
    }

    public static function registrasi($condition = array()){

        $select  = 'a.noreg as no_pendaftaran,';
        $select .= 'a.id_pasien,';
        $select .= 'a.norm as no_rekam_medik,';
        $select .= 'UPPER(b.NAMA) as nama_pasien,';
        $select .= 'a.tglReg as tanggal_pendaftaran,';
        $select .= 'b.TGLAHIR as tanggal_lahir,';
        $select .= 'a.poliID as ruangan_id,';
        $select .= 'c.name as ruangan_nama,';
        $select .= 'a.jns_pembayaran as carabayar_id,';
        $select .= 'e.golpasName as carabayar_nama,';
        $select .= 'f.name as carabayar_group,';
        $select .= 'UPPER(a.userCreated) as nama_pegawai,';
        $select .= 'd.name as rs,';
        $select .= 'UPPER(a.status) as status';

        $user = DB::table('register_rjalan as a')
                ->select(DB::raw($select))
                ->leftJoin('mst_pasien as b', 'b.id_pasien', '=', 'a.id_pasien')
                ->leftJoin('mst_poli as c', 'c.poliid', '=', 'a.poliID')
                ->leftJoin('mst_rs as d', 'd.id', '=', 'a.KODE_RS')
                ->leftJoin('mst_golpas as e', 'e.golpasID', '=', 'a.jns_pembayaran')
                ->leftJoin('mst_grouppas as f', 'f.id', '=', 'e.group');

        if(count($condition) > 0 ){
            foreach ($condition as $rowgroup => $valgroup) {
                if($rowgroup == 'where'){
                    foreach ($valgroup as $val) {
                        $user->where($val['field'], $val['operator'],$val['value']);
                    }
                }
            }
        }

        $user = $user->get();

        $user = ($user) ? $user : FALSE;

        return $user;
    }
}

