<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\HelperService;

class Token extends Model
{
    protected $table = 'helfa_token';
    protected $primaryKey = 'id';

    public $timestamps = false;
}