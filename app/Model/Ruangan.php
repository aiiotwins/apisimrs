<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ruangan extends Model {

    protected $table = 'mst_ruangan';

    protected $fillable = [];

    protected $hidden = [];
}

