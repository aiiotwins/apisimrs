<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Seppasien extends Model {

    protected $table = 'sep_pasien';

    protected $fillable = [];

    protected $hidden = [];

    public static function simpan($data = array()){

        date_default_timezone_set('Asia/Jakarta');

        $insert = DB::table('sep_pasien')->insert(
                    [
                        'noreg' => $data['noreg'],
                        'noSep'=> $data['noSep'],
                        'nik'=> $data['nik'],
                        'tglLahir'=> $data['tglLahir'],
                        'jnsKelamin'=> $data['jnsKelamin'],
                        'noKartu'=> $data['noKartu'],
                        'tglCetakKartu'=> $data['tglCetakKartu'],
                        'jnsPesertaName'=> $data['jnsPesertaName'],
                        'kelasName'=> $data['kelasName'],
                        'cabangName'=> '',
                        'providerID'=> $data['providerID'],
                        'providerName'=> $data['providerName'],
                        'jnsPesertaID'=> $data['jnsPesertaID'],
                        'kelasID'=> $data['kelasID'],
                        'cabangID'=> '0901R003',
                        'tglRujukan'=> $data['tglRujukan'],
                        'noRujukan'=> $data['noRujukan'],
                        'asalRujukanID'=> $data['asalRujukanID'],
                        'asalRujukanName'=> $data['asalRujukanName'],
                        'diagnosaID'=> $data['diagnosaID'],
                        'diagnosaName'=> $data['diagnosaName'],
                        'catatan'=> '',
                        'tglSep'=> $data['tglSep'],
                        'dateCreated'=> date('Y-m-d H:i:s'),
                        'jnsPelayanan'=> $data['jnsPelayanan'],
                        'poliTujuan'=> $data['poliTujuan'],
                        'userCreated'=> 'Anjungan Helfa',
                        'poliID'=> $data['poliID'],
                        'poliName'=> $data['poliName'],
                        'nama'=> $data['nama'],
                        'norm'=> $data['norm'],
                        'id_pasien'=> $data['id_pasien'],
                        'kelasRawat'=> $data['kelasRawat']
                    ]
                );

        return $result = ($insert) ? TRUE : FALSE;
    }
}

