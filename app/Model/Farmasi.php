<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Farmasi extends Model {

    // protected $table = 'monitoring_rekam_medis';

    // protected $fillable = [];

    // protected $hidden = [];

    public static function getStockObatGudangByName($condition = array()){
      
        $select  = 'tabelobat.KET as nama_obat,';
        $select .= 'tabelobat.SATUAN as satuan,';
        $select .= 'FORMAT(GREATEST(tabelobat.STOK, 0), 0) as stok_gudang,';
        $select .= 'tabelobat.ASKES as jns_fornas';

        $obat = DB::connection('mysql5')->table('tabelobat')
                ->select(DB::raw($select));

        if(count($condition) > 0 ){
            foreach ($condition as $rowgroup => $valgroup) {
                if($rowgroup == 'where'){
                    foreach ($valgroup as $val) {
                        $obat->where($val['field'], $val['operator'],$val['value']);
                    }
                }
            }
        }

        $obat->where('tabelobat.STATUS','=','AKTIF');

        $obat = $obat->get()->toArray();

        $obat = ($obat) ? $obat : FALSE;

        return $obat;
    }
}

