<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Lis extends Model {

    // protected $table = 'billing_perawatan_detail';

    // protected $fillable = [];

    // protected $hidden = [];

    public static function getOutstandingList($condition = array()){

    	$rjalan = DB::table('order_data_lis')
		    ->select(	'mst_pasien.KODE AS PatientCode',
		    			DB::raw("CONCAT(mst_awalan.ket, '. ', mst_pasien.NAMA) AS PatientName"),
		    			DB::raw("IF( mst_pasien.KELAMIN = 'L', 'M', 'F' ) AS PatientSexCode"),
		    			DB::raw("IF( mst_pasien.KELAMIN = 'L', 'Male', 'Female' ) AS PatientSexName"),
		    			DB::raw("DATE_FORMAT( mst_pasien.TGLAHIR, '%Y/%m/%d' ) AS PatientDob"),
		    			"mst_pasien.ALAMAT1 AS PatientAddress",
						"mst_kota.id AS PatientCityCode",
						"mst_kota.name AS PatientCityName",
						"mst_pasien.TELP AS PatientMobileNumber",
						DB::raw("'-' AS PatientFaxNumber"),
						"mst_pasien.TELP2 AS PatientPhoneNumber",
						DB::raw("NULL AS PatientEmail"),
						"register_rjalan.noreg AS VisitNumber",
						"order_data_lis.order_number AS OrderNumber",
						"order_data_lis.dateCreated AS OrderDateTime",
						DB::raw("IF(mst_pelaksana.pelaksanaID IS NULL, '-', mst_pelaksana.pelaksanaID) AS DoctorOrderCode"),
						DB::raw("IF(mst_pelaksana.pelaksanaName IS NULL, '-', mst_pelaksana.pelaksanaName) AS DoctorOrderName"),
						DB::raw("IF(diagnosa_akhir.diagnosa_id IS NULL, '-', diagnosa_akhir.diagnosa_id) AS DiagnosisCode"),
						DB::raw("IF(diagnosa_akhir.diagnosa_name IS NULL, '-', diagnosa_akhir.diagnosa_name) AS DiagnosisName"),
						"order_data_lis.isCito AS IsCito",
						DB::raw("IF( LEFT ( register_rjalan.noreg, 2 ) = 'RJ', IF ( register_rjalan.poliID = '033', 'IGD', 'RJ' ), 'RI' ) AS ServiceUnitCode"),
						DB::raw("IF( LEFT ( register_rjalan.noreg, 2 ) = 'RJ', IF ( register_rjalan.poliID = '033', mst_poli.name, 'RJ' ), 'RI' ) AS ServiceUnitName"),
						"mst_golpas.golpasID AS GuarantorID",
						"mst_golpas.golpasName AS GuarantorName",
						DB::raw("CONCAT( order_data_lis.golpasID, '-', order_data_lis.id_mst_kelas ) AS AgreementID"),
						DB::raw("CONCAT( mst_golpas2.golpasName, '-', mst_kelas2.name ) AS AgreementName"),
						"mst_kelas2.id AS ServiceClassCode",
						"mst_kelas2.name AS ServiceClassName",
						"register_rjalan.poliID AS WardRoomCode",
						"mst_poli.name AS WardRoomName",
						"register_rjalan.poliID AS BedCode",
						"mst_poli.name AS BedName",
						"order_data_lis.ReceivedFlag AS ReceivedFlag",
						"order_data_lis.LabRegNo AS LabRegNo",
						"order_data_lis.ReceivedDatetime AS ReceivedDatetime"    			
		    		)
		    ->leftJoin('register_rjalan', 'order_data_lis.noreg', '=', 'register_rjalan.noreg')
		    ->leftJoin('mst_pasien', 'register_rjalan.id_pasien', '=', 'mst_pasien.id_pasien')
		    ->leftJoin('mst_kelurahan', 'mst_pasien.ID_KELURAHAN', '=', 'mst_kelurahan.id')
		    ->leftJoin('mst_kecamatan', 'mst_kelurahan.id_kecamatan', '=', 'mst_kecamatan.id')
		    ->leftJoin('mst_kota', 'mst_kecamatan.id_kota', '=', 'mst_kota.id')
		    ->leftJoin('mst_provinsi', 'mst_kota.id_provinsi', '=', 'mst_provinsi.id')
		    ->leftJoin('mst_pelaksana', 'register_rjalan.dokter', '=', 'mst_pelaksana.pelaksanaID')
		    ->leftJoin(DB::raw("( SELECT noreg, diagnosa_id, diagnosa_name FROM diagnosa_akhir WHERE active = '1' ) as diagnosa_akhir"), 
				        function($join)
				        {
				           $join->on('diagnosa_akhir.noreg', '=', 'register_rjalan.noreg');
				        })
		    ->leftJoin('mst_poli', 'mst_poli.poliid', '=', 'register_rjalan.poliID')
		    ->leftJoin('mst_golpas', 'mst_golpas.golpasID', '=', 'register_rjalan.jns_pembayaran')
		    ->leftJoin('mst_golpas AS mst_golpas2', 'mst_golpas2.golpasID', '=', 'order_data_lis.golpasID')
		    ->leftJoin('mst_kelas AS mst_kelas2', 'mst_kelas2.id', '=', 'order_data_lis.id_mst_kelas')
		    ->leftJoin('mst_awalan', 'mst_awalan.id', '=', 'mst_pasien.ID_AWALAN')
		    ->where(DB::raw('LEFT ( order_data_lis.noreg, 2 )'),'=','RJ');
		    if(count($condition) > 0 ){
	            foreach ($condition as $rowgroup => $valgroup) {
	                if($rowgroup == 'where'){
	                    foreach ($valgroup as $val) {
	                        $rjalan->where($val['field'], $val['operator'],$val['value']);
	                    }
	                }
	            }
	        }
		    $rjalan->groupBy('order_data_lis.order_number');

		    $rjalan = $rjalan->get()->toArray();

    	$rinap = DB::table('order_data_lis')
		    ->select(	'mst_pasien.KODE AS PatientCode',
		    			DB::raw("CONCAT(mst_awalan.ket, '. ', mst_pasien.NAMA) AS PatientName"),
						DB::raw("IF( mst_pasien.KELAMIN = 'L', 'M', 'F' ) AS PatientSexCode"),
						DB::raw("IF( mst_pasien.KELAMIN = 'L', 'Male', 'Female' ) AS PatientSexName"),
						DB::raw("DATE_FORMAT( mst_pasien.TGLAHIR, \"%Y/%m/%d\" ) AS PatientDob"),
						"mst_pasien.ALAMAT1 AS PatientAddress",
						"mst_kota.id AS PatientCityCode",
						"mst_kota.name AS PatientCityName",
						"mst_pasien.TELP AS PatientMobileNumber",
						DB::raw("'-' AS PatientFaxNumber"),
						"mst_pasien.TELP2 AS PatientPhoneNumber",
						DB::raw("NULL AS PatientEmail"),
						"register_rinap.noreg AS VisitNumber",
						"order_data_lis.order_number AS OrderNumber",
						"order_data_lis.dateCreated AS OrderDateTime",
						DB::raw("IF(mst_pelaksana.pelaksanaID IS NULL, '-', mst_pelaksana.pelaksanaID) AS DoctorOrderCode"),
						DB::raw("IF(mst_pelaksana.pelaksanaName IS NULL , '-', mst_pelaksana.pelaksanaName) AS DoctorOrderName"),
						DB::raw("IF(mst_reficd10.kode IS NULL, '-', mst_reficd10.kode) AS DiagnosisCode"),
						DB::raw("IF(mst_reficd10.nama IS NULL, '-', mst_reficd10.nama) AS DiagnosisName"),
						"order_data_lis.isCito AS IsCito",
						DB::raw("'RI' AS ServiceUnitCode"),
						DB::raw("'RI' AS ServiceUnitName"),
						"mst_golpas.golpasID AS GuarantorID",
						"mst_golpas.golpasName AS GuarantorName",
						DB::raw("CONCAT(order_data_lis.golpasID,'-',order_data_lis.id_mst_kelas) AS AgreementID"),
						DB::raw("CONCAT(mst_golpas2.golpasName, '-', mst_kelas2.name) AS AgreementName"),
						"mst_kelas2.id AS ServiceClassCode",
						"mst_kelas2.name AS ServiceClassName",
						"mst_ruangan.ruanganID AS WardRoomCode",
						"mst_ruangan.ruanganName AS WardRoomName",
						"mst_tempat_tidur.bedID  AS BedCode",
						"mst_tempat_tidur.keterangan AS BedName",
						"order_data_lis.ReceivedFlag AS ReceivedFlag",
						"order_data_lis.LabRegNo AS LabRegNo",
						"order_data_lis.ReceivedDatetime AS ReceivedDatetime"
		    		)
		    ->leftJoin('register_rinap', 'order_data_lis.noreg', '=', 'register_rinap.noreg')
		    ->leftJoin('mst_pasien', 'register_rinap.id_pasien', '=', 'mst_pasien.id_pasien')
		    ->leftJoin('mst_kelurahan', 'mst_pasien.ID_KELURAHAN', '=', 'mst_kelurahan.id')
		    ->leftJoin('mst_kecamatan', 'mst_kelurahan.id_kecamatan', '=', 'mst_kecamatan.id')
		    ->leftJoin('mst_kota', 'mst_kecamatan.id_kota', '=', 'mst_kota.id')
		    ->leftJoin('mst_provinsi', 'mst_kota.id_provinsi', '=', 'mst_provinsi.id')
		    ->leftJoin('mst_pelaksana', 'register_rinap.dokterDPJP', '=', 'mst_pelaksana.pelaksanaID')
		    ->leftJoin('rinap_diagnosa_awal', 'rinap_diagnosa_awal.noreg', '=', 'register_rinap.noreg')
		    ->leftJoin('mst_reficd10', 'rinap_diagnosa_awal.diagnosaAwal', '=', 'mst_reficd10.kode')
		    ->leftJoin('mst_golpas', 'mst_golpas.golpasID', '=', 'register_rinap.jns_pembayaran')
		    ->leftJoin('mst_kelas', 'mst_kelas.id', '=', 'register_rinap.id_kelas')
		    ->leftJoin('mst_golpas AS mst_golpas2', 'mst_golpas2.golpasID', '=', 'order_data_lis.golpasID')
		    ->leftJoin('mst_kelas AS mst_kelas2', 'mst_kelas2.id', '=', 'order_data_lis.id_mst_kelas')
		    ->leftJoin('rinap_kamar_rawat', 'rinap_kamar_rawat.id', '=', 'register_rinap.id_rinap_kamar_rawat')
		    ->leftJoin('mst_tempat_tidur', 'mst_tempat_tidur.bedID', '=', 'rinap_kamar_rawat.bedID')
		    ->leftJoin('mst_ruangan', 'mst_ruangan.ruanganID', '=', 'mst_tempat_tidur.ruanganID')
		    ->leftJoin('mst_awalan', 'mst_awalan.id', '=', 'mst_pasien.ID_AWALAN')
		    ->where(DB::raw('LEFT ( order_data_lis.noreg, 2 )'),'=','RI');
		    if(count($condition) > 0 ){
	            foreach ($condition as $rowgroup => $valgroup) {
	                if($rowgroup == 'where'){
	                    foreach ($valgroup as $val) {
	                        $rinap->where($val['field'], $val['operator'],$val['value']);
	                    }
	                }
	            }
	        }
		    //$rinap->unionAll($rjalan);
		    $rinap->groupBy('order_data_lis.order_number');

		    $rinap = $rinap->get()->toArray();

		$result = array_merge($rjalan,$rinap);

        $result = ($result) ? $result : FALSE;

        return $result;
	}

	public static function getOrderItem($order_number){

		$orderlist = DB::table('billing_perawatan_detail')
		    ->select(	'tarif.id_pemeriksaan as ItemCode',
		    			'tarif.namaTarif as ItemName'
		    		)
		    ->leftJoin('tarif_harga', 'tarif_harga.id', '=', 'billing_perawatan_detail.id_tarif_harga')
		    ->leftJoin('tarif', 'tarif_harga.tarifID', '=', 'tarif.tarifID')
		    ->leftJoin('billing_perawatan_head', 'billing_perawatan_head.id', '=', 'billing_perawatan_detail.id_billing_perawatan_head')
		    ->where('billing_perawatan_head.unit','=','LAB')
		    ->where('billing_perawatan_detail.deleted','=','0')
		    ->where('billing_perawatan_detail.order_number_lis','=',$order_number)
		    ->whereNotIn('tarif.id_pemeriksaan', function ($query) {
		        $query->select('id')->from('emr_mst_pemeriksaan')->where('shown_in_bridge','=','0');
		    });
		    
		    $orderlist = $orderlist->get()->toArray();

		    $result = ($orderlist) ? $orderlist : NULL;

		    return $result;
	}

	public static function SetReceivedOrder($data){

		$data['ReceivedDatetime'] = str_replace('/', '-', $data['ReceivedDatetime']);

		$data['ReceivedFlag'] = ($data['ReceivedFlag'] == 1 || $data['ReceivedFlag'] == 'true') ? 'true' : 'false';

		$update = DB::table('order_data_lis')
              ->where('order_number', $data['orderNumber'])
              ->update(['ReceivedFlag' => $data['ReceivedFlag'], 'LabRegNo' => $data['LabRegNo'], 'ReceivedDatetime' => $data['ReceivedDatetime']]);

	    $result = ($update) ? $update : FALSE;

	    return $result;
	}


	public static function PostItemsResult($data){

		date_default_timezone_set('Asia/Jakarta');

		$data = (object) $data;

		//Cek Dulu Number 
		$cek_data = DB::connection('mysql6')->table('result_bridging_lis')
              		->where('lis_reg_no', $data->LisRegNo)
              		->where('lis_test_id', $data->LisTestId)
              		->first();

        if($cek_data){
        	$resultdata = DB::connection('mysql6')->table('result_bridging_lis')
        			->where('lis_reg_no', $data->LisRegNo)
              		->where('lis_test_id', $data->LisTestId)
              		->update([
		                        'lis_reg_no' 		=> $data->LisRegNo,	
								'lis_test_id' 		=> $data->LisTestId,
								'his_reg_no'		=> $data->HisRegNo,	
								'test_name' 		=> $data->TestName,	
								'result' 			=> $data->Result,	
								'result_comment'	=> $data->ResultComment,	
								'reference_value' 	=> $data->ReferenceValue,	
								'reference_note' 	=> $data->ReferenceNote,	
								'test_flag_sign'	=> $data->TestFlagSign,	
								'test_units_name' 	=> $data->TestUnitsName,	
								'instrument_name' 	=> $data->InstrumentName,	
								'authorization_date'=> $data->AuthorizationDate,	
								'authorization_user'=> $data->AuthorizationUser,
								'greaterthan_value' => $data->GreaterThanValue,
								'lessthan_value'    => $data->LessThanValue,	
								'company_id'		=> $data->CompanyId,
								'agreement_id' 		=> $data->AgreementId,
								'age_year' 			=> $data->AgeYear,	
								'age_month' 		=> $data->AgeMonth,	
								'age_days' 			=> $data->AgeDays,	
								'his_test_id_order' => $data->HisTestIdOrder,	
								'transfer_flag' 	=> $data->TransferFlag	
		                    ]);
        }else{
        	$resultdata = DB::connection('mysql6')->table('result_bridging_lis')->insert(
                    [
                        'lis_reg_no' 		=> $data->LisRegNo,	
						'lis_test_id' 		=> $data->LisTestId,
						'his_reg_no'		=> $data->HisRegNo,	
						'test_name' 		=> $data->TestName,	
						'result' 			=> $data->Result,	
						'result_comment'	=> $data->ResultComment,	
						'reference_value' 	=> $data->ReferenceValue,	
						'reference_note' 	=> $data->ReferenceNote,	
						'test_flag_sign'	=> $data->TestFlagSign,	
						'test_units_name' 	=> $data->TestUnitsName,	
						'instrument_name' 	=> $data->InstrumentName,	
						'authorization_date'=> $data->AuthorizationDate,	
						'authorization_user'=> $data->AuthorizationUser,
						'greaterthan_value' => $data->GreaterThanValue,
						'lessthan_value'    => $data->LessThanValue,	
						'company_id'		=> $data->CompanyId,
						'agreement_id' 		=> $data->AgreementId,
						'age_year' 			=> $data->AgeYear,	
						'age_month' 		=> $data->AgeMonth,	
						'age_days' 			=> $data->AgeDays,	
						'his_test_id_order' => $data->HisTestIdOrder,	
						'transfer_flag' 	=> $data->TransferFlag	
                    ]
                );
        }

        

        return $result = ($resultdata) ? TRUE : FALSE;
	} 

}