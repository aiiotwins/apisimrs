<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Mappingjnsbpjs extends Model {

    protected $table = 'mapping_jns_bpjs';

    protected $fillable = [];

    protected $hidden = [];

    public static function getJnsPembayaran($val){

    	$jns_pembayaran = DB::table('mapping_jns_bpjs')->where('keterangan', '=', $val)->first();

    	return $result = ($jns_pembayaran) ? $jns_pembayaran : FALSE;
    }

}

