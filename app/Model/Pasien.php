<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pasien extends Model {

    protected $table = 'mst_pasien';

    protected $fillable = [];

    protected $hidden = [];

    public function golpas()
    {
        return $this->hasOne(Golpas::class, 'golpasID', 'GOLPASKD');
    }

    public static function norm($norm){

    	$select  = 'mst_pasien.id_pasien,';
    	$select .= 'mst_pasien.KODE AS norm,';
		$select .= 'mst_pasien.NAMA AS nama_pasien,';
		$select .= 'mst_pasien.TMPLAHIR AS tempat_lahir,';
		$select .= 'mst_pasien.TGLAHIR AS tanggal_lahir,';
		//$select .= 'mst_pasien.TGLAHIR AS umur,';
		$select .= 'mst_pasien.NIK,';
		$select .= 'mst_pasien.KELAMIN AS jns_kelamin,';
		$select .= 'mst_status_nikah.statusNikahID AS id_status_perkawinan,';
		$select .= 'mst_status_nikah.`name` AS status_perkawinan,';
		$select .= 'mst_kelurahan.id AS id_kelurahan,';
		$select .= 'mst_kelurahan.`name` AS nama_kelurahan,';
		$select .= 'mst_kecamatan.id AS id_kecamatan,';
		$select .= 'mst_kecamatan.`name` AS nama_kecamatan,';
		$select .= 'mst_kota.id AS id_kota,';
		$select .= 'mst_kota.`name` AS nama_kota,';
		$select .= 'mst_provinsi.id AS id_provinsi,';
		$select .= 'mst_provinsi.`name` AS nama_provinsi,';
		$select .= 'mst_pasien.TELP AS telp_pasien,';
		$select .= 'mst_pasien.ALAMAT1 AS alamat,';
		$select .= 'mst_pendidikan.pendidikanID AS id_pendidikan,';
		$select .= 'mst_pendidikan.`name` AS nama_pendidikan,';
		$select .= 'mst_pekerjaan.pekerjaanid AS id_pekerjaan,';
		$select .= 'mst_pekerjaan.`name` AS nama_pekerjaan,';
		$select .= 'mst_suku.id AS id_suku,';
		$select .= 'mst_suku.`name` AS suku,';
		$select .= 'mst_negara.id AS id_negara,';
		$select .= 'mst_negara.`name` AS nama_negara,';
		$select .= 'mst_agama.agamaID AS id_agama,';
		$select .= 'mst_agama.`name` AS nama_agama,';
		$select .= 'mst_rs.id AS id_rs,';
		$select .= 'mst_rs.`name` AS nama_rs,';
		$select .= 'mst_awalan.id AS id_awalan,';
		$select .= 'mst_awalan.`ket` AS awalan,';		
		$select .= 'mst_pasien.TELPKEL AS telp_keluarga,';
		$select .= 'mst_pasien.GOLDARAH AS gol_darah,';
		$select .= 'mst_pasien.NIP AS nip,';
		$select .= 'mst_angkatan.angkatanID AS id_angkatan,';
		$select .= 'mst_angkatan.name AS nama_angkatan,';
		$select .= 'mst_pangkat.pangkatID AS id_pangkat,';
		$select .= 'mst_pangkat.name AS nama_pangkat,';
		$select .= 'mst_pasien.SATUANKT AS kesatuan,';
		$select .= 'mst_pasien.JABATAN AS jabatan,';			
		$select .= 'mst_grouppas.id AS id_group,';
		$select .= 'mst_grouppas.name AS nama_group,';		
		$select .= 'mst_golpas.golpasID AS id_golpas,';
		$select .= 'mst_golpas.golpasName AS nama_golpas,';
		$select .= 'mst_pasien.NOASKES AS no_bpjs,';
		$select .= 'mst_hubkel.hubkelID AS id_hubkel,';
		$select .= 'mst_hubkel.name AS nama_hubkel';

		$user = DB::table('mst_pasien')
                ->select(DB::raw($select))
                ->leftJoin('mst_status_nikah', 'mst_status_nikah.statusNikahID', '=', 'mst_pasien.KAWIN')
                ->leftJoin('mst_kelurahan', 'mst_kelurahan.id', '=', 'mst_pasien.ID_KELURAHAN')
                ->leftJoin('mst_kecamatan', 'mst_kelurahan.id_kecamatan', '=', 'mst_kecamatan.id')
                ->leftJoin('mst_kota', 'mst_kecamatan.id_kota', '=', 'mst_kota.id')
                ->leftJoin('mst_provinsi', 'mst_kota.id_provinsi', '=', 'mst_provinsi.id')
                ->leftJoin('mst_pendidikan', 'mst_pendidikan.pendidikanID', '=', 'mst_pasien.DIDIK')
                ->leftJoin('mst_pekerjaan', 'mst_pekerjaan.pekerjaanid', '=', 'mst_pasien.PKERJAAN')
                ->leftJoin('mst_suku', 'mst_suku.id', '=', 'mst_pasien.SUKU')
                ->leftJoin('mst_negara', 'mst_negara.id', '=', 'mst_pasien.NEGARA')
                ->leftJoin('mst_agama', 'mst_agama.agamaID', '=', 'mst_pasien.AGAMA')
                ->leftJoin('mst_rs', 'mst_rs.id', '=', 'mst_pasien.RS')
                ->leftJoin('mst_awalan', 'mst_awalan.id', '=', 'mst_pasien.id_awalan')
                ->leftJoin('mst_angkatan', 'mst_angkatan.angkatanID', '=', 'mst_pasien.GOLMIL')
                ->leftJoin('mst_pangkat', 'mst_pangkat.pangkatID', '=', 'mst_pasien.PANGKT')
                ->leftJoin('mst_golpas', 'mst_golpas.golpasID', '=', 'mst_pasien.GOLPASKD')
                ->leftJoin('mst_grouppas', 'mst_grouppas.id', '=', 'mst_golpas.group')
                ->leftJoin('mst_hubkel', 'mst_hubkel.hubkelID', '=', 'mst_pasien.hubkel')
                ->where('mst_pasien.KODE', $norm)
                ->where('mst_pasien.ACTIVE', '1')
                ->get();

        $user = ($user) ? $user : FALSE;

        return $user;
	}

	public static function paramsearch($array = array()){

		DB::connection()->enableQueryLog();

    	$select  = 'mst_pasien.id_pasien,';
    	$select .= 'mst_pasien.KODE AS norm,';
		$select .= 'mst_pasien.NAMA AS nama_pasien,';
		$select .= 'mst_pasien.TMPLAHIR AS tempat_lahir,';
		$select .= 'mst_pasien.TGLAHIR AS tanggal_lahir,';
		//$select .= 'mst_pasien.TGLAHIR AS umur,';
		$select .= 'mst_pasien.NIK,';
		$select .= 'mst_pasien.KELAMIN AS jns_kelamin,';
		$select .= 'mst_status_nikah.statusNikahID AS id_status_perkawinan,';
		$select .= 'mst_status_nikah.`name` AS status_perkawinan,';
		$select .= 'mst_kelurahan.id AS id_kelurahan,';
		$select .= 'mst_kelurahan.`name` AS nama_kelurahan,';
		$select .= 'mst_kecamatan.id AS id_kecamatan,';
		$select .= 'mst_kecamatan.`name` AS nama_kecamatan,';
		$select .= 'mst_kota.id AS id_kota,';
		$select .= 'mst_kota.`name` AS nama_kota,';
		$select .= 'mst_provinsi.id AS id_provinsi,';
		$select .= 'mst_provinsi.`name` AS nama_provinsi,';
		$select .= 'mst_pasien.TELP AS telp_pasien,';
		$select .= 'mst_pasien.ALAMAT1 AS alamat,';
		$select .= 'mst_pendidikan.pendidikanID AS id_pendidikan,';
		$select .= 'mst_pendidikan.`name` AS nama_pendidikan,';
		$select .= 'mst_pekerjaan.pekerjaanid AS id_pekerjaan,';
		$select .= 'mst_pekerjaan.`name` AS nama_pekerjaan,';
		$select .= 'mst_suku.id AS id_suku,';
		$select .= 'mst_suku.`name` AS suku,';
		$select .= 'mst_negara.id AS id_negara,';
		$select .= 'mst_negara.`name` AS nama_negara,';
		$select .= 'mst_agama.agamaID AS id_agama,';
		$select .= 'mst_agama.`name` AS nama_agama,';
		$select .= 'mst_rs.id AS id_rs,';
		$select .= 'mst_rs.`name` AS nama_rs,';
		$select .= 'mst_awalan.id AS id_awalan,';
		$select .= 'mst_awalan.`ket` AS awalan,';		
		$select .= 'mst_pasien.TELPKEL AS telp_keluarga,';
		$select .= 'mst_pasien.GOLDARAH AS gol_darah,';
		$select .= 'mst_pasien.NIP AS nip,';
		$select .= 'mst_angkatan.angkatanID AS id_angkatan,';
		$select .= 'mst_angkatan.name AS nama_angkatan,';
		$select .= 'mst_pangkat.pangkatID AS id_pangkat,';
		$select .= 'mst_pangkat.name AS nama_pangkat,';
		$select .= 'mst_pasien.SATUANKT AS kesatuan,';
		$select .= 'mst_pasien.JABATAN AS jabatan,';			
		$select .= 'mst_grouppas.id AS id_group,';
		$select .= 'mst_grouppas.name AS nama_group,';		
		$select .= 'mst_golpas.golpasID AS id_golpas,';
		$select .= 'mst_golpas.golpasName AS nama_golpas,';
		$select .= 'mst_pasien.NOASKES AS no_bpjs,';
		$select .= 'mst_hubkel.hubkelID AS id_hubkel,';
		$select .= 'mst_hubkel.name AS nama_hubkel';

		$user = DB::table('mst_pasien')
                ->select(DB::raw($select))
                ->leftJoin('mst_status_nikah', 'mst_status_nikah.statusNikahID', '=', 'mst_pasien.KAWIN')
                ->leftJoin('mst_kelurahan', 'mst_kelurahan.id', '=', 'mst_pasien.ID_KELURAHAN')
                ->leftJoin('mst_kecamatan', 'mst_kelurahan.id_kecamatan', '=', 'mst_kecamatan.id')
                ->leftJoin('mst_kota', 'mst_kecamatan.id_kota', '=', 'mst_kota.id')
                ->leftJoin('mst_provinsi', 'mst_kota.id_provinsi', '=', 'mst_provinsi.id')
                ->leftJoin('mst_pendidikan', 'mst_pendidikan.pendidikanID', '=', 'mst_pasien.DIDIK')
                ->leftJoin('mst_pekerjaan', 'mst_pekerjaan.pekerjaanid', '=', 'mst_pasien.PKERJAAN')
                ->leftJoin('mst_suku', 'mst_suku.id', '=', 'mst_pasien.SUKU')
                ->leftJoin('mst_negara', 'mst_negara.id', '=', 'mst_pasien.NEGARA')
                ->leftJoin('mst_agama', 'mst_agama.agamaID', '=', 'mst_pasien.AGAMA')
                ->leftJoin('mst_rs', 'mst_rs.id', '=', 'mst_pasien.RS')
                ->leftJoin('mst_awalan', 'mst_awalan.id', '=', 'mst_pasien.id_awalan')
                ->leftJoin('mst_angkatan', 'mst_angkatan.angkatanID', '=', 'mst_pasien.GOLMIL')
                ->leftJoin('mst_pangkat', 'mst_pangkat.pangkatID', '=', 'mst_pasien.PANGKT')
                ->leftJoin('mst_golpas', 'mst_golpas.golpasID', '=', 'mst_pasien.GOLPASKD')
                ->leftJoin('mst_grouppas', 'mst_grouppas.id', '=', 'mst_golpas.group')
                ->leftJoin('mst_hubkel', 'mst_hubkel.hubkelID', '=', 'mst_pasien.hubkel');

        foreach ($array as $key => $value) {
        	$user = $user->where($key, $value);
        }
        
        $user = $user->get();

        $user = ($user) ? $user : FALSE;

        return $user;
	}

	public static function registrasi($condition = array()){

        $select = "mst_golpas.payment";

        $data_regis = DB::table('mst_pasien')
                ->select(DB::raw($select))
                ->leftJoin('mst_golpas', 'mst_golpas.golpasID', '=', 'mst_pasien.GOLPASKD');

        if(count($condition) > 0 ){
            foreach ($condition as $rowgroup => $valgroup) {
                if($rowgroup == 'where'){
                    foreach ($valgroup as $val) {
                        $data_regis->where($val['field'], $val['operator'],$val['value']);
                    }
                }
            }
        }

        $data_regis = $data_regis->first();

        $data_regis = ($data_regis) ? $data_regis : FALSE;

        return $data_regis;
    }
}

