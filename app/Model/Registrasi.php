<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Registrasi extends Model
{
    protected $table = 'andro_regis';

    protected $fillable = [
    	'noreg','norm','nama','noaskes','tglReg','jamReg','str_tanggal_reg','poliklinik','status','id_poliklinik','statusRm', 'helfa_no_antrian', 'helfa_token', 'id_rs', 'id_pasien', 'helfa_queue_number', 'nomor_ref', 'jns_kunjungan'
    ];

    protected $hidden = ['id'];

    public $timestamps = false;

}