<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Monitoringrm extends Model {

    protected $table = 'monitoring_rekam_medis';

    protected $fillable = [];

    protected $hidden = [];

    public static function saveMonitoringrm($data = array()){

        date_default_timezone_set('Asia/Jakarta');

        $insert = DB::table('monitoring_rekam_medis')->insert(
                    [
                        'norm' => $data['norm'],
                        'id_pasien' => $data['id_pasien'],
                        'noreg' => $data['noreg'],
                        'tgl_status' => date('Y-m-d'),
                        'jam_status' => date('H:i:s'),
                        'status' => $data['statusRM'],
                        'user' => 'Anjungan Helfa',
                        'rs' => $data['KODE_RS'],
                        'jns_rawat' => $data['jns_rawat'],
                        'from' => $data['from'],
                        'to' => $data['to']
                    ]
                );

        return $result = ($insert) ? TRUE : FALSE;
    }
}

