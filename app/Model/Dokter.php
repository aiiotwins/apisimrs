<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Dokter extends Model {

    protected $table = 'mst_dokter';

    protected $fillable = [];

    protected $hidden = [];
}

