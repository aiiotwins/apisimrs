<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Poli extends Model {

    protected $table = 'mst_poli';

    protected $fillable = ['poliid','name'];

    protected $hidden = ['sr_recno','sr_deleted','kel','subjalan','kodepolibpjs','id_ruangan_lantai','id_ruang_tindakan', 'active'];
}

