<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Bed extends Model {

    protected $table = 'mst_tempat_tidur';

    protected $fillable = [];

    protected $hidden = [];

    public static function getData(){

    	$select  = "mst_tempat_tidur.bedID as id_bed, ";
		$select .= "mst_tempat_tidur.keterangan as nama_bed, ";
		$select .= "mst_tempat_tidur.lantai, ";
		$select .= "mst_tempat_tidur.no_kamar, ";
		$select .= "mst_tempat_tidur.jns_rawat, ";
		$select .= "mst_tempat_tidur.rs, ";
		$select .= "mst_ruangan.ruanganName as nama_ruangan";

		$ruangan = DB::table('mst_tempat_tidur')
                ->select(DB::raw($select))
                ->leftJoin('mst_ruangan', 'mst_tempat_tidur.ruanganID', '=', 'mst_ruangan.ruanganID')
                ->where('mst_tempat_tidur.active', 1)
                ->get();

        $ruangan = ($ruangan) ? $ruangan : FALSE;

        return $ruangan;
	}
}

