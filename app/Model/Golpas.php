<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Golpas extends Model {

    protected $table = 'mst_golpas';

    protected $fillable = [];

    protected $hidden = [];

    public function pasien()
    {
    	return $this->belongsTo(Pasien::class, 'GOLPASKD', 'golpasID');
	}
}

