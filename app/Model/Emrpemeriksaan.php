<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Emrpemeriksaan extends Model {

    protected $table = 'emr_pemeriksaan';

    protected $fillable = [];

    protected $hidden = [];

    public static function getEmrMasterPemeriksaan($condition = array()){
    	DB::connection()->enableQueryLog();
      
		$select  = "emr_mst_pemeriksaan.id, ";
		$select .= "emr_mst_pemeriksaan.`name`, ";
		$select .= "cat_tarif.namaCatTarif as cat_tarif, ";
		$select .= "cat_tarif2.namaCatTarif as sub_cat_tarif,";
		$select .= "emr_mst_pemeriksaan.`group`";

        $pemeriksaan = DB::table('emr_mst_pemeriksaan')
                ->select(DB::raw($select))
                ->leftJoin('cat_tarif', 'emr_mst_pemeriksaan.id_cat_tarif', '=', 'cat_tarif.cat_tarifID')
                ->leftJoin('cat_tarif as cat_tarif2', 'emr_mst_pemeriksaan.id_cat_tarif', '=', 'cat_tarif2.cat_tarifID');

        if(count($condition) > 0 ){
            foreach ($condition as $rowgroup => $valgroup) {
                if($rowgroup == 'where'){
                    foreach ($valgroup as $val) {
                        $pemeriksaan->where($val['field'], $val['operator'],$val['value']);
                    }
                }
            }
        }

        $pemeriksaan = $pemeriksaan->get()->toArray();

        $pemeriksaan = ($pemeriksaan) ? $pemeriksaan : FALSE;

        return $pemeriksaan;
    }

}