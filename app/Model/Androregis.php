<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Androregis extends Model {

    protected $table = 'andro_regis';

    protected $fillable = [];

    protected $hidden = [];

    public static function registrasi($condition = array()){

        $select  = "andro_regis.id_pasien,";
        $select .= "andro_regis.norm,";
        $select .= "andro_regis.nama,";
        $select .= "andro_regis.noaskes,";
        $select .= "andro_regis.tglReg,";
        $select .= "andro_regis.jamReg,";
        $select .= "mst_poli.`name` as poliklinik,";
        $select .= "mst_poli.`poliid` as poliklinik_id,";
        $select .= "andro_regis.`status`,";
        $select .= "mst_rs.`name` as rs,";
        $select .= "andro_regis.statusRm,";
        $select .= "andro_regis.payment_method,";
        $select .= "andro_regis.helfa_no_antrian,";
        $select .= "andro_regis.helfa_queue_number,";
        $select .= "andro_regis.helfa_token";

        $history = DB::table('andro_regis')
                ->select(DB::raw($select))
                ->leftJoin('mst_poli', 'andro_regis.id_poliklinik', '=', 'mst_poli.poliid')
                ->leftJoin('mst_rs', 'andro_regis.id_rs', '=', 'mst_rs.id');

        if(count($condition) > 0 ){
            foreach ($condition as $rowgroup => $valgroup) {
                if($rowgroup == 'where'){
                    foreach ($valgroup as $val) {
                        $history->where($val['field'], $val['operator'],$val['value']);
                    }
                }
            }
        }

        $history = $history->get();

        $history = ($history) ? $history : FALSE;

        return $history;
    }

}

