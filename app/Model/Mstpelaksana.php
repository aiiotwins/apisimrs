<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Mstpelaksana extends Model {

    protected $table = 'mst_pelaksana';

    protected $fillable = [];

    protected $hidden = [];

    public static function getDataPelaksana($condition = array()){

    	// DB::connection()->enableQueryLog();

        $select  = 'mst_pelaksana.pelaksanaID as id_pelaksana,';
        $select .= 'mst_pelaksana.pelaksanaName as nama_pelaksana,';
        $select .= 'mst_pelaksana.group';

        $user = DB::table('mst_pelaksana')
                ->select(DB::raw($select))
                ->leftJoin('mst_unit', 'mst_pelaksana.unitID', '=', 'mst_unit.unitID');

        if(count($condition) > 0 ){
            foreach ($condition as $rowgroup => $valgroup) {
                if($rowgroup == 'where'){
                    foreach ($valgroup as $val) {
                        $user->where($val['field'], $val['operator'],$val['value']);
                    }
                }
            }
        }

        $user = $user->get();

        // print_r(DB::getQueryLog());
        // exit;

        $user = ($user) ? $user : FALSE;

        return $user;
    }

}

