<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Rjalan extends Model {

    protected $table = 'register_rjalan';

    protected $fillable = [];

    protected $hidden = [];

    public static function detail($noreg, $where = array()){

    	$select  = 'a.id_pasien,';
    	$select .= 'a.norm as no_rekam_medik,';
    	$select .= 'UPPER(b.NAMA) as nama_pasien,';
    	$select .= 'b.TGLAHIR as tanggal_lahir,';
		$select .= 'IF(b.KELAMIN = \'L\', \'LAKI-LAKI\', \'PEREMPUAN\') as jns_kelamin,';
		$select .= 'YEAR(CURDATE()) - YEAR(b.TGLAHIR) as umur,';
        $select .= 'a.noreg as no_pendaftaran,';
		$select .= 'a.tglReg as tanggal_pendaftaran,';
        $select .= '"0" as kelaspelayanan_id,';
        $select .= '"NON KELAS" as kelaspelayanan_nama,';
		$select .= 'a.jns_pembayaran as carabayar_id,';
		$select .= 'c.golpasName as carabayar_nama,';
        $select .= 'i.group as group_penjamin,';
        $select .= 'b.GOLPASKD as penjamin_id,';
        $select .= 'i.golpasName as penjamin_nama,';
        $select .= '"" as jeniskasuspenyakit_nama,';
		$select .= 'a.poliID as ruangan_id,';
		$select .= 'e.name as ruangan_nama,';
		$select .= 'a.dokter as dokter_id,';
		$select .= 'CONCAT(f.title, \' \', UPPER(f.nama), \' \', f.spesialisName) as dokter_nama,';
		$select .= 'UPPER(a.userCreated) as nama_pegawai,';
        $select .= '"0" as status_ranap,';
		$select .= 'b.ALAMAT1 as alamat,';
		$select .= 'b.SATUANKT as satuan,';
		$select .= 'g.pangkatID as pangkat_id,';
		$select .= 'g.name as pangkat_nama,';
		$select .= 'h.kode as kode_diagnosa,';
        $select .= 'h.nama as nama_diagnosa,';
        $select .= 'b.NIK as NIK,';
        $select .= 'b.AGAMA as id_agama,';
        $select .= 'm.name as nama_agama,';
        $select .= 'b.GOLDARAH as gol_darah,';
        $select .= 'b.TELP as telp_pasien,';
        $select .= 'j.statusNikahID as id_status_perkawinan,';
        $select .= 'j.name as status_perkawinan,';
        $select .= 'b.SUKU as id_suku,';
        $select .= 'n.name as nama_suku,';
        $select .= 'b.DIDIK as id_pendidikan,';
        $select .= 'o.name as nama_pendidikan,';
        $select .= 'b.PKERJAAN as id_pekerjaan,';
        $select .= 'p.name as nama_pekerjaan,';
        $select .= 'l.id as id_provinsi,';
        $select .= 'l.name as nama_provinsi,';
        $select .= 'k.id as id_kota,';
        $select .= 'k.name as nama_kota,';
        $select .= 'q.id as id_kecamatan,';
        $select .= 'q.name as nama_kecamatan,';
        $select .= 'r.id as id_kelurahan,';
        $select .= 'r.name as nama_kelurahan,';
        $select .= 'b.NEGARA as id_negara,';
        $select .= 's.name as nama_negara,';
        $select .= 't.kunjungan_ke as kunjungan_ke';

		$user = DB::table('register_rjalan as a')
                ->select(DB::raw($select))
                ->leftJoin('mst_pasien as b', 'b.id_pasien', '=', 'a.id_pasien')
                ->leftJoin('mst_golpas as c', 'c.golpasID', '=', 'a.jns_pembayaran')
                ->leftJoin('mst_grouppas as d', 'd.id', '=', 'c.group')
                ->leftJoin('mst_ruang_tindakan as e', 'e.id', '=', 'a.poliID')
                ->leftJoin('mst_dokter as f', 'f.dokterID', '=', 'a.dokter')
                ->leftJoin('mst_pangkat as g', 'g.pangkatID', '=', 'b.PANGKT')
                ->leftJoin('mst_reficd10 as h', 'h.kode', '=', 'a.diagMasuk')
                ->leftJoin('mst_golpas as i', 'i.golpasID', '=', 'b.GOLPASKD')
                ->leftJoin('mst_status_nikah as j', 'j.statusNikahID', '=', 'b.KAWIN')
                ->leftJoin('mst_kota as k', 'k.id', '=', 'b.ID_KOTA')
                ->leftJoin('mst_provinsi as l', 'l.id', '=', 'k.id_provinsi')
                ->leftJoin('mst_agama as m', 'm.agamaID', '=', 'b.AGAMA')
                ->leftJoin('mst_suku as n', 'n.id', '=', 'b.SUKU')
                ->leftJoin('mst_pendidikan as o', 'o.pendidikanID', '=', 'b.DIDIK')
                ->leftJoin('mst_pekerjaan as p', 'p.pekerjaanid', '=', 'b.PKERJAAN')
                ->leftJoin('mst_kecamatan as q', 'q.id_kota', '=', 'k.id')
                ->leftJoin('mst_kelurahan as r', 'r.id_kecamatan', '=', 'q.id')
                ->leftJoin('mst_negara as s', 's.id', '=', 'b.NEGARA')
                ->leftJoin('tr_kunjungan_poli as t', 't.noreg', '=', 'a.noreg')
                ->where('a.noreg', $noreg)
                ->first();

        $user = ($user) ? $user : FALSE;

        return $user;
	}

    public static function listreg($tgl){

        $select  = 'a.noreg as no_pendaftaran,';
        $select .= 'a.id_pasien,';
        $select .= 'a.norm as no_rekam_medik,';
        $select .= 'UPPER(b.NAMA) as nama_pasien,';
        $select .= 'a.tglReg as tanggal_pendaftaran,';
        $select .= 'b.TGLAHIR as tanggal_lahir,';
        $select .= 'a.poliID as ruangan_id,';
        $select .= 'c.name as ruangan_nama,';
        $select .= 'a.jns_pembayaran as carabayar_id,';
        $select .= 'e.golpasName as carabayar_nama,';
        $select .= 'f.name as carabayar_group,';
        $select .= 'UPPER(a.userCreated) as nama_pegawai,';
        $select .= 'd.name as rs,';
        $select .= 'UPPER(a.status) as status';

        $user = DB::table('register_rjalan as a')
                ->select(DB::raw($select))
                ->leftJoin('mst_pasien as b', 'b.id_pasien', '=', 'a.id_pasien')
                ->leftJoin('mst_poli as c', 'c.poliid', '=', 'a.poliID')
                ->leftJoin('mst_rs as d', 'd.id', '=', 'a.KODE_RS')
                ->leftJoin('mst_golpas as e', 'e.golpasID', '=', 'a.jns_pembayaran')
                ->leftJoin('mst_grouppas as f', 'f.id', '=', 'e.group')
                ->where('a.tglReg', $tgl)
                ->get();

        $user = ($user) ? $user : FALSE;

        return $user;
    }

    public static function listbynorm($norm){

        $select  = "a.noreg,";
        $select .= "a.norm,";
        $select .= "b.NAMA as nama_pasien,";
        $select .= "a.tglReg as tgl_registrasi,";
        $select .= "c.`name` as ruangan,";
        $select .= "a.`status`,";
        $select .= "d.`name` as rs,";
        $select .= "f.`name` as grouppasien";

        $user = DB::table('register_rjalan as a')
                ->select(DB::raw($select))
                ->leftJoin('mst_pasien as b', 'b.id_pasien', '=', 'a.id_pasien')
                ->leftJoin('mst_poli as c', 'c.poliid', '=', 'a.poliID')
                ->leftJoin('mst_rs as d', 'd.id', '=', 'a.KODE_RS')
                ->leftJoin('mst_golpas as e', 'e.golpasID', '=', 'a.jns_pembayaran')
                ->leftJoin('mst_grouppas as f', 'f.id', '=', 'e.group')
                ->where('a.norm', $norm)
                ->orderBy('a.tglReg', 'desc')
                ->get();

        $user = ($user) ? $user : FALSE;

        return $user;
    }

    public static function registrasi($condition = array()){

        $select  = 'a.noreg as no_pendaftaran,';
        $select .= 'a.id_pasien,';
        $select .= 'a.norm as no_rekam_medik,';
        $select .= 'UPPER(b.NAMA) as nama_pasien,';
        $select .= 'a.tglReg as tanggal_pendaftaran,';
        $select .= 'b.KELAMIN as id_kelamin,';
        $select .= 'if(b.KELAMIN = "L", "LAKI-LAKI", "PEREMPUAN") as jns_kelamin,';
        $select .= 'b.TGLAHIR as tanggal_lahir,';
        $select .= '(YEAR(CURDATE())-YEAR(b.TGLAHIR)) as usia,';
        $select .= 'a.poliID as ruangan_id,';
        $select .= 'i.pelaksanaID as dokter_id,';
        $select .= 'i.pelaksanaName as dokter_name,';
        $select .= 'c.name as ruangan_nama,';
        $select .= 'a.jns_pembayaran as carabayar_id,';
        $select .= 'e.golpasName as carabayar_nama,';
        $select .= 'f.name as carabayar_group,';
        $select .= 'UPPER(a.userCreated) as nama_pegawai,';
        $select .= 'd.name as rs,';
        $select .= 'UPPER(a.status) as status,';
        $select .= 'a.helfa_no_antrian as helfa_no_antrian,';
        $select .= 'a.helfa_token as helfa_token,';
        $select .= 'f.id as type,';
        $select .= 'b.NOASKES as noaskes,';
        $select .= 'h.kunjungan_ke as kunjungan_ke';

        $user = DB::table('register_rjalan as a')
                ->select(DB::raw($select))
                ->leftJoin('mst_pasien as b', 'b.id_pasien', '=', 'a.id_pasien')
                ->leftJoin('mst_poli as c', 'c.poliid', '=', 'a.poliID')
                ->leftJoin('mst_rs as d', 'd.id', '=', 'a.KODE_RS')
                ->leftJoin('mst_golpas as e', 'e.golpasID', '=', 'a.jns_pembayaran')
                ->leftJoin('mst_grouppas as f', 'f.id', '=', 'e.group')
                ->leftJoin('mst_ruang_tindakan as g', 'g.id', '=', 'c.poliid')
                ->leftJoin('tr_kunjungan_poli as h', 'h.noreg', '=', 'a.noreg')
                ->leftJoin('mst_pelaksana as i', 'i.pelaksanaID', '=', 'a.dokter');

        if(count($condition) > 0 ){
            foreach ($condition as $rowgroup => $valgroup) {
                if($rowgroup == 'where'){
                    foreach ($valgroup as $val) {
                        $user->where($val['field'], $val['operator'],$val['value']);
                    }
                }
            }
        }

        $user = $user->get();

        $user = ($user) ? $user : FALSE;

        return $user;
    }

    public static function cekRegistrasiWatlan($data = array()){

        $select  = "a.noreg as noreg,";
        $select .= "b.KODE as norm,";
        $select .= "b.NAMA as nama_pasien,";
        $select .= "a.tglReg as tgl_registrasi,";
        $select .= "a.`status` as status,";
        $select .= "a.KODE_RS as rs,";
        $select .= "c.name as poliklinik";

        $query = DB::table('register_rjalan as a')
                ->select(DB::raw($select))
                ->leftJoin('mst_pasien as b', 'b.id_pasien', '=', 'a.id_pasien')
                ->leftJoin('mst_poli as c', 'c.poliid', '=', 'a.poliID');
        foreach ($data as $key => $value) {
            $query->where($key, $value);
        }
        $result = $query->first();
        return $result;
    }

    protected static function getTimestamp(){
        return microtime(true);
    }

    public static function saveWatlan($data = array()){

        date_default_timezone_set('Asia/Jakarta');

        $timestamp = self::getTimestamp();

        $storeNoreg = DB::select(DB::raw('SELECT DISTINCTROW
                    concat(
                        DATE_FORMAT(now(), \'RJ%y%m%d\'),
                        lpad(
                            COALESCE (max(RIGHT(aa.noreg, 5)) + 1, 1),
                            5,
                            \'0\'
                        )
                    ) as a INTO @qNoreg
                    FROM
                        register_rjalan as aa
                    WHERE
                        LEFT (aa.noreg, 8) = DATE_FORMAT(now(), \'RJ%y%m%d\')'));

        $storeNoreg = DB::select("SELECT @qNoreg as lastNoreg");

        $insert = DB::table('register_rjalan')->insert(
                    [
                        'noreg'             => $storeNoreg[0]->lastNoreg,
                        'norm'              => $data['norm'],
                        'id_pasien'         => $data['id_pasien'],
                        'noAskes'           => $data['noAskes'],
                        'poliID'            => $data['poliID'],
                        'tglReg'            => date('Y-m-d'),
                        'diagMasuk'         => $data['diagMasuk'],
                        'perhatian'         => $data['perhatian'],
                        'jamReg'            => date('H:i:s'),
                        'userCreated'       => 'Anjungan Helfa',
                        'dateCreated'       => date('Y-m-d H:i:s'),
                        'status'            => 'open',
                        'jns_pembayaran'    => $data['jns_pembayaran'],
                        'timestamp'         => $timestamp,
                        'noSEP'             => $data['noSEP'],
                        'KODE_RS'           => $data['KODE_RS'],
                        'dokter'            => $data['dokter'],
                        'group_reg'         => $storeNoreg[0]->lastNoreg,
                        'shift'             => '1',
                        'helfa_no_antrian'  => $data['helfa_no_antrian'],
                        'helfa_token'       => $data['helfa_token'],
                        'helfa_queue_number'=> $data['helfa_queue_number']
                    ]
                );

        return $result = ($insert) ? self::getNoreg($timestamp) : FALSE;
    }

    public static function getNoreg($timestamp){

        $select  = "register_rjalan.noreg";

        $noreg = DB::table('register_rjalan')
                ->select(DB::raw($select))
                ->where('register_rjalan.timestamp', $timestamp)
                ->first();

        $noreg = ($noreg) ? $noreg : FALSE;

        return $noreg;
    }

    public static function lastCountKunjunganPoli($data){

    }

    //UPDATE DATA
    public static function registrasiUpdate($condition, $data){

        // DB::connection()->enableQueryLog();

        $update = DB::table('register_rjalan');

        if(count($condition) > 0 ){
            foreach ($condition as $rowgroup => $valgroup) {
                if($rowgroup == 'where'){
                    foreach ($valgroup as $val) {
                        $update->where($val['field'], $val['operator'],$val['value']);
                    }
                }
            }
        }

        $run = $update->update(['dokter' => @$data['data']['dokterDpjp']]);

        $result = ($run) ? $run : FALSE;

        return $result;
    }

}

