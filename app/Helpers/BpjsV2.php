<?php
namespace App\Helpers;

Class BpjsV2
{
    public static function getHeader($form_url_encoded = false)
    {
        if ($form_url_encoded)
            $conten_type = "Content-Type: application/x-www-form-urlencoded";
        else
            $conten_type = "Content-Type: application/json";

        return [
            "X-cons-id:".env('CONST_BPJSV2', 'forge'),
            "X-timestamp:".self::getTimestamp(),
            "X-signature:".self::getSignature(),
            "user_key:".env('USER_KEYV2', 'forge'),
            $conten_type
        ];
    }

    public static function getTimestamp()
    {
        date_default_timezone_set('UTC');
        return strval(time()-strtotime('1970-01-01 00:00:00'));
    }

    public static function getSignature()
    {
        // Computes the signature by hashing the salt with the secret key as the key
        $signature = hash_hmac(
            'sha256',
            env('CONST_BPJSV2', 'forge') . "&" . self::getTimestamp(),
            env('KEY_BPJSV2', 'forge'),
            true
        );

        return base64_encode($signature);
    }

    public static function out($data)
    {
        return $data && is_string($data) && json_decode($data) ? json_decode($data, true) : "";
    }

    public static function getKeyDecrypt()
    {
        $keyDecrypt = env('CONST_BPJSV2', 'forge').''.env('KEY_BPJSV2', 'forge').''.self::getTimestamp();

        return $keyDecrypt;
    }

    // function decrypt
    public static function stringDecrypt($key, $string){
        $encrypt_method = 'AES-256-CBC';
    
        // hash
        $key_hash = hex2bin(hash('sha256', $key));
  
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);

        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);
  
        return \LZCompressor\LZString::decompressFromEncodedURIComponent($output);
    }

    public static function curl($url, $data, $header, $action = '')
    {

        $curl = curl_init($url);

        self::getKeyDecrypt();
        
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);

        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            if (!empty($action)) {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $action);
            }
        }

        $res = json_decode(curl_exec($curl), true);

        $curl_get_info = curl_getinfo($curl);
        $get_response_time = !empty($curl_get_info['total_time']) ? $curl_get_info['total_time'] : 0;

        if (!$res) {
            $errorArray = [
                'metadata'=>[
                    "code"=>500,
                    "message"=>curl_error($curl) ? : "Problem with bridging",
                ]
            ];
            $res = json_encode($errorArray);
            // die('Error: "'.curl_error($curl).'" - Code: '.curl_errno($curl));
        }

        curl_close($curl);

        $metaData['metaData'] = $res['metaData'];

        $hasil['response'] = json_decode(self::stringDecrypt(self::getKeyDecrypt(), $res['response']));
          
        $result = json_encode(array_merge($metaData,$hasil),JSON_PRETTY_PRINT); 
        
        return $result; 

    }

}

?>