<?php

namespace App\Helpers;

class Helfa
{
	public static function getLocalTime(){

		$curdate = date("Y-m-d H:i:s", strtotime("+7 hours"));

		return date($curdate);
	}

	public static function gettoken(){

		$url = "http://192.168.2.35:8888/api/helfa/token";
		$curl = curl_init($url);

		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, self::getHeader(true));
		curl_setopt($curl, CURLOPT_VERBOSE, 1);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,0);
		curl_setopt($curl, CURLOPT_TIMEOUT, 500);

		$res = curl_exec($curl);
		//curl_close($curl);
		$result = json_encode($res);


		$curl_get_info = curl_getinfo($curl);
		$get_response_time = !empty($curl_get_info['total_time']) ? $curl_get_info['total_time'] : 0;

		if (!$res) {
		    $errorArray = [
		        'metadata'=>[
		            "code"=>500,
		            "message"=>curl_error($curl) ? : "Problem with bridging",
		        ]
		    ];
		    $res = json_encode($errorArray);
		}
		curl_close($curl);

		return $res;
	}

	public static function getnumber($data){

		$url = "http://192.168.2.35:8888/api/helfa/get-number";
		$curl = curl_init($url);

		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, self::getHeader(false));
		curl_setopt($curl, CURLOPT_VERBOSE, 1);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,0);
		curl_setopt($curl, CURLOPT_TIMEOUT, 500);

		if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        }

		$res = curl_exec($curl);
		//curl_close($curl);
		$result = json_encode($res);

		$curl_get_info = curl_getinfo($curl);
		$get_response_time = !empty($curl_get_info['total_time']) ? $curl_get_info['total_time'] : 0;

		if (!$res) {
		    $errorArray = [
		        'metadata'=>[
		            "code"=>500,
		            "message"=>curl_error($curl) ? : "Problem with bridging",
		        ]
		    ];
		    $res = json_encode($errorArray);
		}
		curl_close($curl);

		return $res;
	}

	protected static function getHeader($form_url_encoded = false)
    {
        if ($form_url_encoded)
            $conten_type = "Content-Type:application/x-www-form-urlencoded";
        else
            $conten_type = "Content-Type:application/json";

        return [
            "X-user:" . "helfa",
            "X-timestamp:" . self::getTimestamp(),
            "X-signature:" . self::getSignature(),
            $conten_type
        ];
    }

    protected static function getTimestamp()
    {
        date_default_timezone_set('UTC');
        return strval(time()-strtotime('1970-01-01 00:00:00'));
    }

    protected static function getSignature()
    {

        // Computes the signature by hashing the salt with the secret key as the key
        $signature = hash_hmac(
            'sha256',
            self::getuser() . "&" . self::getTimestamp(),
            self::getkey(),
            true
        );

        return base64_encode($signature);
    }

    protected static function getuser(){
    	return "helfa";
    }

    protected static function getkey(){
    	return "hel123fa";
    }
}