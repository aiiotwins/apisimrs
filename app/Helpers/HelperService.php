<?php

namespace App\Helpers;
use DateTime;
use DateInterval;
use DatePeriod;

class HelperService
{
    public static function _success($customMessage = 'success')
    {
        $result = array(
            'response_code' => 200,
            'message' => $customMessage,
        );
        return $result;
    }

    public static function _created()
    {
        $result = array(
            'response_code' => 201,
            'message' => 'created',
        );
        return $result;
    }

    public static function _noContent()
    {
        $result = array(
            'response_code' => 204,
            'message' => 'No Content',
        );

        return $result;
    }

    public static function _badRequest()
    {
        $result = array(
            'response_code' => 400,
            'message' => 'Bad Request',
        );

        return $result;
    }

    public static function _unauthorized()
    {
        $result = array(
            'response_code' => 401,
            'message' => 'Unauthorized error',
        );

        return $result;
    }

    public static function _forbidden()
    {
        $result = array(
            'response_code' => 403,
            'message' => 'Forbidden',
        );

        return $result;
    }

    public static function _notFound()
    {
        $result = array(
            'response_code' => 404,
            'message' => 'Not Found',
        );
        return $result;
    }

    public static function _methodNotAllowed()
    {
        $result = array(
            'response_code' => 405,
            'message' => 'Method Not Allowed',
        );

        return $result;
    }

    public static function _conflict()
    {
        $result = array(
            'response_code' => 409,
            'message' => 'Conflict',
        );

        return $result;
    }

    public static function _internalServerError($customMessage = 'Internal Server Error')
    {
        $result = array(
            'response_code' => 500,
            'message' => $customMessage,
        );

        return $result;
    }

    public static function _noToken()
    {
        $result = array(
            'response_code' => 405,
            'message' => 'Please provide your token key'
        );
        return $result;
    }

    public static function _sessionExpired()
    {
        $result = array(
            'response_code' => 401,
            'message' => 'Your session expired, Please login to continue'
        );
        return $result;
    }

    public static function _getWrong()
    {
        $result = array(
            'response_code' => 409,
            'message' => 'Something Wrong. Please Call The IT Team'
        );
        return $result;
    }

    public static function _fullTimeToSecond($str_time)
    {
        // $str_time = "23:12:95";
        $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);

        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);

        return $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;

    }

     public static function _timeToTextHourMinute($numberOfSecond)
    {
        $data = substr('00' . ($numberOfSecond / 3600 % 24), -2)
        . ' hour ' . substr('00' . ($numberOfSecond / 60 % 60), -2)
            . ' minute';
        return $data;
    }

    public static function _timeToTextNormalHourMinute($numberOfSecond)
    {
        $data = substr('00' . ($numberOfSecond / 3600 % 24), -2)
        . ':' . substr('00' . ($numberOfSecond / 60 % 60), -2);
        return $data;
    }

    public static function _hoursToMinutes($hours)
    {
        if (strstr($hours, ':')) {
            # Split hours and minutes.
            $separatedData = explode(':', $hours);

            $minutesInHours = $separatedData[0] * 60;
            $minutesInDecimals = $separatedData[1];

            $totalMinutes = $minutesInHours + $minutesInDecimals;
        } else {
            $totalMinutes = $hours * 60;
        }

        return $totalMinutes;
    }

    public static function _imagePath($branch_id, $directory, $image)
    {
        $path = '';
        if ($image) {
            $path = env('IMG_PUBLIC_PATH') . $branch_id . "/".$directory."/" . $image;
        }
        return $path;
    }

    public static function _datePeriod($from, $to)
    {
        $begin = new DateTime($from);
        $end = new DateTime($to);
        $end = $end->modify('+1 day');

        $interval = new DateInterval('P1D');
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        return $period;
    }

    public static function array_sort_by_column(&$arr, $col, $dir = SORT_ASC)
    {
        $sort_col = array();

        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row[$col];
        }
        array_multisort($sort_col, $dir, $arr);
    }

    public static function listMonth($type)
    {
        $result['number'] = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        $result['name'] = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        $result['alias'] = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

        if ($type == 'object') {
            for ($i = 0; $i < count($result['number']); $i++) {
                $result['object'][$result['number'][$i]] = [
                                        'number' => $result['number'][$i],
                                        'name' => $result['name'][$i],
                                        'alias' => $result['alias'][$i],
                                    ];
            }
        }

        return $result[$type];
    }

    public static function generateRandomString($length, $isNumeric = false) {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if ($isNumeric) {
            $characters = '0123456789';
        }

        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function _successResponse($data, $message = 'Ok'){
        $result = ['response' => $data, 'metadata' => ['messages' => $message, 'code' => 200]];

        return response()->json($result);
    }

    public static function _failResponse(){
        $result = ['response' => null, 'metadata' => ['messages' => 'Gagal', 'code' => 201]];

        return response()->json($result);
    }
}