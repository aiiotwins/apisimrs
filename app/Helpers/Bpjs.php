<?php
Class Bpjs
{
    public static function getHeader($form_url_encoded = false)
    {
        if ($form_url_encoded)
            $conten_type = "Content-Type: application/x-www-form-urlencoded";
        else
            $conten_type = "Content-Type: application/json";

        return [
            "X-cons-id : " . env('CONST_BPJS', 'forge'),
            "X-timestamp : " . self::getTimestamp(),
            "X-signature : " . self::getSignature(),
            $conten_type
        ];
    }

    public static function getTimestamp()
    {
        date_default_timezone_set('UTC');
        return strval(time()-strtotime('1970-01-01 00:00:00'));
    }

    public static function getSignature()
    {
        // Computes the signature by hashing the salt with the secret key as the key
        $signature = hash_hmac(
            'sha256',
            env('CONST_BPJS', 'forge') . "&" . self::getTimestamp(),
            env('KEY_BPJS', 'forge'),
            true
        );

        return base64_encode($signature);
    }

    public static function out($data)
    {
        return $data && is_string($data) && json_decode($data) ? json_decode($data, true) : "";
    }

    public static function curl($url, $data, $header, $action = '')
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);

        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            if (!empty($action)) {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $action);
            }
        }

        $res = curl_exec($curl);

        $curl_get_info = curl_getinfo($curl);
        $get_response_time = !empty($curl_get_info['total_time']) ? $curl_get_info['total_time'] : 0;

        if (!$res) {
            $errorArray = [
                'metadata'=>[
                    "code"=>500,
                    "message"=>curl_error($curl) ? : "Problem with bridging",
                ]
            ];
            $res = json_encode($errorArray);
            // die('Error: "'.curl_error($curl).'" - Code: '.curl_errno($curl));
        }
        curl_close($curl);

        return $res;
    }

    //SEP
    public static function createSEP($data){

        date_default_timezone_set('Asia/Jakarta');

        $dpjp = self::getDPJP($data['dpjp']);

        $asalRujukan = (substr($data['response']['rujukan'][0]['provPerujuk']['kode'], 4,1) != 'R') ? '1':'2';
        $noSurat = str_pad(rand(1000, 9999), 6, '0', STR_PAD_LEFT);;

        $array = array (
            'request' =>
                        array (
                            't_sep' =>
                                    array (
                                        'noKartu'       => $data['response']['rujukan'][0]['peserta']['noKartu'],
                                        'tglSep'        => date('Y-m-d'),
                                        'ppkPelayanan'  => '0901R003',
                                        'jnsPelayanan'  => "".$data['response']['rujukan'][0]['pelayanan']['kode']."",
                                        'klsRawat'      => "".$data['response']['rujukan'][0]['peserta']['hakKelas']['kode']."",
                                        'noMR'          => $data['norm'],
                                        'rujukan'       =>  array (
                                                                'asalRujukan' => "".$asalRujukan."",
                                                                'tglRujukan' => $data['response']['rujukan'][0]['tglKunjungan'],
                                                                'noRujukan' => $data['response']['rujukan'][0]['noKunjungan'],
                                                                'ppkRujukan' => $data['response']['rujukan'][0]['provPerujuk']['kode'],
                                                            ),
                                        'catatan'       => '',
                                        'diagAwal'      => $data['response']['rujukan'][0]['diagnosa']['kode'],
                                        'poli'          =>
                                                            array (
                                                                'tujuan' => $data['response']['rujukan'][0]['poliRujukan']['kode'],
                                                                'eksekutif' => '0',
                                                            ),
                                        'cob'           =>
                                                            array (
                                                                'cob' => '0',
                                                            ),
                                        'katarak'           =>
                                                            array (
                                                                'katarak' => '0',
                                                            ),
                                        'jaminan'       =>
                                                            array (
                                                                'lakaLantas' => '0',
                                                                'penjamin' => array(
                                                                    'penjamin' => '',
                                                                    'tglKejadian' => date('Y-m-d'),
                                                                    'keterangan' => '',
                                                                    'suplesi' => array(
                                                                        'suplesi' =>  '0',
                                                                        'noSepSuplesi' => '',
                                                                        'lokasiLaka' => array(
                                                                            'kdPropinsi' => '',
                                                                            'kdKabupaten' => '',
                                                                            'kdKecamatan' => '',
                                                                        )
                                                                    )
                                                                )
                                                            ),
                                        'skdp' => array (
                                            'noSurat' => $noSurat,
                                            'kodeDPJP' => $dpjp,
                                         ),
                                        'noTelp'        => $data['response']['rujukan'][0]['peserta']['mr']['noTelepon'],
                                        'user'          => 'Anjungan Helfa',
                                    ),
                        )
        );  

        $jsonData = json_encode($array);

        $param = 'SEP/1.1/insert';

        $hasil = self::out(self::curl(env('URL_BPJS', 'forge') . $param, $jsonData, Bpjs::getHeader(true), 'POST'));

        return $hasil;
    }

    //REFERENSI
    public static function getDPJP($id_poli_vclaim){

        //GET DATA DOKTER DPJP
        $dpjp   = $id_poli_vclaim;
        $tglsep = date('Y-m-d');
        $param  = "referensi/dokter/pelayanan/2/tglPelayanan/{$tglsep}/Spesialis/{$dpjp}";
        $dpjp = Bpjs::out(Bpjs::curl(env('URL_BPJS', 'forge') . $param, false, Bpjs::getHeader(true)));

        $index_start = 0;
        $end_start   = count($dpjp['response']['list']) - 1;

        $index_random = rand($index_start, $end_start); 
        
        $dpjp = $dpjp['response']['list'][$index_random]['kode'];

        return $dpjp;
    }

    //HISTORY
    public static function getHistoryPelayanan($nobpjs){

        $tglmulai = date('Y-m-d', strtotime('-2 months'));
        $tglakhir = date('Y-m-d', strtotime('+2 months'));

        $param    = "monitoring/HistoriPelayanan/NoKartu/{$nobpjs}/tglAwal/{$tglmulai}/tglAkhir/{$tglakhir}";

        $hasil = Bpjs::out(Bpjs::curl(env('URL_BPJS', 'forge') . $param, false, Bpjs::getHeader(true)));

        return $hasil;
    }
}

?>