<?php

Class Libdoco
{

    public static function getUsername(){
    	return 'rspad';
    }

    public static function getKey(){
    	return 'UvbxfAwfRRSBDsjj';
    }

    public static function rspadEncryptor($data=array())
    {

    	$data = [
			'username'       => self::getUsername(),
			'timestamp'      => date('Y-m-d H:00:00'),
			'dokter_id'      => 1,
			'dokter_nama'    => 'Admin'
		];

        $secretKey = self::getKey();
        // $secretKey = 'UvbxfAwfRRSBDsjj';

        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

        // Create token payload as a JSON string
        $payload = json_encode($data);

        // Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secretKey, true);

        // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        return $jwt;
        // return rtrim(strtr(base64_encode(serialize($data)), '+/', '-_'), '=');
    }
}

?>