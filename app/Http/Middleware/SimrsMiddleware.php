<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\Userapi;

class SimrsMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        date_default_timezone_set('UTC');
        $user       = $request->header('X-user');
        $tStamp     = $request->header('X-timestamp');
        $signature  = $request->header('X-signature');

        if($request->header('xUser')){
            $user   = $request->header('xUser');
        }
        if($request->header('xTimestamp')){
            $tStamp   = $request->header('xTimestamp');
        }
        if($request->header('xSignature')){
            $signature= $request->header('xSignature');
        }

        //CEK USER KE DATABASE
        $datauser = Userapi::where('username', $user)->first();

        //JIKA DATA USER TIDAK DITEMUKAN
        if(empty($datauser)){
            return response()->json(['code' => 401, 'message' => 'Not Authorized']);
        }

        //GET SIGNATURE
        $decode64 = base64_decode($signature);

        //JIKA SIGNATURE TIDAK SESUAI
        if($decode64 != hash_hmac('sha256', $user."&".$tStamp, $datauser->token, true)){
            return response()->json(['code' => 401, 'message' => 'Invalid Signature']);
        }

        //HITUNG BATAS WAKTU AKSES API SELAMA 5 MENIT SEJAK REQUEST API
        $tReq = date('Y-m-d H:i:s', $tStamp);

        //$eReq = date('Y-m-d H:i:s', strtotime("+5 minutes", strtotime($tReq)));

        $diff  = date_diff(date_create(date('Y-m-d H:i:s', time())), date_create($tReq));

        //JIKA MELEBIHI WAKTU YANG DITENTUKAN DALAM MENGAKSES API
        //if($diff->i == 1 || $diff->i >= 5){
        // if($diff->i >= 10){
        //     return response()->json(['code' => 401, 'message' => 'Expired Service']);
        // }

        return $next($request);
    }

}