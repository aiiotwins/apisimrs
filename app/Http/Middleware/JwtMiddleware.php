<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\Userapi;
use \Firebase\JWT\JWT;

class JwtMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $key   = '4p!5!mR5';

        $jwt   = $request->header('token');

        if(empty($request->header('token'))){
             return response()->json(['code' => 401, 'message' => 'Not Authorized']);
        }

        // $payload = array(
        //     "user" => 'urologi',
        //     "pass" => 'uro123logi'
        // );

        // $jwt = JWT::encode($payload, $key);

        // print_r($jwt);
        // exit;

        $decoded = JWT::decode($jwt, $key, array('HS256'));

        //CEK USER KE DATABASE
        $datauser = Userapi::where('token', $decoded->pass)->where('username', $decoded->user)->first();

        if(empty($datauser)){
            return response()->json(['code' => 401, 'message' => 'Not Authorized']);
        }

        return $next($request);
    }

}