<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\Userapi;

class SimmutuMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        date_default_timezone_set('UTC');
        $user       = $request->header('X-user');
        $signature  = $request->header('X-signature');

        //CEK USER KE DATABASE
        $datauser = Userapi::where([['username', '=', $user],['token', '=', $signature]])->first();

        //JIKA DATA USER TIDAK DITEMUKAN
        if(empty($datauser)){
            return response()->json(['code' => 401, 'message' => 'Not Authorized']);
        }

        return $next($request);
    }

}