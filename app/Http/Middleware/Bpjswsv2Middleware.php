<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\Userapi;

class Bpjswsv2Middleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        date_default_timezone_set('UTC');

        $user      = $request->header('x-username');
        $password  = $request->header('x-password');
        $token     = $request->header('x-token');

        if($user && $password){
            //CEK USER KE DATABASE
            $datauser = Userapi::where([['username', '=', $user],['token', '=', $password]])->first();
        }

        if($token){        
            $sendToken = base64_decode($token);
            $getString = explode("/",$sendToken);

            $datauser = Userapi::where([['username', '=', $user]])->first();

            if($getString[0] != hash_hmac('sha256', $datauser->username.$datauser->token, true)){
                return response()->json(['code' => 401, 'message' => 'Invalid Token']);
            }

            $diff  = date_diff(date_create(date('Y-m-d H:i:s', time())), date_create($getString[1]));

            if($diff->i > 5){
                return response()->json(['code' => 401, 'message' => 'Expired Service']);
            }
        }        

        //JIKA DATA USER TIDAK DITEMUKAN
        if(empty($datauser)){
            return response()->json(['code' => 402, 'message' => 'Not Authorized']);
        }

        return $next($request);
    }

}