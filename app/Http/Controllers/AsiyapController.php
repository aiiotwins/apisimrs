<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AsiyapController extends Controller
{
    public function getLogin(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);
      
        $select  = 'NIP,';
        $select .='kategori_user';

        $data = DB::connection('mysql4')->table('login_user')
        ->select(DB::raw($select))
        ->where('NIP', $data['nip'])
        ->where('password', $data['password'])
        ->get();

        if(count($data) > 0){
            $data = ['status' => 200, 'message' => 'OK', 'data' => $data[0]];
        }else{
            $data = ['status' => 500, 'message' => 'Failed', 'data' => 'Data Tidak ditemukan.'];
        }
        return response()->json($data);
    }

    public function allPersonil($nama= null){

        $select = 'nip,';
        $select .='Nama,';
        $select .= 'Pangkat';
        $personil = DB::connection('mysql4')->table('data_personil')
         ->select(DB::raw($select));

        if($nama !=  null){
            $personil->where('Nama', 'like', '%'.$nama.'%');
        }
                   

        $personil = $personil->get()->toArray();

        if($personil){
             $data = ['status' => 200, 'message' => 'OK', 'data' => $personil];
        }else{
            $data = ['status' => 500, 'message' => 'Failed (Data tidak ditemukan/Tidak ada)', 'data' => NULL];
        }

        return response()->json($data);        
    }

    public function personil($nip){

        //DB::connection('mysql3')->enableQueryLog();
        $select  = 'nip,';
        $select  .= 'nama,';
        $select  .= 'tempat_lahir,';
        $select  .= 'DATE_FORMAT(tgl_lahir,\'%d/%m/%Y\') tgl_lahir,';
        $select  .= 'kelamin,';
        $select  .= 'suku,';
        $select  .= 'agama,';
        $select  .= 'npwp,';
        $select  .= 'no_bpjs,';
        $select  .= 'no_tlp,';
        $select  .= 'email,';
        $select  .= 'pangkat,';
        $select  .= 'no_str,';
        $select  .= 'DATE_FORMAT(tahun_berlaku_STR,\'%Y\') tahun_berlaku_STR,';
        $select  .= 'no_sip,';
        $select  .= 'DATE_FORMAT(tahun_berakhir_sip,\'%Y\') tahun_berakhir_sip';

        $data = DB::connection('mysql4')->table('data_personil')
        ->where('data_personil.NIP', $nip)
        ->select(DB::raw($select))
        ->get();

        if(count($data) > 0){
            $data = ['status' => 200, 'message' => 'OK', 'data' => $data];
        }else{
            $data = ['status' => 500, 'message' => 'Failed (Data tidak ditemukan/Tidak ada)', 'data' => NULL];
        }

        // print_r(DB::connection('mysql3')->getQueryLog());
        // exit;

        return response()->json($data);
    }
    public function keluarga($nip){

        $select  = 'NIP,';
        $select .= 'nama_ayah,';
        $select .='nama_ibu,';
        $select .= 'nama_pasangan,';
        $select .='status_kawin,';
        $select .= 'tempat_lahir,';
        $select .= 'DATE_FORMAT(tgl_lahir,\'%d/%m/%Y\') tgl_lahir,';
        $select .= 'agama,';
        $select .='anak1,';
        $select .= 'anak2,';
        $select .='anak3,';
        $select .= 'anak4,';
        $select .='anak5';

        $data = DB::connection('mysql4')->table('data_keluarga')
        ->where('data_keluarga.NIP', $nip)
        ->select(DB::raw($select))
        ->get();

        if(count($data) > 0){
            $data = ['status' => 200, 'message' => 'OK', 'data' => $data];
        }else{
            $data = ['status' => 500, 'message' => 'Failed (Data tidak ditemukan/Tidak ada)', 'data' => NULL];
        }

        return response()->json($data);
    }
    public function kepegawaian($nip){

        //DB::connection('mysql3')->enableQueryLog();
       
        $select  = 'nip,';
        $select  .= 'status_personel,';
        $select  .= 'kategori_personel,';
        $select  .= 'DATE_FORMAT(tmt_kategori,\'%d/%m/%Y\') tmt_kategori,';
        $select  .= 'tmt_tni,';
        $select  .= 'sumber_perwira,';
        $select  .= 'DATE_FORMAT(tmt_perwira,\'%d/%m/%Y\') tmt_perwira';

        $data = DB::connection('mysql4')->table('kepegawaian')
        ->where('kepegawaian.NIP', $nip)
        ->select(DB::raw($select))
        ->get();

        if(count($data) > 0){
            $data = ['status' => 200, 'message' => 'OK', 'data' => $data];
        }else{
            $data = ['status' => 500, 'message' => 'Failed (Data tidak ditemukan/Tidak ada)', 'data' => NULL];
        }

        // print_r(DB::connection('mysql3')->getQueryLog());
        // exit;

        return response()->json($data);
    }
    public function kepangkatan($nip){

       
        $select = 'pangkat,';
        $select .= 'taun_pangkat,';
        $select .= 'no_skep';

        $data = DB::connection('mysql4')->table('kepangkatan')
        ->where('kepangkatan.NIP', $nip)
        ->select(DB::raw($select))
        ->get();
        /*$nArray = array();
        foreach ($data as $value) {
                $nArray[$value->NIP][] = array('pangkat' => $value->pangkat, 'taun_pangkat' => $value->taun_pangkat,'no_skep' =>$value->no_skep);
        }

        $output_arr = array();

        foreach($nArray as $key=>$value){
           $output_arr[]=array("NIP"=>$key,$value);
        }*/

        if(count($data) > 0){
            $data = ['status' => 200, 'message' => 'OK', 'data' => $data];
        }else{
            $data = ['status' => 500, 'message' => 'Failed (Data tidak ditemukan/Tidak ada)', 'data' => NULL];
        }
        return response()->json($data);
    }

    public function riwayatjabatan($nip){
       
        $select = 'nama_jabatan,';
        $select .= 'taun_jabatan,';
        $select .= 'no_skep';

        $data = DB::connection('mysql4')->table('riwayat_jabatan')
        ->where('riwayat_jabatan.NIP', $nip)
        ->select(DB::raw($select))
        ->get();

        /*$nArray = array();
        foreach ($data as $value) {
                $nArray[$value->NIP][] = array('nama_jabatan' => $value->nama_jabatan, 'taun_jabatan' => $value->taun_jabatan,'no_skep' =>$value->no_skep);
        }

        $output_arr = array();

        foreach($nArray as $key=>$value){
           $output_arr[]=array("NIP"=>$key,"riwayat"=>$value);
        }*/

        if(count($data) > 0){
            $data = ['status' => 200, 'message' => 'OK', 'data' => $data];
        }else{
            $data = ['status' => 500, 'message' => 'Failed (Data tidak ditemukan/Tidak ada)', 'data' => NULL];
        }
        return response()->json($data);
    }
    public function riwayatpendidikan($nip){
       
        $select = 'pendidikan,';
        $select .= 'tahun_didik';

        $data = DB::connection('mysql4')->table('riwayat_pendidikan')
        ->where('riwayat_pendidikan.NIP', $nip)
        ->select(DB::raw($select))
        ->get();

        /*$nArray = array();
        foreach ($data as $value) {
                $nArray[$value->NIP][] = array('pendidikan' => $value->pendidikan, 'tahun_didik' => $value->tahun_didik,'no_STR' =>$value->no_STR,'tahun_berlaku_STR' => $value->tahun_berlaku_STR,'no_SIP' => 'no_SIP', 'tahun_berakhir_SIP' => 'tahun_berakhir_SIP');
        }

        $output_arr = array();

        foreach($nArray as $key=>$value){
           $output_arr[]=array("NIP"=>$key,"riwayat"=>$value);
        }*/

        if(count($data) > 0){
            $data = ['status' => 200, 'message' => 'OK', 'data' => $data];
        }else{
            $data = ['status' => 500, 'message' => 'Failed (Data tidak ditemukan/Tidak ada)', 'data' => NULL];
        }
        return response()->json($data);
    }
    public function getHome(){
        $select  = '*';

        $data = DB::connection('mysql4')->table('mst_home')
        ->select(DB::raw($select))
        ->get();

        if(count($data) > 0){
            $data = ['status' => 200, 'message' => 'OK', 'data' => $data];
        }else{
            $data = ['status' => 500, 'message' => 'Failed (Data tidak ditemukan/Tidak ada)', 'data' => NULL];
        }
        return response()->json($data);
    }
    public function getHomeDetail($id_home){
        if($id_home == 1){
            $select  = 'mst_home.id_home,';
            $select .= 'mst_home.kategori_home';
        }else if($id_home == 2){            
            $select = 'detail_home_diklat.id_diklat,';
            $select .= 'detail_home_diklat.kategori_diklat';
        }else if($id_home == 3){               
            $select = 'detail_home_binkar.id_binkar,';
            $select .= 'detail_home_binkar.kategori_binkar';
        }else if($id_home == 4){            
            
        $select = 'detail_home_perawatan.id_perawatan,';
        $select .= 'detail_home_perawatan.kategori_perawatan';
        }else if($id_home == 5){            
            $select = 'detail_home_pemisahan.id_pemisah,';
            $select .= 'detail_home_pemisahan.kategori_pemisah';
        }

        $data = DB::connection('mysql4')->table('mst_home')
        ->leftJoin('detail_home_diklat', 'mst_home.id_home', '=', 'detail_home_diklat.id_mst_home')
        ->leftJoin('detail_home_binkar', 'mst_home.id_home', '=', 'detail_home_binkar.id_mst_home')
        ->leftJoin('detail_home_perawatan', 'mst_home.id_home', '=', 'detail_home_perawatan.id_mst_home')
        ->leftJoin('detail_home_pemisahan', 'mst_home.id_home', '=', 'detail_home_pemisahan.id_mst_home')
        ->where('mst_home.id_home', $id_home)
        ->select(DB::raw($select))
        ->get();

        /*$nArray = array();
        foreach ($data as $value) {
            if($value->id_home == '2'){                
                $nArray[$value->kategori_home][] = array('id_diklat' => $value->id_diklat, 'kategori_diklat' => $value->kategori_diklat);
            }else if($value->id_home == '3'){
                $nArray[$value->kategori_home][] = array('id_binkar' => $value->id_binkar, 'kategori_binkar' => $value->kategori_binkar);
            }else if($value->id_home == '4'){
                $nArray[$value->kategori_home][] = array('id_perawatan' => $value->id_perawatan, 'kategori_perawatan' => $value->kategori_perawatan);
            }else{
                $nArray[$value->kategori_home][] = null;
            }
        }

        $output_arr = array();

        foreach($nArray as $key=>$value){
           $output_arr[]=array("kategori_home"=>$key,"detail_home"=>$value);
        }
        */
        /*foreach ($output_arr as $value) {
            print_r($value['kategori_home'].'<br>');
        }
        exit();*/
        if(count($data) > 0){
            $data = ['status' => 200, 'message' => 'OK', 'data' => $data];
        }else{
            $data = ['status' => 500, 'message' => 'Failed (Data tidak ditemukan/Tidak ada)', 'data' => NULL];
        }
        return response()->json($data);
    }   
    
    public function detailperawatan(){
        $select  = '*';

        $data = DB::connection('mysql4')->table('detail_perawatan')
        ->select(DB::raw($select))
        ->get();

        if(count($data) > 0){
            $data = ['status' => 200, 'message' => 'OK', 'data' => $data];
        }else{
            $data = ['status' => 500, 'message' => 'Failed (Data tidak ditemukan/Tidak ada)', 'data' => NULL];
        }
        return response()->json($data);
    }
    public function detailperawatanlainnya(){
        $select  = '*';

        $data = DB::connection('mysql4')->table('detail_perawatan_lainnya')
        ->select(DB::raw($select))
        ->get();

        if(count($data) > 0){
            $data = ['status' => 200, 'message' => 'OK', 'data' => $data];
        }else{
            $data = ['status' => 500, 'message' => 'Failed (Data tidak ditemukan/Tidak ada)', 'data' => NULL];
        }
        return response()->json($data);
    }
}
