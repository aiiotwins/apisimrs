<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Rinap;
use App\Model\Rjalan;
use App\Model\Androregis;
use App\Model\Userapi;
use \Firebase\JWT\JWT;

class RegistrasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function listreg($jns_rawat, $tgl_reg){

        if($jns_rawat == 'rj'){
            $listreg = Rjalan::listreg($tgl_reg);
        }else{
            $listreg = Rinap::listreg($tgl_reg);
        }

        if($listreg){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $listreg, 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function listbynorm($jns_rawat, $norm){

        if($jns_rawat == 'rj'){
            $listnorm = Rjalan::listbynorm($norm);
        }else{
            $listnorm = Rinap::listbynorm($norm);
        }

        if($listnorm){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $listnorm, 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function detail($noreg){

        $jns_rawat = substr($noreg, 0, 2);

        if($jns_rawat == 'RJ'){
            $user = Rjalan::detail($noreg);
        }else{
            $user = Rinap::detail($noreg);
        }

        if($user){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $user, 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function gethistory($jns_rawat, $norm, $daritgl, $sampaitgl){

        $condition['where'][] = ['field' => 'a.norm', 'operator' => '=', 'value' => $norm];
        $condition['where'][] = ['field' => 'a.tglReg', 'operator' => '>=', 'value' => $daritgl];
        $condition['where'][] = ['field' => 'a.tglReg', 'operator' => '<=', 'value' => $sampaitgl];
        $condition['where'][] = ['field' => 'a.status', 'operator' => '<>', 'value' => 'canceled'];

        if($jns_rawat == 'RJ' || $jns_rawat == 'rj'){
            $user = Rjalan::registrasi($condition);
        }else{
            $user = Rinap::registrasi($condition);
        }

        if($user){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $user, 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function getData(Request $request){

        date_default_timezone_set("UTC");
        $key   = '4p!5!mR5';

        $jwt    = $request->header('token');
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        
        $username         = $decoded->user;
        $password         = $decoded->pass;
        $tStamp           = strval(time()-strtotime('1970-01-01 00:00:00'));
        $signature        = hash_hmac('sha256', $username."&".$tStamp, $password, true);
        $encodeSignature  = base64_encode($signature);

        $data = ['xUser' =>$username, 'xTimestamp' => $tStamp, 'xSignature' => $encodeSignature, 'datetime' => date('Y-m-d H:i:s')];

        return response()->json($data);

        // echo "X-user : ".$username."<br>";
        // echo "X-timestamp : ".$tStamp."<br>";
        // echo "X-signature : ".$encodeSignature."<br>";
        // exit;
    }

    public function getHistoryOnline($norm){

        $condition['where'][] = ['field' => 'andro_regis.norm', 'operator' => '=', 'value' => $norm];

        $history = Androregis::registrasi($condition);

        if($history){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $history, 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function getHistoryOnlineByDate($daritgl,$sampaitgl){
        
        $condition['where'][] = ['field' => 'andro_regis.tglReg', 'operator' => '>=', 'value' => $daritgl];
        $condition['where'][] = ['field' => 'andro_regis.tglReg', 'operator' => '<=', 'value' => $sampaitgl];

        $history = Androregis::registrasi($condition);

        if($history){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $history, 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function pencarian(Request $request){

        $rjalan = [];
        //$rinap  = [];
        $result = [];

        if(!empty($request->input())){
            $data = $request->input();
        }else{
            $data = json_decode(file_get_contents("php://input"), true);
        }   

        // DB::connection()->enableQueryLog();

        if(isset($data['id_pasien'])){
            $condition['where'][] = ['field' => 'a.id_pasien', 'operator' => '=', 'value' => $data['id_pasien']];
        }

        if(isset($data['norm'])){
            $condition['where'][] = ['field' => 'a.norm', 'operator' => '=', 'value' => $data['norm']];
        }

        if(isset($data['tgl_reg_from'])){
            $condition['where'][] = ['field' => 'a.tglReg', 'operator' => '>=', 'value' => $data['tgl_reg_from']];
        } 

        if(isset($data['tgl_reg_to'])){
            $condition['where'][] = ['field' => 'a.tglReg', 'operator' => '<=', 'value' => $data['tgl_reg_to']];
        } 

        if(isset($data['id_ruangan'])){
            $condition['where'][] = ['field' => 'g.id', 'operator' => '=', 'value' => $data['id_ruangan']];
        }        

        $rjalan = Rjalan::registrasi($condition);
        //$rinap  = Rinap::registrasi($condition);   

        if(count($rjalan) > 0){
            array_push($result, $rjalan);
        } 

        // if(count($rinap) > 0){
        //     array_push($result, $rinap);
        // } 

        if(count($result) > 0){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $rjalan, 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 200, 'message' => 'data tidak ditemukan.']];
        }

        return response()->json($data);
    }

    public function dataupdate(Request $request){

        if(!empty($request->input())){
            $data = $request->input();
        }else{
            $data = json_decode(file_get_contents("php://input"), true);
        }

        if(isset($data['noreg'])){
            $condition['where'][] = ['field' => 'noreg', 'operator' => '=', 'value' => $data['noreg']];
        }

        if(!isset($condition)){

            $data = ['metadata' => [ 'status' => 500, 'message' => 'Parameter Kondisi harus didefinisikan']];

            return response()->json($data); 
        }

        $update = Rjalan::registrasiUpdate($condition, $data);

        if($update){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => '', 'message' => 'Berhasil Di Update' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Gagal / Tidak ada perubahan data']];
        }

        return response()->json($data);
    }
}
