<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Pelaksana;
use App\Model\Emrpemeriksaan;
use Libdoco;

class EmrController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function getTindakanPemeriksaan(Request $request){

        $id_buku_tarif  = ($request->input('id_buku_tarif')) ? $request->input('id_buku_tarif') : '02';
        $cat            = ($request->input('cat')) ? $request->input('cat') : null;
        $nama_tindakan  = ($request->input('nama_tindakan')) ? $request->input('nama_tindakan') : null;

        $select  = 'a.id,';
        $select .= 'a.name,';
        $select .= 'a.id_cat_tarif,';
        $select .= 'b.namaCatTarif';

        $query = DB::table('emr_mst_pemeriksaan as a')
                ->select(DB::raw($select))
                ->leftJoin('cat_tarif as b', 'b.cat_tarifID', '=', 'a.id_cat_tarif');

        if($cat == null){
            $query->where('a.group', 'LAB');
        }else{
            $query->where('a.group', $cat);
        }

        if($nama_tindakan != null){
            $query->where('a.name', 'like', "%".$nama_tindakan."%");
        }

        $query->where('a.id_buku_tarif', $id_buku_tarif);

        $result = $query->get();

        if(!empty($result)){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $result, 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function getPemeriksaanPenunjang($cat=null, $nama=null){

        if($cat){
            $condition['where'][] = ['field' => 'emr_mst_pemeriksaan.group', 'operator' => '=', 'value' => $cat];
        }

        if($nama){
            $condition['where'][] = ['field' => 'emr_mst_pemeriksaan.name', 'operator' => 'LIKE', 'value' => '%'.$nama.'%'];
        }
       
        $result = Emrpemeriksaan::getEmrMasterPemeriksaan($condition);
        
        if(!empty($result)){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $result, 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function getPelaksana($type, $nama_pelaksana){

        $pelaksana = Pelaksana::select('id_pelaksana as id', 'pelaksanaName as nama_pelaksana')
                     ->where('group_jasa', $type)
                     ->where('pelaksanaName', 'like', "%".$nama_pelaksana."%")
                     ->where('active', '1')
                     ->get();

        if(!empty($pelaksana)){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $pelaksana, 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function getToken($token){

        $select  = 'token,';
        $select .= 'username,';
        $select .= 'unit,';
        $select .= 'role,';
        $select .= 'shift,';
        $select .= 'nik,';
        $select .= 'pelaksana';

        $token = DB::table('session_token')
                ->select(DB::raw($select))
                ->where('token', $token)
                ->where('expired', '0')
                ->get();

        if(!empty($token)){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $token, 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function cekTokenLinkFarmasi(Request $request){

        $noreg  = ($request->input('noreg')) ? $request->input('noreg') : null;
        $token  = ($request->input('token')) ? $request->input('token') : null;

        $cekTokenEmr = DB::table('session_token')
                ->where('tokenEmr', $token)
                //->where('expired', '0')
                ->get();
                // print_r($token);
                // exit;

        if(count($cekTokenEmr) > 0){
           if($cekTokenEmr[0]->tokenEmr == $token){
                echo "<script>window.open('http://10.2.1.81/simrsad_v3/#/farmasi/index/$noreg/$token', '_self');</script>";
            }else{
                return "Token tidak sesuai";
            }
        }

    }

    public function reqPemeriksaan(Request $request){

        if(!empty($request->input())){
            $data = $request->input();
        }else{
            $data = json_decode(file_get_contents("php://input"), true);
        }
        
        $batch = array();
        $sess = microtime(TRUE);

        date_default_timezone_set("Asia/Bangkok");

        foreach ($data['data'] as $row) {
            $new = array(
                'noreg' => $row['noreg'],
                'group' => strtoupper($row['group']),
                'id_pemeriksaan' => $row['id_pemeriksaan'],
                'sess' => $sess,
                'dateCreated' => date("Y-m-d H:i:s"),
                'userCreated' => $row['user']
            );
            array_push($batch, $new);
        }

        $insertEmrPemeriksaan = DB::table('emr_pemeriksaan')->insert($batch);

        if($insertEmrPemeriksaan){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'Success'], 'response' => ['data' => '', 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);

    }

    public function getResep($noreg){

        DB::connection()->enableQueryLog();

        $select  = 'emr_resep_farmasi_head.id,';
        $select .= 'emr_resep_farmasi_head.unique_code as no_resep,';
        $select .= 'emr_resep_farmasi_head.noreg,';
        $select .= 'emr_resep_farmasi_head.nosep,';
        $select .= 'emr_resep_farmasi_head.noKartu,';
        $select .= 'emr_resep_farmasi_head.kategori,';
        $select .= 'emr_resep_farmasi_head.group_obat,';
        $select .= 'COALESCE(emr_resep_farmasi_head.catatan,emr_resep_farmasi_detail.catatan) as catatan,';
        $select .= 'emr_resep_farmasi_detail.kode_obat,';
        $select .= 'emr_resep_farmasi_detail.nama_obat,';
        $select .= 'emr_resep_farmasi_detail.jumlah,';
        $select .= 'emr_resep_farmasi_detail.dosis';

        $data = DB::table('emr_resep_farmasi_detail')
                ->select(DB::raw($select))
                ->leftJoin('emr_resep_farmasi_head', 'emr_resep_farmasi_detail.id_emr_resep_farmasi_head', '=', 'emr_resep_farmasi_head.id')
                ->where('emr_resep_farmasi_head.noreg', $noreg)
                ->where('emr_resep_farmasi_head.deleted', 0)
                ->get();

        if(!empty($data)){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $data, 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        // print_r(DB::getQueryLog());
        // exit;

        return response()->json($data);
    }

    public function getScan($norm, $tgl_lahir){

        //DB::connection('mysql2')->enableQueryLog();

        $select  = 'a.id,';
        $select .= 'CONCAT(\'http://192.168.2.44/erekammedis/dokumen/\',b.file_image) as file';

        $data = DB::connection('mysql2')->table('dok_rekam_medis as a')
        ->select(DB::raw($select))
        ->leftJoin('dok_file_rekam_medis as b', 'b.no_scan', '=', 'a.no_scan')
        ->where('a.no_rm', $norm)
        ->where('a.tgl_lahir', $tgl_lahir)
        ->get();

        if(!empty($data)){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $data, 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        // print_r(DB::connection('mysql2')->getQueryLog());
        // exit;

        return response()->json($data);
    }

    public function hapusResep(Request $request){

        $noresep  = ($request->input('noresep')) ? $request->input('noresep') : null;

        $result = DB::table('emr_resep_farmasi_head')
                                ->where('unique_code', $noresep)
                                ->update(['deleted' => 1]);

        if($result){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'Success'], 'response' => ['data' => '', 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);

    }
}
