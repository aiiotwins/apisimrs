<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\HelperService;
use App\Helpers\BpjsV2;
use App\Helpers\Helfa;
use App\Model\Pasien;
use App\Model\Poli;
use App\Model\Registrasi;

class Bpjswsv2Controller extends Controller
{
    private function getTokenNumber($length){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "0123456789";


        for($i=0;$i<$length;$i++){
            $token .= $codeAlphabet[mt_rand(0,strlen($codeAlphabet)-1)];
        }
        return $token;
    }

    // START ANTREAN RS WS RS
    public function getToken(Request $request){

        date_default_timezone_set('UTC');

        $user      = $request->header('x-username');
        $password  = $request->header('x-password');

        $token  = hash_hmac('sha256', $user.$password, true);
        $encode = base64_encode($token.'/'.date('Y-m-d H:i:s'));

        $result = ['token' => $encode];

        return HelperService::_successResponse($result);

    }

    public function getStatusAntrian(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $result = [
            'namapoli' => 'Nama Poli',
            'namadokter' => 'Nama Dokter',
            'totalantrean' => 25,
            'sisaantrean' => 4,
            'antreanpanggil' => 'A-21',
            'sisakuotajkn' => 5,
            'kuotajkn' => 30,
            'sisakuotanonjkn' => 5,
            'kuotanonjkn' => 30,
            'keterangan' => ''
        ];

        return HelperService::_successResponse($result);
    }

    public function getAmbilAntrian(Request $request){

        //GET ID PASIEN
        $data_pasien = Pasien::where('NOASKES', $request->nomorkartu)
                    ->where('KODE', $request->norm)
                    ->first();

        $condition['where'][] = ['field' => 'mst_pasien.id_pasien', 'operator' => '=', 'value' => $data_pasien->id_pasien];

        $jns_pasien = Pasien::registrasi($condition);

        //GET ID POLI
        $data_poli = Poli::where('kodepolibpjs', $request->kodepoli)->first();

        $tgl_daftar = empty($request->tanggalperiksa) ? date('Y-m-d') : $request->tanggalperiksa;
        
        $data_pendaftaran = array('hospital_code' => '1', 'polyclinic_id' => $data_poli->poliid, 'visit_date' => $tgl_daftar, 'bpjs_type' => strtolower($jns_pasien->payment));

        DB::beginTransaction();

        try 
        { 
            $register = new Registrasi;

            $norm = empty($data_pasien->KODE) ? null : $data_pasien->KODE;
            $nama = empty($data_pasien->NAMA) ? null : $data_pasien->NAMA;
            $str_tanggal = empty($tgl_daftar) ? null : $tgl_daftar;

            //DB::connection()->enableQueryLog();

            $data_regis = Registrasi::where(['norm' => $norm, "tglReg" => $tgl_daftar])->where("status","!=", "canceled")->first();

            // print_r(DB::getQueryLog());
            // exit;

            if ($data_regis) {

                return HelperService::_failResponse();

            } else {

                $type = 'BPJS';

                do
                {
                    $token = $this->getTokenNumber(4);
                    $code = $token . substr(strftime("%Y", time()),2);
                    $regis_code = Registrasi::where('noreg', $code)->get();
                }

                while(empty($regis_code));

                $register = new Registrasi;

                $register->noreg = $code;
                $register->norm = $norm;
                $register->nama = $nama;

                if ($type != "umum") {

                    $register->noaskes = empty($data_pasien->NOASKES) ? null : $data_pasien->NOASKES;

                    // $request->id_pasien = empty($request->id_pasien) ? Pasien::where('NOASKES', $registrasi_online->noaskes)->first() : $request->id_pasien;

                }

                $id_rs = '1';
                $register->tglReg = $tgl_daftar;
                $register->jamReg = date('H:i:s');
                $register->str_tanggal_reg = $tgl_daftar;
                $register->poliklinik = empty($data_poli->name) ? null : $data_poli->name;
                $register->status = "open";
                $register->statusRm = "open";
                //$register->id_rs = (isset($request->id_rs)) ? $request->id_rs : '1' ;

                // INI UNTUK HELFA
                $register->id_poliklinik = empty($data_poli->poliid) ? null : $data_poli->poliid;
                $register->id_rs = $id_rs ;
                $register->id_pasien = $data_pasien->id_pasien;
                $register->payment_method = $type;
                $register->nomor_ref = $request->nomorreferensi;
                $register->jns_kunjungan = $request->jeniskunjungan;

                $register->dateCreated = Helfa::getLocalTime();
                //END INI UNTUK HELFA

                $status_simpan = $register->save();

                if ($status_simpan) {

                    //GET TOKEN HELFA
                    $token          = Helfa::gettoken();
                    $token          = json_decode($token);
                    $helfa_token    = $token->data->token;

                    $no_antrian = Helfa::getnumber($data_pendaftaran);
                    $no_antrian = json_decode($no_antrian);

                    $helfa_no_antrian = $no_antrian->data->number;
                    $helfa_queue_number = $no_antrian->data->queue_number;  

                    $update_reg = DB::table('andro_regis')
                    ->where('noreg', $register->noreg)
                    ->update(
                        [
                            'helfa_no_antrian'     => $helfa_no_antrian,
                            'helfa_token'          => $helfa_token,
                            'helfa_queue_number'   => $helfa_queue_number
                        ]
                    );

                    $get_result = DB::table('andro_regis')->where('noreg', $register->noreg)->first();

                    DB::commit();             

                    $result = [
                        'nomorantrean' => $get_result->helfa_queue_number,
                        'angkaantrean' => substr($get_result->helfa_queue_number, 1),
                        'kodebooking' => $get_result->helfa_token,
                        'norm' => $get_result->norm,
                        'namapoli' => $get_result->poliklinik,
                        'namadokter' => '',
                        'estimasidilayani' => 0,
                        'sisakuotajkn' => 0,
                        'kuotajkn' => 0,
                        'sisakuotanonjkn' => 0,
                        'kuotanonjkn' => 0,
                        'keterangan' => 'Peserta harap 60 menit lebih awal guna pencatatan administrasi.'
                    ];

                    return HelperService::_successResponse($result);
                }
            }
        } 
        catch (\Exception $e) 
        {
            DB::rollback();
            return HelperService::_failResponse();
        }
    }

    public function getSisaAntrian(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $result = [
            'nomorantrean' => 'A20',
            'namapoli' => 'Anak',
            'namadokter' => 'Dr. Hendra',
            'sisaantrean' => 12,
            'antreanpanggil' => 'A-8',
            'waktutunggu' => 9000,
            'keterangan' => ''
        ];

        return HelperService::_successResponse($result);
    }

    public function getBatalAntrian(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $result = ['metadata' => ['messages' => 'Ok', 'code' => 200]];

        return response()->json($result);
    }

    public function getCheckin(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $result = ['metadata' => ['messages' => 'Ok', 'code' => 200]];

        return response()->json($result);
    }

    public function getInfoPasienBaru(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $result = [
            'norm' => '123456'
        ];

        $message = 'Harap datang ke admisi untuk melengkapi data rekam medis';

        return HelperService::_successResponse($result, $message);
    }

    public function getJadwalOperasiRS(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $result = [
            'list' => [
                [
                    'kodebooking' => '123456ZXC',
                    'tanggaloperasi' => '2019-12-11',
                    'jenistindakan' => 'operasi gigi',
                    'kodepoli' => '001',
                    'namapoli' => 'Poli Bedah Mulut',
                    'terlaksana' => 1,
                    'nopeserta' => '0000000924782',
                    'lastupdate' => 1577417743000
                ],[
                    'kodebooking' => '67890QWE',
                    'tanggaloperasi' => '2019-12-11',
                    'jenistindakan' => 'operasi mulut',
                    'kodepoli' => '001',
                    'namapoli' => 'Poli Bedah Mulut',
                    'terlaksana' => 0,
                    'nopeserta' => '',
                    'lastupdate' => 1577417743000
                ]                 
            ]
        ];

        return HelperService::_successResponse($result);
    }

    public function getJadwalOperasiPasien(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $result = [
            'list' => [
                [
                    'kodebooking' => '123456ZXC',
                    'tanggaloperasi' => '2019-12-11',
                    'jenistindakan' => 'operasi gigi',
                    'kodepoli' => '001',
                    'namapoli' => 'Poli Bedah Mulut',
                    'terlaksana' => 0 
                ]                 
            ]
        ];

        return HelperService::_successResponse($result);
    }
    // END ANTREAN RS WS RS

    // START ANTREAN RS WS BPJS

    public function getRefPoli(){

        $param = "ref/poli";
        $result = BpjsV2::curl(env('URL_BPJSV2', 'forge') . $param, false, BpjsV2::getHeader(true));

        return HelperService::_successResponse($result);
    }

    public function getRefDokter(){

        $param = "ref/dokter";
        $result = BpjsV2::curl(env('URL_BPJSV2', 'forge') . $param, false, BpjsV2::getHeader(true));

        return HelperService::_successResponse($result);
    }

    public function getJadwalDokter($kdpolibpjs = null, $tgl = null){

        $param = "jadwaldokter/kodepoli/$kdpolibpjs/tanggal/$tgl";
        $result = BpjsV2::curl(env('URL_BPJSV2', 'forge') . $param, false, BpjsV2::getHeader(true));

        return HelperService::_successResponse($result);
    }

    public function updateJadwalDokter(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $param = "jadwaldokter/updatejadwaldokter";
        $result = BpjsV2::curl(env('URL_BPJSV2', 'forge') . $param, false, BpjsV2::getHeader(true));

        return HelperService::_successResponse($result);
    }

    public function tambahAntrian(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $param = "antrean/add";
        $result = BpjsV2::curl(env('URL_BPJSV2', 'forge') . $param, false, BpjsV2::getHeader(true));

        return HelperService::_successResponse($result);
    }

    public function updateWaktuAntrian(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $param = "antrean/updatewaktu";
        $result = BpjsV2::curl(env('URL_BPJSV2', 'forge') . $param, false, BpjsV2::getHeader(true));

        return HelperService::_successResponse($result);
    }

    public function batalAntrian(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $param = "antrean/batal";
        $result = BpjsV2::curl(env('URL_BPJSV2', 'forge') . $param, false, BpjsV2::getHeader(true));

        return HelperService::_successResponse($result);
    }

    public function getListTask(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $param = "antrean/getlisttask";
        $result = BpjsV2::curl(env('URL_BPJSV2', 'forge') . $param, false, BpjsV2::getHeader(true));

        return HelperService::_successResponse($result);
    }

    public function dashboardDay($tgl = null, $waktu = null){

        $param = "dashboard/waktutunggu/tanggal/{$tgl}/waktu/{$waktu}";
        $result = BpjsV2::curl(env('URL_BPJSV2', 'forge') . $param, false, BpjsV2::getHeader(true));

        return HelperService::_successResponse($result);
    }

    public function dashboardMonth($bulan = null,$tahun = null, $waktu = null){

        $param = "dashboard/waktutunggu/bulan/{$bulan}/tahun/{$tahun}/waktu/{$waktu}";
        $result = BpjsV2::curl(env('URL_BPJSV2', 'forge') . $param, false, BpjsV2::getHeader(true));

        return HelperService::_successResponse($result);
    }

    // END ANTREAN RS WS BPJS
}