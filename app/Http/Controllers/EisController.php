<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\Eis;

class EisController extends Controller
{
    public function postKamar(){
        $results = DB::select( DB::raw("SELECT 
                bb.Keterangan,bb.Ruangan,bb.Lantai,
                IF(bb.kondisi='Baik', 'Tanpa Negatif',bb.kondisi) AS Kondisi,
                bb.Kelas,bb.Kapasitas,bb.Digunakan,bb.Tersedia
                FROM (
                SELECT 
                IF ( (aa.Lantai = 'LANTAI 1' OR aa.Lantai = 'LANTAI 2' )
                AND aa.Ruangan = 'PAVILIUN DARMAWAN',
                'Non Covid-19', 'Covid-19' ) AS Keterangan,
                aa.Ruangan,
                aa.Lantai,
                aa.kondisi,
                aa.Kelas,
                SUM( aa.Kapasitas ) AS Kapasitas,
                SUM( aa.Digunakan ) AS Digunakan,
                SUM( aa.Kapasitas - aa.Digunakan ) AS Tersedia
                FROM
                (SELECT
                    'PAVILIUN SOEHARDO KERTOHUSODO' AS Ruangan,
                    lantai AS Lantai,
                    IF ( kondisi = 'covid', 'Tekanan Negatif', kondisi ) AS Kondisi,
                        IF(mst_kelas.`name` = 'I', 'KELAS I',
                        IF(mst_kelas.`name` = 'II','KELAS II',
                        IF(mst_kelas.`name` ='III','KELAS III',
                                mst_kelas.`name` ))) AS Kelas,
                        COUNT( keterangan ) AS Kapasitas,
                    '0' AS Digunakan,
                    '0' AS Tersedia
                FROM
                    mst_tempat_tidur
                    LEFT JOIN mst_kelas ON mst_tempat_tidur.kelas = mst_kelas.id 
                WHERE
                    mst_tempat_tidur.active = 1 
                    AND keterangan LIKE '%PAVILIUN SOEHARDO KERTOHUSODO%'
                    GROUP BY lantai,Kelas,kondisi
                    UNION ALL
                    SELECT
                    'PAVILIUN DARMAWAN' AS Ruangan,
                    lantai AS Lantai,
                    IF ( kondisi = 'covid', 'Tekanan Negatif', kondisi ) AS Kondisi,
                        IF(mst_kelas.`name` = 'I', 'KELAS I',
                        IF(mst_kelas.`name` = 'II','KELAS II',
                        IF(mst_kelas.`name` ='III','KELAS III',
                                mst_kelas.`name` ))) AS Kelas,
                        COUNT( keterangan ) AS Kapasitas,
                    '0' AS Digunakan,
                    '0' AS Tersedia
                FROM
                    mst_tempat_tidur
                    LEFT JOIN mst_kelas ON mst_tempat_tidur.kelas = mst_kelas.id 
                WHERE
                    mst_tempat_tidur.active = 1 
                    AND keterangan LIKE '%PAVILIUN DARMAWAN%'
                    GROUP BY lantai,Kelas,kondisi
                    UNION ALL
                        SELECT
                    'PAVILIUN SOEHARDO KERTOHUSODO' AS Ruangan,
                    mst_tempat_tidur.lantai AS Lantai,
                    IF ( kondisi = 'covid', 'Tekanan Negatif', kondisi ) AS Kondisi,
                    IF(mst_kelas.`name` = 'I', 'KELAS I',
                    IF(mst_kelas.`name` = 'II','KELAS II',
                    IF(mst_kelas.`name` ='III','KELAS III',
                                mst_kelas.`name` ))) AS Kelas,
                        '0' AS Kapasitas,
                        COUNT( ab.bedID ) AS Digunakan,
                    0 AS Tersedia 
                FROM
                    rinap_kamar_rawat
                    LEFT JOIN mst_tempat_tidur ON rinap_kamar_rawat.bedID = mst_tempat_tidur.bedID 
                        LEFT JOIN mst_kelas ON mst_tempat_tidur.kelas = mst_kelas.id 
                        LEFT JOIN (SELECT   * FROM  rinap_kamar_rawat 
                        WHERE   tglKeluar IS NULL   AND deleted = 0 
                        GROUP BY    bedID) as ab On rinap_kamar_rawat.noreg =ab.noreg
                WHERE
                    rinap_kamar_rawat.deleted = 0 
                    AND rinap_kamar_rawat.tglKeluar IS NULL 
                    AND rinap_kamar_rawat.ruanganID = 138 
                    AND mst_tempat_tidur.active = 1 
                GROUP BY
                lantai,kelas,kondisi
                    UNION ALL
                        SELECT
                    'PAVILIUN DARMAWAN' AS Ruangan,
                    mst_tempat_tidur.lantai AS Lantai,
                    IF ( kondisi = 'covid', 'Tekanan Negatif', kondisi ) AS Kondisi,
                    IF(mst_kelas.`name` = 'I', 'KELAS I',
                    IF(mst_kelas.`name` = 'II','KELAS II',
                    IF(mst_kelas.`name` ='III','KELAS III',
                    mst_kelas.`name`))) as Kelas,
                    '0' AS Kapasitas,
                  COUNT(ab.bedID) as Digunakan,
                    0 AS Tersedia 
                FROM
                    rinap_kamar_rawat
                    LEFT JOIN mst_tempat_tidur ON rinap_kamar_rawat.bedID = mst_tempat_tidur.bedID 
                        LEFT JOIN mst_kelas ON mst_tempat_tidur.kelas = mst_kelas.id 
                            LEFT JOIN (SELECT   * FROM  rinap_kamar_rawat 
                            WHERE   tglKeluar IS NULL   AND deleted = 0 
                            GROUP BY    bedID) as ab On rinap_kamar_rawat.noreg =ab.noreg
                WHERE
                    rinap_kamar_rawat.deleted = 0 
                    AND rinap_kamar_rawat.tglKeluar IS NULL 
                    AND rinap_kamar_rawat.ruanganID = 137 
                    AND mst_tempat_tidur.active = 1 
                GROUP BY
                lantai,kelas,kondisi
                UNION ALL
                SELECT
                    'PAVILIUN KARTIKA II' AS Ruangan,
                    lantai AS Lantai,   
                    IF ( kondisi = 'covid', 'Tekanan Negatif', kondisi ) AS Kondisi,
                    IF(mst_kelas.`name` = 'I', 'KELAS I',
                    IF(mst_kelas.`name` = 'II','KELAS II',
                    IF(mst_kelas.`name` ='III','KELAS III',
                    IF(mst_kelas.`name` ='I Utama','KELAS I',
                    mst_kelas.`name`)))) as Kelas,
                    COUNT(keterangan) as Kapasitas,
                    '0' AS Digunakan,
                    '0' AS Tersedia
                FROM
                    mst_tempat_tidur
                    LEFT JOIN mst_kelas ON mst_tempat_tidur.kelas = mst_kelas.id 
                WHERE
                    mst_tempat_tidur.active = 1 
                    AND keterangan LIKE '%PAVILIUN KARTIKA 2%'
                    GROUP BY lantai,Kelas,kondisi
                    UNION ALL
                        SELECT
                    'PAVILIUN KARTIKA II' AS Ruangan,
                    mst_tempat_tidur.lantai AS Lantai,  
                    IF ( kondisi = 'covid', 'Tekanan Negatif', kondisi ) AS Kondisi,
                    IF(mst_kelas.`name` = 'I', 'KELAS I',
                    IF(mst_kelas.`name` = 'II','KELAS II',
                    IF(mst_kelas.`name` ='III','KELAS III',
                    IF(mst_kelas.`name` ='I Utama','KELAS I',
                    mst_kelas.`name`)))) as Kelas,
                    '0' AS Kapasitas,
                  COUNT(ab.bedID) as Digunakan,
                    0 AS Tersedia 
                FROM
                    rinap_kamar_rawat
                    LEFT JOIN mst_tempat_tidur ON rinap_kamar_rawat.bedID = mst_tempat_tidur.bedID 
                        LEFT JOIN mst_kelas ON mst_tempat_tidur.kelas = mst_kelas.id 
                    LEFT JOIN (SELECT   * FROM  rinap_kamar_rawat 
                    WHERE   tglKeluar IS NULL   AND deleted = 0 
                    GROUP BY    bedID) as ab On rinap_kamar_rawat.noreg =ab.noreg
                WHERE
                    rinap_kamar_rawat.deleted = 0 
                    AND rinap_kamar_rawat.tglKeluar IS NULL 
                    AND rinap_kamar_rawat.ruanganID = 132 
                    AND mst_tempat_tidur.active = 1 
                GROUP BY
                lantai,kelas,kondisi
                )AS aa
                    GROUP BY aa.Ruangan,aa.Lantai,aa.kelas
                    UNION ALL
                    SELECT 
                IF ( aa.Lantai = 'LANTAI 2' 
                AND aa.Ruangan = 'CICU KARTIKA',
                'Covid-19',
                IF(aa.kelas like '%covid%','Covid-19',
                IF(aa.Ruangan ='IGD','Covid-19','Non Covid-19' ) ))AS Keterangan,
                aa.Ruangan,
                aa.Lantai,
                IF ( kondisi = 'covid', 'Tekanan Negatif', kondisi ) AS Kondisi,
                aa.Kelas,
                SUM( aa.Kapasitas ) AS Kapasitas,
                SUM( aa.Digunakan ) AS Digunakan,
                SUM( aa.Kapasitas - aa.Digunakan ) AS Tersedia
                FROM
                (SELECT
                    mst_ruangan.ruanganName AS Ruangan, 
                    lantai AS Lantai, 
                    IF ( kondisi = 'covid', 'Tekanan Negatif', kondisi ) AS Kondisi,
                    IF(mst_kelas.`name` = 'I', 'KELAS I',
                    IF(mst_kelas.`name` = 'II','KELAS II',
                    IF(mst_kelas.`name` ='III','KELAS III',
                    IF(mst_kelas.`name` ='I Utama','KELAS I',
                    mst_kelas.`name`)))) AS Kelas, 
                    COUNT(keterangan) AS Kapasitas, 
                    '0' AS Digunakan, 
                    '0' AS Tersedia
                FROM
                    mst_tempat_tidur
                    LEFT JOIN mst_kelas ON mst_tempat_tidur.kelas = mst_kelas.id
                    LEFT JOIN mst_ruangan ON mst_tempat_tidur.ruanganID = mst_ruangan.ruanganID 
                WHERE
                    mst_tempat_tidur.active = 1 
                    AND (  mst_ruangan.ruanganName <> 'PAVILIUN DARMAWAN'
                    AND mst_ruangan.ruanganName <> 'PAVILIUN SOEHARDO KERTOHUSODO'
                    AND mst_ruangan.ruanganName <> 'PAVILIUN KARTIKA 2' )
                    GROUP BY
                    Ruangan,lantai,Kelas,kondisi
                    UNION ALL
                    SELECT
                    mst_ruangan.ruanganName AS Ruangan,
                    mst_tempat_tidur.lantai AS Lantai,
                    IF ( kondisi = 'covid', 'Tekanan Negatif', kondisi ) AS Kondisi,
                    IF(mst_kelas.`name` = 'I', 'KELAS I',
                    IF(mst_kelas.`name` = 'II','KELAS II',
                    IF(mst_kelas.`name` ='III','KELAS III',
                    IF(mst_kelas.`name` ='I Utama','KELAS I',
                                    mst_kelas.`name` )))) AS Kelas,
                        '0' AS Kapasitas,
                        COUNT( ab.bedID ) AS Digunakan,
                0 AS Tersedia 
                FROM
                    rinap_kamar_rawat
                    LEFT JOIN mst_tempat_tidur ON rinap_kamar_rawat.bedID = mst_tempat_tidur.bedID
                    LEFT JOIN mst_kelas ON mst_tempat_tidur.kelas = mst_kelas.id
                    LEFT JOIN mst_ruangan ON mst_tempat_tidur.ruanganID = mst_ruangan.ruanganID 
                        LEFT JOIN (SELECT   * FROM  rinap_kamar_rawat 
                        WHERE   tglKeluar IS NULL   AND deleted = 0 
                        GROUP BY    bedID) as ab On rinap_kamar_rawat.noreg =ab.noreg
                WHERE
                    rinap_kamar_rawat.deleted = 0 
                    AND rinap_kamar_rawat.tglKeluar IS NULL 
                    AND ( mst_ruangan.ruanganName <> 'PAVILIUN DARMAWAN' 
                    AND mst_ruangan.ruanganName <> 'PAVILIUN SOEHARDO KERTOHUSODO' 
                    AND mst_ruangan.ruanganName <> 'PAVILIUN KARTIKA 2' ) 
                    AND mst_tempat_tidur.active = 1 
                GROUP BY
                    Ruangan,lantai,kelas,kondisi
                            ) AS aa 
                            
                            GROUP BY aa.Ruangan, aa.Lantai, aa.kelas ) AS bb
                            #WHERE bb.keterangan ='Covid-19'
                    ORDER BY
                    bb.Keterangan,bb.Ruangan,bb.Lantai") );

        $results = array_map(function ($value) {
            return (array)$value;
        }, $results);

        // KAPASITAS
        $data['kapasitas_vip']=0;
        $data['kapasitas_kelas_1']=0;
        $data['kapasitas_kelas_2']=0;
        $data['kapasitas_kelas_3']=0;
        $data['kapasitas_hcu']=0;
        $data['kapasitas_iccu']=0;
        $data['kapasitas_icu_negatif_ventilator']=0;
        $data['kapasitas_icu_negatif_tanpa_ventilator']=0;
        $data['kapasitas_icu_tanpa_negatif_ventilator']=0;
        $data['kapasitas_icu_tanpa_negatif_tanpa_ventilator']=0;
        $data['kapasitas_isolasi_negatif']=0;
        $data['kapasitas_isolasi_tanpa_negatif']=0; //3 tambahan HD
        $data['kapasitas_nicu_covid']=0;
        $data['kapasitas_perina_covid']=0;
        $data['kapasitas_picu_covid']=0;
        $data['kapasitas_ok_covid']=1;
        $data['kapasitas_hd_covid']=0;
        
        // TERSERDIA
        $data['kosong_vip']=0;
        $data['kosong_kelas_1']=0;
        $data['kosong_kelas_2']=0;
        $data['kosong_kelas_3']=0;
        $data['kosong_hcu']=0;
        $data['kosong_iccu']=0;
        $data['kosong_icu_negatif_ventilator']=0;
        $data['kosong_icu_negatif_tanpa_ventilator']=0;
        $data['kosong_icu_tanpa_negatif_ventilator']=0;
        $data['kosong_icu_tanpa_negatif_tanpa_ventilator']=0;
        $data['kosong_isolasi_negatif']=0;
        $data['kosong_isolasi_tanpa_negatif']=0;
        $data['kosong_nicu_covid']=0;
        $data['kosong_perina_covid']=0;
        $data['kosong_picu_covid']=0;
        $data['kosong_ok_covid']=0;
        $data['kosong_hd_covid']=0;   


        foreach($results as $loop){
            if(strtolower($loop['Keterangan']) == "non covid-19" && (strtolower($loop['Kelas'])== "vip" || strtolower($loop['Kelas'])== "vvip")){
                $data['kapasitas_vip']+=$loop['Kapasitas'];
                $data['kosong_vip']+=$loop['Tersedia'];
            }
            if(strtolower($loop['Keterangan']) == "non covid-19" && strtolower($loop['Kelas'])== "kelas i"){
                $data['kapasitas_kelas_1']+=$loop['Kapasitas'];
                $data['kosong_kelas_1']+=$loop['Tersedia'];
            }
            if(strtolower($loop['Keterangan']) == "non covid-19" && strtolower($loop['Kelas'])== "kelas ii"){
                $data['kapasitas_kelas_2']+=$loop['Kapasitas'];
                $data['kosong_kelas_2']+=$loop['Tersedia'];
            }
            if(strtolower($loop['Keterangan']) == "non covid-19" && strtolower($loop['Kelas'])== "kelas iii"){
                $data['kapasitas_kelas_3']+=$loop['Kapasitas'];
                $data['kosong_kelas_3']+=$loop['Tersedia'];
            }            

            if(strtolower($loop['Keterangan']) == "non covid-19" && strtolower($loop['Kelas'])== "hcu"){
                $data['kapasitas_hcu']+=$loop['Kapasitas'];
                $data['kosong_hcu']+=$loop['Tersedia'];
            }            
            if(strtolower($loop['Keterangan']) == "non covid-19" && strtolower($loop['Kelas'])== "icu"){
                $data['kapasitas_iccu']+=$loop['Kapasitas'];
                $data['kosong_iccu']+=$loop['Tersedia'];

                //$data['kapasitas_icu_tanpa_negatif_ventilator']+=$loop['Kapasitas'];
                //$data['kosong_icu_tanpa_negatif_ventilator']+=$loop['Tersedia'];
            }   

            if(strtolower($loop['Keterangan']) == "covid-19" && $loop['Ruangan']== "PAVILIUN SOEHARDO KERTOHUSODO" && ($loop['Lantai']== "LANTAI 1" || $loop['Lantai']== "LANTAI 2") || $loop['Ruangan']== "CICU KARTIKA" || ($loop['Ruangan']== "IGD" && $loop['Kelas']== "ICU")){
                // $data['kapasitas_icu_negatif_ventilator']+=$loop['Kapasitas'];
                // $data['kosong_icu_negatif_ventilator']+=$loop['Tersedia'];
            }
            
            if(strtolower($loop['Keterangan']) == "covid-19" && $loop['Ruangan']== "PAVILIUN SOEHARDO KERTOHUSODO" && $loop['Lantai']== "LANTAI 4" && $loop['Lantai']== "LANTAI 3"){
                $data['kapasitas_icu_negatif_tanpa_ventilator']+=$loop['Kapasitas'];
                $data['kosong_icu_negatif_tanpa_ventilator']+=$loop['Tersedia'];

            }            

            // if(strtolower($loop['Keterangan']) == "covid-19" && $loop['Ruangan']== "PAVILIUN SOEHARDO KERTOHUSODO" && $loop['Lantai']== "LANTAI 3"){
            //     $data['kapasitas_isolasi_negatif']+=$loop['Kapasitas'];
            //     $data['kosong_isolasi_negatif']+=$loop['Tersedia'];
            // }


            if(strtolower($loop['Keterangan']) == "covid-19" && ($loop['Ruangan']== "PAVILIUN DARMAWAN" && $loop['Lantai']== "LANTAI 3") || ($loop['Ruangan']== "PAVILIUN DARMAWAN" && $loop['Lantai']== "LANTAI 4") || ($loop['Ruangan']== "PAVILIUN DARMAWAN" && $loop['Lantai']== "LANTAI 5" && $loop['Kondisi'] == "Tekanan Negatif") || ($loop['Ruangan']== "PAVILIUN DARMAWAN" && $loop['Lantai']== "LANTAI 6" && $loop['Kondisi'] == "Tekanan Negatif") || ($loop['Ruangan']== "PAVILIUN KARTIKA II")){
                //$data['kapasitas_isolasi_negatif']+=$loop['Kapasitas'];
                //$data['kosong_isolasi_negatif']+=$loop['Tersedia'];
            }            


            if(strtolower($loop['Keterangan']) == "covid-19" && strtolower($loop['Kelas'])== "level iii / nicu (post covid-19)"){
                $data['kapasitas_nicu_covid']+=$loop['Kapasitas'];
                $data['kosong_nicu_covid']+=$loop['Tersedia'];
            }   

            if(strtolower($loop['Keterangan']) == "covid-19" && (strtolower($loop['Kelas'])== "level i / peristi" || strtolower($loop['Kelas'])== "level ii / peristi")){
                $data['kapasitas_perina_covid']+=$loop['Kapasitas'];
                $data['kosong_perina_covid']+=$loop['Tersedia'];
            }               

            if(strtolower($loop['Keterangan']) == "covid-19" && strtolower($loop['Kelas'])== "picu (post covid-19)"){
                $data['kapasitas_picu_covid']+=$loop['Kapasitas'];
                $data['kosong_picu_covid']+=$loop['Tersedia'];
            }   


        }

        $data = Eis::curl(Eis::base_url()."/bed", json_encode($data),Eis::getHeader());

        return $data;
    }
}
