<?php

namespace App\Http\Controllers;
use App\Model\Pasien;

class BpjsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cekpeserta($noaskes = null)
    {
        $data_pasien = Pasien::where('NOASKES',$noaskes)->first();

        if ($data_pasien) {
            $tglsep =  date('Y-m-d');
            $param = "Peserta/nokartu/{$noaskes}/tglSEP/{$tglsep}";
            $return = self::curl(env('URL_BPJS', 'forge') . $param, false, self::getHeader(true));

            $data = [
                'status' => 200,
                'data' => ['bpjs' => self::out($return), 'pasien' => $data_pasien],
                'message' => "response bpjs"
            ];
        } else {
            $data = [
                'status' => 500,
                'data' => null,
                'message' => "Data tidak ditemukan. Mohon datang ke RSPAD, mendaftar sebagai pasien Baru dan pastikan membawa data rujukan."
            ];
        }

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cekrujukan($noaskes = null)
    {

        $paramppk = "Rujukan/List/Peserta/{$noaskes}";
        $paramrs = "Rujukan/RS/List/Peserta/{$noaskes}";
        $rs = self::curl(env('URL_BPJS', 'forge') . $paramrs, false, self::getHeader(true));
        $rs = self::out($rs);

        if ($rs['metaData']['code'] == 201) {
            $ppk = self::curl(env('URL_BPJS', 'forge') . $paramppk, false, self::getHeader(true));
            $return = self::out($ppk);
        } else {
            $return = $rs;
        }

        $status = ($return['metaData']['code'] == 201) ? 500 : 200;
        $message = ($return['metaData']['code'] == 201) ? $return['metaData']['message'] : "response bpjs";

        $data = [
            'status' => $status,
            'data' => $return,
            'message' => $message
        ];

        return response()->json($data);
    }

    public function getpoli($namapoli = null)
    {

        $param      = "referensi/poli/".$namapoli;
        $datapoli   = self::curl(env('URL_BPJS', 'forge') . $param, false, self::getHeader(true));
        
        $data = [
            'status' => 200,
            'data' => ['bpjs' => self::out($datapoli)],
            'message' => "response bpjs"
        ];

        return response()->json($data);
    }

    public function gethistorybpjs($nokartu = null)
    {

        $tglmulai = date('Y-m-d', strtotime('-2 months'));
        $tglakhir = date('Y-m-d', strtotime('+2 months'));

        $param    = "monitoring/HistoriPelayanan/NoKartu/{$nokartu}/tglAwal/{$tglmulai}/tglAkhir/{$tglakhir}";

        $hasil      = self::curl(env('URL_BPJS', 'forge') . $param, false, self::getHeader(true));

        $data = [
            'status' => 200,
            'data' => ['bpjs' => self::out($hasil)],
            'message' => "response bpjs"
        ];

        return response()->json($data);
    }

    protected static function getHeader($form_url_encoded = false)
    {
        if ($form_url_encoded)
            $conten_type = "Content-Type: application/x-www-form-urlencoded";
        else
            $conten_type = "Content-Type: application/json";

        return [
            "X-cons-id : " . env('CONST_BPJS', 'forge'),
            "X-timestamp : " . self::getTimestamp(),
            "X-signature : " . self::getSignature(),
            $conten_type
        ];
    }

    protected static function getTimestamp()
    {
        date_default_timezone_set('UTC');
        return strval(time()-strtotime('1970-01-01 00:00:00'));
    }

    protected static function getSignature()
    {
        // Computes the signature by hashing the salt with the secret key as the key
        $signature = hash_hmac(
            'sha256',
            env('CONST_BPJS', 'forge') . "&" . self::getTimestamp(),
            env('KEY_BPJS', 'forge'),
            true
        );

        return base64_encode($signature);
    }

    /**
     * Output from CURL
     * @param mixed $data;
     * @return string JSON
     */
    protected static function out($data)
    {
        return $data && is_string($data) && json_decode($data) ? json_decode($data, true) : "";
    }


    protected static function curl($url, $data, $header, $action = '')
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);

        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            if (!empty($action)) {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $action);
            }
        }

        $res = curl_exec($curl);

        $curl_get_info = curl_getinfo($curl);
        $get_response_time = !empty($curl_get_info['total_time']) ? $curl_get_info['total_time'] : 0;

        if (!$res) {
            $errorArray = [
                'metadata'=>[
                    "code"=>500,
                    "message"=>curl_error($curl) ? : "Problem with bridging",
                ]
            ];
            $res = json_encode($errorArray);
            // die('Error: "'.curl_error($curl).'" - Code: '.curl_errno($curl));
        }
        curl_close($curl);

        return $res;
    }
}
