<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Lis;

class LisController extends Controller
{
    public function getoutstandinglist(Request $request){

        $post = $request->input();

        $start = str_replace('/', '-', $post['orderDateTimeStart']);
        $end   = str_replace('/', '-', $post['orderDateTimeEnd']);
        $flag  = $post['receivedFlag'];

        $condition['where'][] = ['field' => 'order_data_lis.dateReq', 'operator' => '>=', 'value' => $start];
        $condition['where'][] = ['field' => 'order_data_lis.dateReq', 'operator' => '<=', 'value' => $end];
        $condition['where'][] = ['field' => 'order_data_lis.ReceivedFlag', 'operator' => '=', 'value' => $flag]; 

        $result = Lis::getOutstandingList($condition);     

        if(!empty($result)){
            $data = [ 'status' => ['OK' => true, 'Code' => '-1', 'Messages' => 'SUCCESS'], 'OrderList' => $result ];
        }else{
            $data = [ 'status' => ['OK' => false, 'Code' => '-1', 'Messages' => 'FAILED'], 'OrderList' => '' ];
        }

        return response()->json($data);
    }

    public function getorderdetail(Request $request){

        $post = $request->input();

        $condition['where'][] = ['field' => 'order_data_lis.order_number', 'operator' => '=', 'value' => $post['orderNumber']];

        DB::beginTransaction();

        try
        {

            $data_registrasi = Lis::getOutstandingList($condition);
            $result = $data_registrasi[0];

            $data_items = Lis::getOrderItem($post['orderNumber']);

            if(!empty($result)){
                $array_responses = [
                    'status' => [
                        'OK'        => true,
                        'Code'      => '-1',
                        'Messages'  => 'SUCCESS'
                    ],
                    'PatientCode' => $result->PatientCode,
                    "PatientName" => $result->PatientName,
                    "PatientSexCode" => $result->PatientSexCode,
                    "PatientSexName" => $result->PatientSexName,
                    "PatientDob" => $result->PatientDob,
                    "PatientAddress" => $result->PatientAddress,
                    "PatientCityCode" => $result->PatientCityCode,
                    "PatientCityName" => $result->PatientCityName,
                    "PatientPhoneNumber" => $result->PatientPhoneNumber,
                    "PatientFaxNumber" => $result->PatientFaxNumber,
                    "PatientMobileNumber" => $result->PatientMobileNumber,
                    "PatientEmail" => $result->PatientEmail,
                    "VisitNumber" => $result->VisitNumber,
                    "OrderNumber" => $result->OrderNumber,
                    "OrderDateTime" => $result->OrderDateTime,
                    "DoctorOrderCode" => $result->DoctorOrderCode,
                    "DoctorOrderName" => $result->DoctorOrderName,
                    "DiagnosisCode" => $result->DiagnosisCode,
                    "DiagnosisName" => $result->DiagnosisName,
                    "IsCito" => $result->IsCito,
                    "ServiceUnitCode" => $result->ServiceUnitCode,
                    "ServiceUnitName" => $result->ServiceUnitName,
                    "GuarantorID" => $result->GuarantorID,
                    "GuarantorName" => $result->GuarantorName,
                    "AgreementID" => $result->AgreementID,
                    "AgreementName" => $result->AgreementName,
                    "ServiceClassCode" => $result->ServiceClassCode,
                    "ServiceClassName" => $result->ServiceClassName,
                    "WardRoomCode" => $result->WardRoomCode,
                    "WardRoomName" => $result->WardRoomName,
                    "BedCode" => $result->BedCode,
                    "BedName" => $result->BedName,
                    "ReceivedFlag" => $result->ReceivedFlag,
                    "LabRegNo" => $result->LabRegNo,
                    "ReceivedDatetime" => $result->ReceivedDatetime,
                    "OrderedItems" => @$data_items
                ];
            }else{
                $array_responses = [
                    'status' => [
                        'OK'        => false,
                        'Code'      => '-1',
                        'Messages'  => 'FAILED'
                    ]
                ];
            }

            // Commit Transaction
            DB::commit();

        } catch (\Exception $e) {

            $array_responses = [
                    'status' => [
                        'OK'        => false,
                        'Code'      => '-1',
                        'Messages'  => 'FAILED'
                    ]
                ];
            // Rollback Transaction
            DB::rollback();

        }        

        $data = [ $array_responses ];

        return response()->json($data[0]);
    }

    public function setreceivedorder(Request $request){

        $post = $request->input();

        DB::beginTransaction();

        try
        {

            $update = Lis::SetReceivedOrder($post);

            $get_last_updated_data = DB::table('order_data_lis')
                                    //->select('name', 'email as user_email')
                                    ->where('order_number', $post['orderNumber'])
                                    ->first();
            $get_last_updated_data->ReceivedFlag        = ($get_last_updated_data->ReceivedFlag == 'true' || $get_last_updated_data->ReceivedFlag == 1) ? 1 : 0;
            $get_last_updated_data->ReceivedDatetime    = str_replace('-', '/', $get_last_updated_data->ReceivedDatetime);

            $array_responses = [
                    'status' => [
                        'OK'        => true,
                        'Code'      => '-1',
                        'Messages'  => 'SUCCESS'
                    ],
                    "orderNumber" => $get_last_updated_data->order_number,
                    "LabRegNo" => $get_last_updated_data->LabRegNo,
                    "ReceivedFlag" => $get_last_updated_data->ReceivedFlag,
                    "ReceivedDatetime" => $get_last_updated_data->ReceivedDatetime
                ];

            // Commit Transaction
            DB::commit();

        } catch (\Exception $e) {

             $array_responses = [
                    'status' => [
                        'OK'        => false,
                        'Code'      => '-1',
                        'Messages'  => 'FAILED'
                    ]
                ];

            // Rollback Transaction
            DB::rollback();

        }    

        $data = [ $array_responses ];

        return response()->json($data[0]);

    }

    public function postitemsresult(Request $request){

        $post = $request->input();

        DB::beginTransaction();

        try
        {
            $update = Lis::PostItemsResult($post);

            $array_responses = [
                    'status' => [
                        'OK'        => true,
                        'Code'      => '-1',
                        'Messages'  => 'SUCCESS'
                    ]
                ];

            // Commit Transaction
            DB::commit();

        } catch (\Exception $e) {

             $array_responses = [
                    'status' => [
                        'OK'        => false,
                        'Code'      => '-1',
                        'Messages'  => 'FAILED'
                    ]
                ];

            // Rollback Transaction
            DB::rollback();

        }    

        $data = $array_responses;

        return response()->json($data);

    }
}
