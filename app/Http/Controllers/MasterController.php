<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Pasien;
use App\Model\Dokter;
use App\Model\Mstpelaksana;
use App\Model\Poli;
use App\Model\Ruangan;
use App\Model\Bed;

class MasterController extends Controller
{
    //PASIEN
    public function getPasienByNorm($norm){

        $pasien = Pasien::norm($norm);

        if(count($pasien) > 1){
            $data = ['metadata' => [ 'status' => 201, 'message' => 'Norm '.$pasien[0]->norm.' dimiliki oleh 2 Pasien. Harap menghubungi Bagminpasien RSPAD untuk verifikasi data.'],
                    'response' => ['data' => $pasien]];
            return response()->json($data);
        }

        if($pasien){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $pasien]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function getPasienByParams(Request $request){

        $arr = $request->input();

        $array_search = [];

        if(array_key_exists('id_pasien', $arr)){
            $array_search['mst_pasien.id_pasien'] = $arr['id_pasien']; 
        }

        if(array_key_exists('norm', $arr)){
            $array_search['mst_pasien.KODE'] = $arr['norm']; 
        }

        $pasien = Pasien::paramsearch($array_search);

        if(count($pasien) > 1){
            $data = ['metadata' => [ 'status' => 201, 'message' => 'Norm '.$pasien[0]->norm.' dimiliki oleh 2 Pasien. Harap menghubungi Bagminpasien RSPAD untuk verifikasi data.'],
                    'response' => ['data' => 'Terdapat duplikasi data NORM '.$pasien[0]->norm.', Harap menghubungi Bagminpasien']];
            return response()->json($data);
        }

        if($pasien){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $pasien]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    //DOKTER
    public function getDokter($val){

        $dokter = Dokter::Select("dokterID as id_dokter", DB::raw("CONCAT(title,' ',nama,', ',spesialisName) as nama_dokter"))
                    ->where('nama', 'like', '%'.$val.'%')
                    ->get()
                    ->toArray();

        if($dokter){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $dokter]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function getDokterByPost(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $dokter = Dokter::Select("dokterID as id_dokter", DB::raw("CONCAT(title,' ',nama,', ',spesialisName) as nama_dokter"))
                    ->whereIn('dokterID', $data['id_dokter'])
                    ->get()
                    ->toArray();

        if($dokter){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $dokter]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function getAllDokterByPost(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $dokter = Dokter::Select("dokterID as id_dokter", DB::raw("CONCAT(title,' ',nama,', ',spesialisName) as nama_dokter"))
                    //->whereIn('dokterID', $data['id_dokter'])
                    ->where('nama', 'like', '%'.$data['nama_dokter'].'%')
                    ->get()
                    ->toArray();

        if($dokter){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $dokter]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function getDataPelaksana(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        if(isset($data['unit'])){
            $condition['where'][] = ['field' => 'mst_pelaksana.unit', 'operator' => 'LIKE', 'value' => '%'.$data['unit'].'%'];
        }

        if(isset($data['group'])){
            $condition['where'][] = ['field' => 'mst_pelaksana.group', 'operator' => '=', 'value' => $data['group']];
        }

        if(isset($data['nama_pelaksana'])){
            $condition['where'][] = ['field' => 'mst_pelaksana.pelaksanaName', 'operator' => 'LIKE', 'value' => '%'.$data['nama_pelaksana'].'%'];
        }

        $dokter = Mstpelaksana::getDataPelaksana($condition);

        if($dokter){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $dokter]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    //PELAKSANA

    public function getPelaksana($nama = null, $group = null){

        $pelaksana = Mstpelaksana::Select("pelaksanaID as id_pelaksana", "pelaksanaName as nama_pelaksana", "group");

        if($nama !=  null){
            $pelaksana->where('pelaksanaName', 'like', '%'.$nama.'%');
        }

        if($group != null){
            $pelaksana->where('group', $group);
        }            

        $pelaksana = $pelaksana->get()->toArray();

        if($pelaksana){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $pelaksana]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    //RUANGAN

    public function getRuangan(){

        $ruangan = Ruangan::Select("ruanganID as id_ruangan", "ruanganName as nama_ruangan", "rs")
                    ->where('active', 1)->get()->toArray();

        if($ruangan){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $ruangan]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    //BED

    public function getBed(){

        $ruangan = Bed::getData();

        if($ruangan){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $ruangan]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    //POLI

    public function getPoli(){

        $poli = Poli::where('active', 1)
                ->get();

        if($poli){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $poli]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function getPoliByName($nama_poli){

        $poli = Poli::where('name', 'like', '%'.$nama_poli.'%')
                ->where('active', 1)
                ->get();

        if($poli){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $poli]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function getTarifPenunjangByIdPemeriksaan($id_tarif_pemeriksaan){

        $data = DB::connection()->select(
                "SELECT
                    emr_mst_pemeriksaan.id,
                    emr_mst_pemeriksaan.`name`,
                    emr_mst_pemeriksaan.`group`,
                    tarif.kelas,
                    tarif_harga.harga,
                    tarif_harga.tarifID as id_tarif,
                    tarif_harga.id as id_tarif_harga 
                FROM
                    emr_mst_pemeriksaan
                    LEFT JOIN (SELECT * FROM tarif WHERE active = '1') as tarif ON emr_mst_pemeriksaan.id = tarif.id_pemeriksaan
                    LEFT JOIN (SELECT * FROM tarif_harga WHERE `status` = '1' ) as tarif_harga ON tarif.tarifID = tarif_harga.tarifID 
                WHERE
                    tarif.id_buku_tarif = '02'
                AND emr_mst_pemeriksaan.id = '{$id_tarif_pemeriksaan}'"
                );

        if($data){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $data]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

    public function getTarifUTD(){

        $data = DB::connection()->select(
                "SELECT tarif.tarifID, tarif.namaTarif, tarif.kelas, tarif_harga.harga FROM (SELECT * FROM tarif WHERE tarifID in (
                        '02193200756',
                        '02193200757',
                        '02193200758',
                        '02193200759',
                        '02193200760',
                        '02193200751',
                        '02193200766',
                        '02193200771',
                        '02193200776',
                        '02193200781',
                        '02193200801',
                        '02193200861'
                        )) as tarif LEFT JOIN (SELECT * FROM `tarif_harga` WHERE `status` = '1') as tarif_harga ON tarif_harga.tarifID = tarif.tarifID"
                );

        if($data){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $data]];
        }else{
            $data = ['metadata' => [ 'status' => 500, 'message' => 'Failed']];
        }

        return response()->json($data);
    }

}
