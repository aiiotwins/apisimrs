<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Farmasi;

class FarmasiController extends Controller
{
    public function getStockObatGudangByName(Request $request){

        if(!empty($request->input())){
            $data = $request->input();
        }else{
            $data = json_decode(file_get_contents("php://input"), true);
        }   

        // DB::connection()->enableQueryLog();

        if(isset($data['fornas'])){
            $condition['where'][] = ['field' => 'tabelobat.ASKES', 'operator' => '=', 'value' => $data['fornas']];
        }

        if(isset($data['nama_obat'])){
            $condition['where'][] = ['field' => 'tabelobat.KET', 'operator' => 'LIKE', 'value' => '%'.$data['nama_obat'].'%'];
        }      

        $obat = Farmasi::getStockObatGudangByName($condition);

        if(count($obat) > 0){
            $data = ['metadata' => [ 'status' => 200, 'message' => 'OK'], 'response' => ['data' => $obat, 'message' => '' ]];
        }else{
            $data = ['metadata' => [ 'status' => 200, 'message' => 'data tidak ditemukan.']];
        }

        return response()->json($data);
    }
}
