<?php

namespace App\Http\Controllers;

use App\Helpers\HelperService;
use App\SkemaAntrian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Model\Mappingpolivclaim;
use App\Model\Mappingjnsbpjs;
use App\Model\Rjalan;
use App\Model\Monitoringrm;
use App\Model\Seppasien;
use App\Token;
use App\Antrian;
use Bpjs;

class HelfaController extends Controller
{
    public function cetaksep($nottoken)
    {
        date_default_timezone_set('Asia/Jakarta');

        //DB::enableQueryLog();

        $registrasi_online = DB::table('andro_regis')
                            ->select(
                                'mst_pasien.NAMA as nama_pasien',
                                'mst_pasien.KODE as norm',
                                'mst_pasien.id_pasien as id_pasien',
                                'mst_pasien.NOASKES as noaskes',
                                'mst_pasien.KELAMIN as jns_kelamin',
                                'mst_pasien.TGLAHIR as tgl_lahir',
                                'mst_pasien.TELP as telepon',
                                'andro_regis.id_poliklinik as id_poli',
                                'mst_golpas.golpasName',
                                'mst_poli.name as nama_poli',
                                'andro_regis.id_rs as id_rs',
                                'andro_regis.helfa_no_antrian as helfa_no_antrian',
                                'andro_regis.helfa_token as helfa_token',
                                'andro_regis.helfa_queue_number as helfa_queue_number',
                                'andro_regis.payment_method as payment_method',
                                'mapping_poli_vclaim.id_poli_bpjs as id_poli_vclaim')
                            ->leftJoin('mst_pasien', 'mst_pasien.id_pasien', '=', 'andro_regis.id_pasien')
                            ->leftJoin('mst_poli', 'mst_poli.poliid', '=', 'andro_regis.id_poliklinik')
                            ->leftJoin('mst_golpas', 'mst_golpas.golpasID', '=', 'mst_pasien.GOLPASKD')
                            ->leftJoin('mapping_poli_vclaim', 'mapping_poli_vclaim.id_poli_rspad', '=', 'mst_poli.poliid')
                            ->where('andro_regis.helfa_token', $nottoken)->first();

        if(!$registrasi_online){
            $data = [
                            'status' => 500,
                            'data' => '',
                            'message' => "Data tidak ditemukan"
                        ];

            return response()->json($data);
        }

        if($registrasi_online->payment_method == 'UMUM' || $registrasi_online->payment_method == 'umum' ){

            DB::beginTransaction();

            try
            {
                //SIMPAN DATA REGISTRASI WATLAN
                //SIAPKAN FORMAT DATA YANG AKAN DISIMPAN KE DB
                $data = [
                    'norm'              => $registrasi_online->norm,
                    'id_pasien'         => $registrasi_online->id_pasien,
                    'noAskes'           => $registrasi_online->noaskes,
                    'poliID'            => $registrasi_online->id_poli,
                    'tglReg'            => date('Y-m-d'),
                    'diagMasuk'         => '',
                    'perhatian'         => '',
                    'noSEP'             => '',
                    'jamReg'            => date('H:i:s'),
                    'userCreated'       => 'Anjungan Helfa',
                    'dateCreated'       => date('Y-m-d H:i:s'),
                    'status'            => 'open',
                    'jns_pembayaran'    => '28003',
                    'KODE_RS'           => $registrasi_online->id_rs,
                    'dokter'            => NULL,
                    'helfa_no_antrian'  => $registrasi_online->helfa_no_antrian,
                    'helfa_token'       => $registrasi_online->helfa_token,
                    'helfa_queue_number'=> $registrasi_online->helfa_queue_number
                ];

                $saveWatlan = Rjalan::saveWatlan($data);
                //END SIMPAN DATA REGISTRASI WATLAN

                //GET LAST COUNT KUNJUNGAN DI POLI TUJUAN
                $last_count_poli = Rjalan::lastCountKunjunganPoli($registrasi_online);

                //SIMPAN DATA MONITORING RM
                $data['noreg']      = $saveWatlan->noreg;
                $data['statusRM']   = 'request';
                $data['jns_rawat']  = 'RJ';
                $data['from']       = $registrasi_online->id_poli;
                $data['to']         = 'MP01';

                $saveMonitoringRM   = Monitoringrm::saveMonitoringrm($data);
                //END SIMPAN DATA MONITORING RM

                // Commit Transaction
                DB::commit();

                $data = [
                            'status' => 200,
                            'message' => "Berhasil",
                            'data' => [
                                        "sep_number" => '',
                                        "patient_name" => $registrasi_online->nama_pasien,
                                        "mrnumber" => $registrasi_online->norm,
                                        "sep_date" => '',
                                        "patient_birthdate" => $registrasi_online->tgl_lahir,
                                        "regnumber" => $saveWatlan->noreg,
                                        "instalasi_id" => null,
                                        "smf_id" => null,
                                        "faskes" => null,
                                        "referral_diagnosis" => '',
                                        "jenis_peserta" => '',
                                        "referral_cob" => null,
                                        "jenis_rawat" => "Rawat Jalan",
                                        "kelas_rawat" => '',
                                        "lakalantas" => null,
                                        "queue_number" => $registrasi_online->helfa_no_antrian,
                                        "policlinic_name" => $registrasi_online->nama_poli,
                                        "policlinic_id" => $registrasi_online->id_poli,
                                        "insurance_number" => '',
                                        "payment_method_id" => 'UMUM',
                                        "sub_polyclinic" => null,
                                        "visit_date" => date('Y-m-d'),
                                        "patient_id" => $registrasi_online->id_pasien,
                                        "referralnumber_bpjs" => '',
                                        "referral_number" => '',
                                        "doctor_name" => null,
                                        "start_time" => null,
                                        "end_time" => null,
                                        "gender" => $registrasi_online->jns_kelamin,
                                        "hospital_code" => $registrasi_online->id_rs,
                                        "phone_number" => $registrasi_online->telepon,
                                        "jns_pembayaran" => $registrasi_online->golpasName,
                                        "queue_number2" => $registrasi_online->helfa_queue_number
                                    ]
                        ];

                return response()->json($data);

            } catch (\Exception $e) {

                // Rollback Transaction
                DB::rollback();

            }

        }else{
            //CEK KEPESERTAAN
            $tglsep     =  date('Y-m-d');
            $param      = "Peserta/nokartu/{$registrasi_online->noaskes}/tglSEP/{$tglsep}";

            $peserta    = Bpjs::out(Bpjs::curl(env('URL_BPJS', 'forge') . $param, false, Bpjs::getHeader(true)));
            $return     = $peserta;

            if($return['response']['peserta']['statusPeserta']['keterangan'] == 'AKTIF'){
                //CEK RUJUKAN
                $paramppk = "Rujukan/List/Peserta/{$registrasi_online->noaskes}";
                $paramrs = "Rujukan/RS/List/Peserta/{$registrasi_online->noaskes}";
                $ppk = Bpjs::out(Bpjs::curl(env('URL_BPJS', 'forge') . $paramppk, false, Bpjs::getHeader(true)));

                if($ppk['metaData']['code'] == '201'){
                    $return = Bpjs::out(Bpjs::curl(env('URL_BPJS', 'forge') . $paramrs, false, Bpjs::getHeader(true)));
                    if($return['metaData']['code'] == '201'){
                        return $return;
                    }
                }else{
                    $return = $ppk;
                }
                //END CEK RUJUKAN

                //RUMUS MENGHITUNG SELISIH HARI 
                // $tgl1 = new DateTime("2019-11-01");
                // $tgl2 = new DateTime("2019-12-31");
                // $d = $tgl2->diff($tgl1)->days + 1;
                // echo $d." hari";
                
                $rujukan = $return;

                $rujukan['response']['rujukan'] = $return['response']['rujukan'][0];

                $diagMasuk = $rujukan['response']['rujukan']['diagnosa']['kode'];

                $return['norm'] = $registrasi_online->norm;

                //GET HISTORY PELAYANAN
                $getHistoryPelayanan = Bpjs::getHistoryPelayanan($registrasi_online->noaskes);

                $getHistoryPelayanan = $getHistoryPelayanan['response']['histori'];
                $arrayTemp           = array();

                if (is_array($getHistoryPelayanan) || is_object($getHistoryPelayanan)){
                    foreach ($getHistoryPelayanan as $key => $value) {
                        if($value['ppkPelayanan'] == 'RSPAD GATOT SUBROTO'){
                            if($value['noRujukan'] == $rujukan['response']['rujukan']['noKunjungan']){
                                array_push($arrayTemp, $value);
                            }
                        }
                    }
                }                
                //END GET HISTORY PELAYANAN

                //GET POLI KUNJUNGAN TERAKHIR UNTUK PENGAMBILAN DATA DPJP SEBELUMNYA
                $last_poli = (count($arrayTemp) == 0) ? $rujukan['response']['rujukan']['poliRujukan']['nama'] : ($arrayTemp[0]['poli'] == '') ? $rujukan['response']['rujukan']['poliRujukan']['nama'] : $arrayTemp[0]['poli'];

                $last_poli = str_replace('%20', ' ', $last_poli);
                $kunjungan_poli_akhir = Mappingpolivclaim::where('nama_poli', $last_poli)->first();

                $return['dpjp'] = $kunjungan_poli_akhir->id_poli_bpjs;

                //HITUNG KUNJUNGAN DI RSPAD DENGAN NO RUJUKAN TERSEBUT
                //JIKA LEBIH DARI SATU, MAKA TUJUAN POLI DIUBAH SESUAI ID_POLIKLINIK ANDRO REGIS
                //JIKA 0 MAKA AMBIL DATA POLI DARI RUJUKAN
                $rujukan['response']['rujukan']['poliRujukan']['kode'] = (count($arrayTemp) == 0) ? $rujukan['response']['rujukan']['poliRujukan']['kode'] : $registrasi_online->id_poli_vclaim;

                //CEK MONITORING REKAM MEDIS DAN KUNJUNGAN PASIEN
                //$checkMonitoring = $this->_check_monitoring_rm( $post->registrasi->norm );

                $array = ['a.norm' => $registrasi_online->norm, 'a.tglReg' => date('Y-m-d'), 'a.status' => 'open'];
                $checkKunjunganToday = Rjalan::cekRegistrasiWatlan( $array );

                if($checkKunjunganToday){
                    $data = [
                                'status' => 500,
                                'data' => '',
                                'message' => "Pasien Dengan NORM : ".$checkKunjunganToday->norm." Sudah mendapatkan pelayanan Rawat Jalan di Poliklinik : ".$checkKunjunganToday->poliklinik." Pada Tanggal ".date("d-m-Y", strtotime($checkKunjunganToday->tgl_registrasi))
                            ];
                    return response()->json($data);
                }

                $sep   = Bpjs::createSEP($return);

                $return = $sep;

                if($return['metaData']['code'] == 200){

                    DB::beginTransaction();

                    try
                    {
                        $jns_pembayaran = Mappingjnsbpjs::where('keterangan', $peserta['response']['peserta']['jenisPeserta']['keterangan'])->first();
                        $jns_pembayaran = ($jns_pembayaran) ? $jns_pembayaran->golpasid : '';

                        //SIMPAN DATA REGISTRASI WATLAN
                        //SIAPKAN FORMAT DATA YANG AKAN DISIMPAN KE DB
                        $data = [
                            'norm'              => $registrasi_online->norm,
                            'id_pasien'         => $registrasi_online->id_pasien,
                            'noAskes'           => $registrasi_online->noaskes,
                            'poliID'            => $registrasi_online->id_poli,
                            'tglReg'            => date('Y-m-d'),
                            'diagMasuk'         => $diagMasuk,
                            'perhatian'         => '',
                            'noSEP'             => $return['response']['sep']['noSep'],
                            'jamReg'            => date('H:i:s'),
                            'userCreated'       => 'Anjungan Helfa',
                            'dateCreated'       => date('Y-m-d H:i:s'),
                            'status'            => 'open',
                            'jns_pembayaran'    => $jns_pembayaran,
                            'KODE_RS'           => $registrasi_online->id_rs,
                            'dokter'            => NULL,
                            'helfa_no_antrian'  => $registrasi_online->helfa_no_antrian,
                            'helfa_token'       => $registrasi_online->helfa_token,
                            'helfa_queue_number'=> $registrasi_online->helfa_queue_number
                        ];

                        $saveWatlan = Rjalan::saveWatlan($data);
                        //END SIMPAN DATA REGISTRASI WATLAN

                        //SIMPAN DATA MONITORING RM
                        $data['noreg']      = $saveWatlan->noreg;
                        $data['statusRM']   = 'request';
                        $data['jns_rawat']  = 'RJ';
                        $data['from']       = $registrasi_online->id_poli;
                        $data['to']         = 'MP01';

                        $saveMonitoringRM   = Monitoringrm::saveMonitoringrm($data);
                        //END SIMPAN DATA MONITORING RM

                        //SAVE SEP PASIEN
                        $jnsPelayanan = ($sep['response']['sep']['jnsPelayanan'] == 'R.Jalan') ? 2 : 1;

                        $data = [
                            'noreg' => $saveWatlan->noreg,
                            'noSep'=> $sep['response']['sep']['noSep'],
                            'nik'=> $peserta['response']['peserta']['nik'],
                            'tglLahir'=> $peserta['response']['peserta']['tglLahir'],
                            'jnsKelamin'=> $peserta['response']['peserta']['sex'],
                            'noKartu'=> $peserta['response']['peserta']['noKartu'],
                            'tglCetakKartu'=> $peserta['response']['peserta']['tglCetakKartu'],
                            'jnsPesertaName'=> $peserta['response']['peserta']['jenisPeserta']['keterangan'],
                            'kelasName'=> $peserta['response']['peserta']['hakKelas']['keterangan'],
                            'cabangName'=> '',
                            'providerID'=> $peserta['response']['peserta']['provUmum']['kdProvider'],
                            'providerName'=> $peserta['response']['peserta']['provUmum']['nmProvider'],
                            'jnsPesertaID'=> $peserta['response']['peserta']['jenisPeserta']['kode'],
                            'kelasID'=> $peserta['response']['peserta']['hakKelas']['kode'],
                            'cabangID'=> '0901R003',
                            'tglRujukan'=> $rujukan['response']['rujukan']['tglKunjungan'],
                            'noRujukan'=> $rujukan['response']['rujukan']['noKunjungan'],
                            'asalRujukanID'=> $rujukan['response']['rujukan']['provPerujuk']['kode'],
                            'asalRujukanName'=> $rujukan['response']['rujukan']['provPerujuk']['nama'],
                            'diagnosaID'=> $rujukan['response']['rujukan']['diagnosa']['kode'],
                            'diagnosaName'=> $rujukan['response']['rujukan']['diagnosa']['nama'],
                            'catatan'=> '',
                            'tglSep'=> $sep['response']['sep']['tglSep'],
                            'jnsPelayanan'=> $jnsPelayanan,
                            'poliTujuan'=> $registrasi_online->id_poli,
                            'poliID'=> $registrasi_online->id_poli,
                            'poliName'=> $registrasi_online->nama_poli,
                            'nama'=> $registrasi_online->nama_pasien,
                            'norm'=> $registrasi_online->norm,
                            'id_pasien'=> $registrasi_online->id_pasien,
                            'kelasRawat'=> $peserta['response']['peserta']['hakKelas']['keterangan']
                        ];

                        $saveSEP = Seppasien::simpan($data);

                        //END SEP PASIEN

                        // Commit Transaction
                        DB::commit();

                        $data = [
                                    'status' => 200,
                                    'message' => "Berhasil",
                                    'data' => [
                                                "sep_number" => $sep['response']['sep']['noSep'],
                                                "patient_name" => $registrasi_online->nama_pasien,
                                                "mrnumber" => $registrasi_online->norm,
                                                "sep_date" => $sep['response']['sep']['tglSep'],
                                                "patient_birthdate" => $registrasi_online->tgl_lahir,
                                                "regnumber" => $saveWatlan->noreg,
                                                "instalasi_id" => null,
                                                "smf_id" => null,
                                                "faskes" => null,
                                                "referral_diagnosis" => $rujukan['response']['rujukan']['diagnosa']['nama'],
                                                "jenis_peserta" => $peserta['response']['peserta']['jenisPeserta']['keterangan'],
                                                "referral_cob" => null,
                                                "jenis_rawat" => "Rawat Jalan",
                                                "kelas_rawat" => $peserta['response']['peserta']['hakKelas']['keterangan'],
                                                "lakalantas" => null,
                                                "queue_number" => $registrasi_online->helfa_no_antrian,
                                                "policlinic_name" => $registrasi_online->nama_poli,
                                                "policlinic_id" => $registrasi_online->id_poli,
                                                "insurance_number" => $peserta['response']['peserta']['noKartu'],
                                                "payment_method_id" => 'BPJS',
                                                "sub_polyclinic" => null,
                                                "visit_date" => date('Y-m-d'),
                                                "patient_id" => $registrasi_online->id_pasien,
                                                "referralnumber_bpjs" => $rujukan['response']['rujukan']['noKunjungan'],
                                                "referral_number" => $rujukan['response']['rujukan']['noKunjungan'],
                                                "doctor_name" => null,
                                                "start_time" => null,
                                                "end_time" => null,
                                                "gender" => $registrasi_online->jns_kelamin,
                                                "hospital_code" => $registrasi_online->id_rs,
                                                "phone_number" => $registrasi_online->telepon,
                                                "jns_pembayaran" => $registrasi_online->golpasName,
                                                "queue_number2" => $registrasi_online->helfa_queue_number
                                            ]
                                ];

                        return response()->json($data);

                    } catch (\Exception $e) {

                        // Rollback Transaction
                        DB::rollback();

                    }
                }
            }else{
                $data = [
                            'status' => 500,
                            'data' => '',
                            'message' => $return['response']['peserta']['statusPeserta']['keterangan']
                        ];
                return response()->json($data);
            }
        }

        //END CEK KEPESERTAAN
        return $return;
    }

    public function generate(Request $request)
    {
        // var_dump($request->all());exit();
        // $length = $request->has('length') ? $request->length : 6;
        $length     = 6;
        $isNumeric  = true;

        if ($request->has('numeric')) {
            if ($request->numeric == 'true') {
                $isNumeric = true;
            }
        }

        // $model = Token::generate($length);
        $model = NULL;

        do {
            $newToken = HelperService::generateRandomString($length, $isNumeric);

            // check dulu sudah ada apa belom
            $model = Token::where('token', $newToken)->first();
        } while ($model != NULL);

        // save token baru
        date_default_timezone_set('Asia/Jakarta');
        $token = new Token;
        $token->token = $newToken;
        $token->created_date = date('Y-m-d H:i:s');

        if ($token->save()) {
            $data   = HelperService::_success();
            $data['data'] = $token;
        } else {
            $data   = HelperService::_internalServerError();
        }

        return response()->json($data, 200);
    }

    public function pickOne(Request $request)
    {
        // var_dump($request->all());exit();
        date_default_timezone_set('Asia/Jakarta');
        if (!$request->has('hospital_code') || !$request->has('polyclinic_id')) {
            $data   = HelperService::_internalServerError('Hospital or Polyclinic Not Found');
            return response()->json($data, 200);
        }
        if (!$request->has('visit_date')) {
            $data   = HelperService::_internalServerError('Required Visit Date');
            return response()->json($data, 200);
        }
        $visit_date = $request->input('visit_date');

        $withHour = false;
        if (!$request->has('start_hour') && $request->has('end_hour')) {
            $data   = HelperService::_internalServerError('Start Hour Not Found');
            return response()->json($data, 200);
        } elseif ($request->has('start_hour') && !$request->has('end_hour')) {
            $data   = HelperService::_internalServerError('End Hour Not Found');
            return response()->json($data, 200);
        } elseif ($request->has('start_hour') && $request->has('end_hour')) {
            $withHour = true;
        }

        $numbStart      = 1;
        $timeNow        = date('H:i:s');
        $hospital_code  = $request->hospital_code;
        $polyclinic_id  = $request->polyclinic_id;
        $doctor_id      = $request->has('doctor_id') ? $request->doctor_id : NULL;
        $bpjsType       = $request->input('bpjs_type');
        if ($withHour) {
            $start_hour = $request->start_hour;
            $end_hour   = $request->end_hour;
        }

        // check if antrian is has rule
        $skemaAntrian = SkemaAntrian::where('polyclinic_id',$polyclinic_id)
            ->where('bpjs_type', $bpjsType)
            ->orderBy('created_at', 'DESC')->first();

        // check dulu sudah ada apa belom yg sesuai sama param nya di hari yg sama
        // ambil yg terakhirnya
            $where = "  hospital_code = $hospital_code
                            AND
                        polyclinic_id = $polyclinic_id";
            if ($doctor_id != NULL) {
                $where .= " AND doctor_id = $doctor_id ";
            } else {
                $where .= " AND doctor_id is NULL";
            }
            if ($withHour) {
                $where .= " AND start_hour = '$start_hour' AND end_hour = '$end_hour' ";
            } else {
                $where .= " AND start_hour is NULL AND end_hour is NULL ";
            }

            $model = Antrian::whereDate('created_date', $visit_date)
                            ->whereRaw($where)
                            ->where('bpjs_type', $bpjsType)
                            ->orderBy('number', 'DESC')
                            ->first();

        // var_dump($model);exit();
        if (isset($model)) {
            $oldNumber = $model['number'];
            $numbStart = $oldNumber + 1;
        }

        if ($skemaAntrian) {
            $letter = empty($skemaAntrian->letter) ? '' : $skemaAntrian->letter;
            $queue_number = $letter . str_pad($numbStart,$skemaAntrian->numbering,'000',STR_PAD_LEFT);
        } else {
            $queue_number = $numbStart;
        }

        // save ke database
        $ant = new Antrian;
        $ant->number        = $numbStart;
        $ant->hospital_code = $hospital_code;
        $ant->polyclinic_id = $polyclinic_id;
        if ($doctor_id != NULL) {
            $ant->doctor_id = $doctor_id;
        }
        if ($withHour) {
            $ant->start_hour = $start_hour;
            $ant->end_hour = $end_hour;
        }
        $ant->created_date = $visit_date . ' ' . $timeNow;
        $ant->bpjs_type = $bpjsType;
        $ant->queue_number = $queue_number;
        $ant->save();

        $data = HelperService::_success();
        $data['data'] = [
            "number" => $numbStart,
            "queue_number" => $queue_number
        ];

        return response()->json($data, 200);
    }
}
