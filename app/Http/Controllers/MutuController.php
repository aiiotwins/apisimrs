<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MutuController extends Controller
{
    public function getLogin(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        //DB::connection('mysql3')->enableQueryLog();

        $select  = 'users.username,';
        $select .= 'users.full_name,';
        $select .= 'groups.`name`';

        $data = DB::connection('mysql3')->table('users')
        ->select(DB::raw($select))
        ->leftJoin('users_groups', 'users.id', '=', 'users_groups.user_id')
        ->leftJoin('groups', 'users_groups.group_id', '=', 'groups.id')
        ->where('users.username', $data['username'])
        ->where('users.password_mobile', md5($data['password']))
        ->get();

        if(count($data) > 0){
            $data = ['status' => 200, 'message' => 'OK', 'data' => $data[0]];
        }else{
            $data = ['status' => 500, 'message' => 'Failed', 'data' => 'Data Tidak ditemukan.'];
        }

        // print_r(DB::connection('mysql3')->getQueryLog());
        // exit;

        return response()->json($data);
    }

    public function sendMail(Request $request){

        $data = json_decode(file_get_contents("php://input"), true);

        $ch = curl_init();

        $arrayHeader[0] = "Content-Type: application/x‐www‐form‐urlencoded";
 
        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL, "http://192.168.2.31/monitorstatus/rekammedis/sendmail");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, $arrayHeader);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         
        // 3. execute and fetch the resulting HTML output
        $output = curl_exec($ch);

        $output = json_decode($output);

        // 4. free up the curl handle
        curl_close($ch);

        //DB::connection('mysql3')->enableQueryLog();

        if($output->code == 200){
            $data = ['status' => 200, 'message' => 'OK'];
        }else{
            $data = ['status' => 500, 'message' => 'Failed'];
        }

        // print_r(DB::connection('mysql3')->getQueryLog());
        // exit;

        return response()->json($data);
    }

    public function getListten(){

        //$arrayData = ['104','105','113','106','2','13','15','8','11','108','107','129','9','18'];

        //DB::connection('mysql3')->enableQueryLog();

        $select  = 'id,';
        $select .= 'nama';

        $data = DB::connection('mysql3')->table('m_indikatormutu')
        ->select(DB::raw($select))
        //->whereIn('id', $arrayData)
        ->where('aktif', 1)
        ->orderBy('nama', 'asc')
        ->get();

        if(count($data) > 0){
            $data = ['status' => 200, 'message' => 'OK', 'data' => $data];
        }else{
            $data = ['status' => 500, 'message' => 'Failed (Data tidak ditemukan/Tidak ada)', 'data' => NULL];
        }

        // print_r(DB::connection('mysql3')->getQueryLog());
        // exit;

        return response()->json($data);
    }

    public function getDataChartPerbulan($periode = null, $id_indikatormutu = null){

        $arrayDataTahunan = [
                                '139',
                                '140',
                                '141',
                                '142',
                                '143',
                                '144',
                                '145',
                                '146',
                                '147',
                                '148',
                                '149',
                                '150'
                            ];

        if(in_array($id_indikatormutu, $arrayDataTahunan)){

            if($id_indikatormutu == '139'){
                $arrayHasil = [['tanggal' => 'Jan', 'jumlah' => 2], ['tanggal' => 'Feb', 'jumlah' => 7], ['tanggal' => 'Mar', 'jumlah' => 108], ['tanggal' => 'Apr', 'jumlah' => 146],['tanggal' => 'Mei', 'jumlah' => 125],['tanggal' => 'Jun', 'jumlah' => 120],['tanggal' => 'Jul', 'jumlah' => 188],['tanggal' => 'Agu', 'jumlah' => 215],['tanggal' => 'Sep', 'jumlah' => 358],['tanggal' => 'Okt', 'jumlah' => 290],['tanggal' => 'Nov', 'jumlah' => 279],['tanggal' => 'Des', 'jumlah' => 463]];
            }elseif($id_indikatormutu == '140'){
                $arrayHasil = [['tanggal' => 'Jan', 'jumlah' => 0], ['tanggal' => 'Feb', 'jumlah' => 0], ['tanggal' => 'Mar', 'jumlah' => 0], ['tanggal' => 'Apr', 'jumlah' => 36],['tanggal' => 'Mei', 'jumlah' => 14],['tanggal' => 'Jun', 'jumlah' => 7],['tanggal' => 'Jul', 'jumlah' => 46],['tanggal' => 'Agu', 'jumlah' => 55],['tanggal' => 'Sep', 'jumlah' => 38],['tanggal' => 'Okt', 'jumlah' => 13],['tanggal' => 'Nov', 'jumlah' => 36],['tanggal' => 'Des', 'jumlah' => 78]];
            }elseif($id_indikatormutu == '141'){
                $arrayHasil = [['tanggal' => 'Jan', 'jumlah' => 2], ['tanggal' => 'Feb', 'jumlah' => 4], ['tanggal' => 'Mar', 'jumlah' => 38], ['tanggal' => 'Apr', 'jumlah' => 38],['tanggal' => 'Mei', 'jumlah' => 95],['tanggal' => 'Jun', 'jumlah' => 92],['tanggal' => 'Jul', 'jumlah' => 62],['tanggal' => 'Agu', 'jumlah' => 97],['tanggal' => 'Sep', 'jumlah' => 87],['tanggal' => 'Okt', 'jumlah' => 23],['tanggal' => 'Nov', 'jumlah' => 14],['tanggal' => 'Des', 'jumlah' => 4]];
            }elseif($id_indikatormutu == '142'){
                $arrayHasil = [['tanggal' => 'Jan', 'jumlah' => 0], ['tanggal' => 'Feb', 'jumlah' => 3], ['tanggal' => 'Mar', 'jumlah' => 70], ['tanggal' => 'Apr', 'jumlah' => 108],['tanggal' => 'Mei', 'jumlah' => 30],['tanggal' => 'Jun', 'jumlah' => 28],['tanggal' => 'Jul', 'jumlah' => 126],['tanggal' => 'Agu', 'jumlah' => 118],['tanggal' => 'Sep', 'jumlah' => 271],['tanggal' => 'Okt', 'jumlah' => 267],['tanggal' => 'Nov', 'jumlah' => 265],['tanggal' => 'Des', 'jumlah' => 459]];
            }elseif($id_indikatormutu == '143'){
                $arrayHasil = [['tanggal' => 'Jan', 'jumlah' => 0], ['tanggal' => 'Feb', 'jumlah' => 3], ['tanggal' => 'Mar', 'jumlah' => 70], ['tanggal' => 'Apr', 'jumlah' => 108],['tanggal' => 'Mei', 'jumlah' => 30],['tanggal' => 'Jun', 'jumlah' => 28],['tanggal' => 'Jul', 'jumlah' => 126],['tanggal' => 'Agu', 'jumlah' => 118],['tanggal' => 'Sep', 'jumlah' => 271],['tanggal' => 'Okt', 'jumlah' => 267],['tanggal' => 'Nov', 'jumlah' => 265],['tanggal' => 'Des', 'jumlah' => 459]];
            }elseif($id_indikatormutu == '144'){
                $arrayHasil = [['tanggal' => 'Jan', 'jumlah' => 0], ['tanggal' => 'Feb', 'jumlah' => 6], ['tanggal' => 'Mar', 'jumlah' => 66], ['tanggal' => 'Apr', 'jumlah' => 113],['tanggal' => 'Mei', 'jumlah' => 114],['tanggal' => 'Jun', 'jumlah' => 102],['tanggal' => 'Jul', 'jumlah' => 109],['tanggal' => 'Agu', 'jumlah' => 168],['tanggal' => 'Sep', 'jumlah' => 285],['tanggal' => 'Okt', 'jumlah' => 273],['tanggal' => 'Nov', 'jumlah' => 223],['tanggal' => 'Des', 'jumlah' => 338]];
            }elseif($id_indikatormutu == '145'){
                $arrayHasil = [['tanggal' => 'Jan', 'jumlah' => 0], ['tanggal' => 'Feb', 'jumlah' => 0], ['tanggal' => 'Mar', 'jumlah' => 14], ['tanggal' => 'Apr', 'jumlah' => 21],['tanggal' => 'Mei', 'jumlah' => 14],['tanggal' => 'Jun', 'jumlah' => 26],['tanggal' => 'Jul', 'jumlah' => 25],['tanggal' => 'Agu', 'jumlah' => 22],['tanggal' => 'Sep', 'jumlah' => 41],['tanggal' => 'Okt', 'jumlah' => 35],['tanggal' => 'Nov', 'jumlah' => 31],['tanggal' => 'Des', 'jumlah' => 42]];
            }elseif($id_indikatormutu == '146'){
                $arrayHasil = [['tanggal' => 'Jan', 'jumlah' => 0], ['tanggal' => 'Feb', 'jumlah' => 0], ['tanggal' => 'Mar', 'jumlah' => 0], ['tanggal' => 'Apr', 'jumlah' => 6],['tanggal' => 'Mei', 'jumlah' => 4],['tanggal' => 'Jun', 'jumlah' => 0],['tanggal' => 'Jul', 'jumlah' => 2],['tanggal' => 'Agu', 'jumlah' => 6],['tanggal' => 'Sep', 'jumlah' => 4],['tanggal' => 'Okt', 'jumlah' => 2],['tanggal' => 'Nov', 'jumlah' => 5],['tanggal' => 'Des', 'jumlah' => 11]];
            }elseif($id_indikatormutu == '147'){
                $arrayHasil = [['tanggal' => '7-28 hr', 'jumlah' => 2], ['tanggal' => '29hr < 1th', 'jumlah' => 1], ['tanggal' => '1-4 th', 'jumlah' => 8], ['tanggal' => '5-18 th', 'jumlah' => 5],['tanggal' => '19-40 th', 'jumlah' => 28],['tanggal' => '41-60 th', 'jumlah' => 142]];
            }elseif($id_indikatormutu == '148'){
                $arrayHasil = [['tanggal' => '> 60 th', 'jumlah' => 125]];
            }elseif($id_indikatormutu == '149'){
                $arrayHasil = [['tanggal' => 'Pria', 'jumlah' => 139], ['tanggal' => 'Wanita', 'jumlah' => 83]];
            }elseif($id_indikatormutu == '150'){
                $arrayHasil = [['tanggal' => 'Pria', 'jumlah' => 57], ['tanggal' => 'Wanita', 'jumlah' => 32]];
            }

            $data = ['status' => 200, 'message' => 'OK', 'type'=>'tahunan', 'data' => $arrayHasil];

            return response()->json($data);
        }

        $days = 0;

        if($periode){
            $mY = explode("-", $periode);
            $month = $mY[0];
            $year = $mY[1];
            $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        }

        if(strlen($mY[0]) == 1){
            $periode = "0".$mY[0]."-".$mY[1];
        }        
        
        $tgl_tran = date('Y-m');
        $header = array(
            'No.',
            'Nama Indikator Mutu'
        );
        $columns1 = "";
        $columns2 = "";
        $total = "";
        for ($i = 1; $i <= $days; $i++) {
            $num_day = str_pad($i, 2, "0", STR_PAD_LEFT);
            $columns1 .= " , ROUND(AVG(CASE WHEN b.h_".$num_day." IS NOT NULL THEN b.h_".$num_day." ELSE 0 END)) AS h_".$num_day;
            $columns2 .= " , SUM(ROUND(CASE WHEN date_format(trn_indikator.tgl_tran,'%d') = '{$num_day}' THEN IF(m_indikatormutu.group='PERSEN' , (trn_indikator.num / trn_indikator.denum) * 100 , IF(m_indikatormutu.nilai IS NULL ,( trn_indikator.num / trn_indikator.denum ), ( trn_indikator.num / trn_indikator.denum ) * m_indikatormutu.nilai)) ELSE 0 END)) AS h_".$num_day;
            $cell = array('data' => $num_day, 'style' => 'width: 10px');
            array_push($header, $cell);
        }

        //DB::connection('mysql3')->enableQueryLog();

        $data = DB::connection('mysql3')->select(
            "SELECT  @rank:=@rank+1 AS nourut, b.nama_indikatormutu
                {$columns1}
                FROM (
                        SELECT
                            m_indikatormutu.id AS id_indikatormutu,
                            m_indikator.id AS id_indikator,
                            m_indikatormutu.nama AS nama_indikatormutu,
                            m_indikatormutu.group,
                            m_indikatormutu.aktif AS status_indikatormutu,
                            m_indikator.stat AS status_indikator,
                            m_unit.id AS id_unit,
                            trn_indikator.tgl_tran,
                            trn_indikator.num,
                            trn_indikator.denum
                            {$columns2}
                        FROM
                            m_indikatormutu
                            LEFT JOIN ( SELECT * FROM m_indikator WHERE stat = 'Aktif' ) AS m_indikator ON m_indikator.id_indikatormutu = m_indikatormutu.id
                            LEFT JOIN ( SELECT * FROM trn_indikator WHERE date_format( tgl_tran, '%m-%Y' ) = '{$periode}' ) AS trn_indikator ON trn_indikator.indikator_id = m_indikator.id
                            LEFT JOIN ( SELECT * FROM m_unit WHERE stat = 'Aktif' ) AS m_unit ON m_unit.id = m_indikator.unit_id 
                        WHERE
                            m_indikatormutu.aktif = 1 
                        GROUP BY
                            m_indikator.id 
                        ORDER BY
                            m_indikatormutu.id
                    ) as b                
                    CROSS JOIN (SELECT @rank:=0) AS nos
                    WHERE b.id_indikatormutu = '{$id_indikatormutu}'
                    GROUP BY b.id_indikatormutu
                "
            );

        $arrayHasil = [];
        $i = 1;
        foreach ($data as $key => $value) {
            foreach ($value as $key2 => $value2) {
                if(($key2 != 'nourut') && ($key2 != 'nama_indikatormutu')){
                    //$newArray = ['$id' => $i, 'Jumlah' => $value2, 'Tanggal' => substr ($key2, -2)." ".substr(date("F", mktime(0, 0, 0, $month, 10)),0,3)." ".$year, 'TotalDay' => $days];
                    $newArray = ['tanggal' => substr ($key2, -2), 'jumlah' => (int)$value2];
                    array_push($arrayHasil, $newArray);
                    $i++;
                }
            }            
        }

        if(count($data) > 0){
            $data = ['status' => 200, 'message' => 'OK', 'type' => 'bulanan', 'data' => $arrayHasil];
        }else{
            $data = ['status' => 500, 'message' => 'Failed (Data tidak ditemukan/Tidak ada)', 'data' => NULL];
        }

        // print_r(DB::connection('mysql3')->getQueryLog());
        // exit;

        return response()->json($data);
    }
}
