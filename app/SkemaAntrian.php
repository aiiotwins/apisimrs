<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 12/10/2020
 * Time: 17.39
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class SkemaAntrian extends Model
{
    protected $table = 'helfa_antrian_skema';
    protected $primaryKey = 'id';
}
