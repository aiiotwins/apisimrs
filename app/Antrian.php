<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\HelperService;

class Antrian extends Model
{
    protected $table = 'helfa_antrian';
    protected $primaryKey = 'id';

    public $timestamps = false;
}
