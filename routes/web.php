<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['prefix' => 'api', /*'middleware' => 'authsimrs'*/], function() use ($app){

	//API DATA MASTER
	$app->group(['prefix' => 'master'], function() use ($app){

		$app->group(['prefix' => 'pasien'], function() use ($app){

			//METHOD GET
			$app->get('norm/{norm}', [
			    'uses' => 'MasterController@getPasienByNorm'
			]);

			$app->post('paramsearch', [
			    'uses' => 'MasterController@getPasienByParams'
			]);

		});

		$app->group(['prefix' => 'poli'], function() use ($app){

			//METHOD GET
			$app->get('/all', [
			    'uses' => 'MasterController@getPoli'
			]);

			$app->get('/{nama_poli}', [
			    'uses' => 'MasterController@getPoliByName'
			]);

		});

		$app->group(['prefix' => 'dokter'], function() use ($app){
			//METHOD GET
			$app->get('/all/{val}', [
			    'uses' => 'MasterController@getDokter'
			]);

			//METHOD POST
			$app->post('post/all', [
			    'uses' => 'MasterController@getAllDokterByPost'
			]);

	        $app->post('post', [
			    'uses' => 'MasterController@getDokterByPost'
			]);

		});

		$app->group(['prefix' => 'pelaksana'], function() use ($app){
			//METHOD GET
			$app->get('/data[/{nama}[/{group}]]', [
			    'uses' => 'MasterController@getPelaksana'
			]);

			//METHOD POST
			$app->post('getdata', [
			    'uses' => 'MasterController@getDataPelaksana'
			]);
		});

		$app->group(['prefix' => 'ruangan'], function() use ($app){
			//METHOD GET
			$app->get('/data', [
			    'uses' => 'MasterController@getRuangan'
			]);
		});

		$app->group(['prefix' => 'bed'], function() use ($app){
			//METHOD GET
			$app->get('/data', [
			    'uses' => 'MasterController@getBed'
			]);
		});

		$app->group(['prefix' => 'tarif'], function() use ($app){
			//METHOD GET
			$app->get('/penunjang/{id_tarif_pemeriksaan}', [
			    'uses' => 'MasterController@getTarifPenunjangByIdPemeriksaan'
			]);

			$app->get('/utd', [
			    'uses' => 'MasterController@getTarifUTD'
			]);
		});

		$app->group(['prefix' => 'farmasi'], function() use ($app){
			//METHOD POST
			$app->post('/stok/gudang', [
			    'uses' => 'FarmasiController@getStockObatGudangByName'
			]);
		});

	});

	//API DATA REGISTRASI
	$app->group(['prefix' => 'registrasi'], function() use ($app){

		//DETAIL REGISTRASI
		$app->get('detail/{noreg}', [
		    'uses' => 'RegistrasiController@detail'
		]);

		//LIST REGISTRASI
		$app->get('listreg/{jns_rawat}/{tgl_reg}', [
		    'uses' => 'RegistrasiController@listreg'
		]);

		//LIST NORM
		$app->get('listbynorm/{jns_rawat}/{norm}', [
		    'uses' => 'RegistrasiController@listbynorm'
		]);

		//GET HISTORY PASIEN WATNAP/WATLAN
		$app->get('gethistory/{jns_rawat}/{norm}/{daritgl}/{sampaitgl}', [
		    'uses' => 'RegistrasiController@getHistory'
		]);

		//GET HISTORY REGISTRASI PASIEN ONLINE
		$app->get('gethistory/online/{norm}', [
		    'uses' => 'RegistrasiController@getHistoryOnline'
		]);

		$app->get('bydate/gethistory/online/{daritgl}/{sampaitgl}', [
		    'uses' => 'RegistrasiController@getHistoryOnlineByDate'
		]);

		$app->post('pencarian', [
		    'uses' => 'RegistrasiController@pencarian'
		]);

		$app->put('data/update', [
		    'uses' => 'RegistrasiController@dataupdate'
		]);

	});

	//API DATA EMR
	$app->group(['prefix' => 'emr'], function() use ($app){

		//METHOD GET
		$app->get('pemeriksaan', [
		    'uses' => 'EmrController@getTindakanPemeriksaan'
		]);

		$app->get('periksapenunjang[/{cat}[/{nama}]]', [
		    'uses' => 'EmrController@getPemeriksaanPenunjang'
		]);

		$app->get('gettoken/{token}', [
		    'uses' => 'EmrController@getToken'
		]);

		$app->get('pelaksana/{type}/{nama_pelaksana}', [
		    'uses' => 'EmrController@getPelaksana'
		]);

		$app->post('reqPemeriksaan', [
		    'uses' => 'EmrController@reqPemeriksaan'
		]);

		$app->group(['prefix' => 'farmasi'], function() use ($app){

			$app->get('resep/{noreg}', [
			    'uses' => 'EmrController@getResep'
			]);

			$app->post('resep/hapus', [
		    	'uses' => 'EmrController@hapusResep'
			]);

		});

		//GET SCAN RM
		$app->group(['prefix' => 'scan'], function() use ($app){
			$app->get('data/{norm}/{tgl_lahir}', [
			    'uses' => 'EmrController@getScan'
			]);
		});
		//END GET SCAN RM
	});

	//API DATA BPJS
	$app->group(['prefix' => 'bpjs'], function() use ($app){

		//METHOD GET
		$app->get('cekkepesertaan/{noaskes}', [
		    'uses' => 'BpjsController@cekpeserta'
		]);

		$app->get('getpoli/{namapoli}', [
		    'uses' => 'BpjsController@getpoli'
		]);

		$app->get('gethistorybpjs/{nokartu}', [
		    'uses' => 'BpjsController@gethistorybpjs'
		]);

	});

	//API UNTUK HELFA
	$app->group(['prefix' => 'helfa'], function() use ($app){

		//METHOD GET
		$app->get('cetaksep/{notoken}', [
		    'uses' => 'HelfaController@cetaksep'
		]);

		$app->get('token', [
		    'uses' => 'HelfaController@generate'
		]);

        //METHOD POST
        $app->post('get-number', [
		    'uses' => 'HelfaController@pickOne'
		]);

	});
	//END API UNTUK HELFA

});

$app->group(['prefix' => 'emr'], function() use ($app){

	$app->get('farmasi', [
	    'uses' => 'EmrController@cekTokenLinkFarmasi'
	]);

	$app->group(['prefix' => 'lis'], function() use ($app){

		$app->post('GetOutstandingList', [
		    'uses' => 'LisController@getoutstandinglist'
		]);

		$app->post('GetOrderDetail', [
		    'uses' => 'LisController@getorderdetail'
		]);

		$app->post('SetReceivedOrder', [
		    'uses' => 'LisController@setreceivedorder'
		]);

		$app->post('PostItemsResult', [
		    'uses' => 'LisController@postitemsresult'
		]);		

	});

});

$app->group(['prefix' => 'api', 'middleware' => 'authsimmutu'], function() use ($app){

	//API DATA MASTER
	$app->group(['prefix' => 'mutu'], function() use ($app){

		$app->group(['prefix' => 'user'], function() use ($app){

			$app->post('login', [
			    'uses' => 'MutuController@getLogin'
			]);

		});

		$app->post('postkomen', [
			    'uses' => 'MutuController@sendMail'
			]);

		$app->get('listten', [
		    'uses' => 'MutuController@getListten'
		]);

		$app->get('datachartbulanan/{periode}/{id_indikatormutu}', [
		    'uses' => 'MutuController@getDataChartPerbulan'
		]);

	});

});

$app->group(['prefix' => 'api', 'middleware' => 'authasiyap'], function() use ($app){

	$app->group(['prefix' => 'asiyap'], function() use ($app){

		$app->group(['prefix' => 'user'], function() use ($app){

			$app->post('login', [
			    'uses' => 'AsiyapController@getLogin'
			]);

		});
		$app->get('keluarga/{nip}', [
		    'uses' => 'AsiyapController@keluarga'
		]);


		$app->group(['prefix' => 'personil'], function() use ($app){

			$app->get('/all[/{nama}]', [
			    'uses' => 'AsiyapController@allPersonil'
			]);

			$app->get('/{nip}', [
			    'uses' => 'AsiyapController@personil'
			]);

		});

		$app->get('kepangkatan/{nip}', [
		    'uses' => 'AsiyapController@kepangkatan'
		]);		
		$app->get('kepegawaian/{nip}', [
		    'uses' => 'AsiyapController@kepegawaian'
		]);
		$app->get('riwayatjabatan/{nip}', [
		    'uses' => 'AsiyapController@riwayatjabatan'
		]);
		$app->get('riwayatpendidikan/{nip}', [
		    'uses' => 'AsiyapController@riwayatpendidikan'
		]);

		$app->group(['prefix' => 'home'], function() use ($app){

			$app->get('/all', [
			    'uses' => 'AsiyapController@getHome'
			]);

			$app->get('/{id_home}', [
			    'uses' => 'AsiyapController@getHomeDetail'
			]);

		});

		$app->get('detailperawatan', [
		    'uses' => 'AsiyapController@detailperawatan'
		]);
		$app->get('detailperawatanlainnya', [
		    'uses' => 'AsiyapController@detailperawatanlainnya'
		]);

	});

});

$app->group(['prefix' => 'jkn'], function() use ($app){
	
	$app->group(['prefix' => 'mobile', 'middleware' => 'authbpjswsv2'], function() use ($app){

			$app->get('token', [
			    'uses' => 'Bpjswsv2Controller@getToken'
			]);

			$app->post('statusantrian', [
			    'uses' => 'Bpjswsv2Controller@getStatusAntrian'
			]);

			$app->post('ambilantrian', [
			    'uses' => 'Bpjswsv2Controller@getAmbilAntrian'
			]);

			$app->post('sisaantrian', [
			    'uses' => 'Bpjswsv2Controller@getSisaAntrian'
			]);

			$app->post('batalantrian', [
			    'uses' => 'Bpjswsv2Controller@getBatalAntrian'
			]);

			$app->post('checkin', [
			    'uses' => 'Bpjswsv2Controller@getCheckin'
			]);

			$app->post('infopasienbaru', [
			    'uses' => 'Bpjswsv2Controller@getInfoPasienBaru'
			]);

			$app->group(['prefix' => 'jadwal-operasi'], function() use ($app){

				$app->post('rs', [
				    'uses' => 'Bpjswsv2Controller@getJadwalOperasiRS'
				]);

				$app->post('pasien', [
				    'uses' => 'Bpjswsv2Controller@getJadwalOperasiPasien'
				]);

			});

	});

	$app->group(['prefix' => 'rs'], function() use ($app){

		$app->group(['prefix' => 'ref'], function() use ($app){

				$app->get('poli', [
				    'uses' => 'Bpjswsv2Controller@getRefPoli'
				]);

				$app->get('dokter', [
				    'uses' => 'Bpjswsv2Controller@getRefDokter'
				]);

				$app->get('jadwal-dokter', [
				    'uses' => 'Bpjswsv2Controller@getJadwalDokter'
				]);
		});

		$app->post('update/jadwal-dokter', [
		    'uses' => 'Bpjswsv2Controller@updateJadwalDokter'
		]);

		$app->post('tambah-antrian', [
		    'uses' => 'Bpjswsv2Controller@tambahAntrian'
		]);

		$app->post('update-waktu-antrian', [
		    'uses' => 'Bpjswsv2Controller@updateWaktuAntrian'
		]);

		$app->post('batal-antrian', [
		    'uses' => 'Bpjswsv2Controller@batalAntrian'
		]);

		$app->post('list-task', [
		    'uses' => 'Bpjswsv2Controller@getListTask'
		]);

		$app->get('dashboard-day/{tgl}/{waktu}', [
		    'uses' => 'Bpjswsv2Controller@dashboardDay'
		]);

		$app->get('dashboard-month/{bulan}/{tahun}/{waktu}', [
		    'uses' => 'Bpjswsv2Controller@dashboardMonth'
		]);

	});
	
});

$app->get('getdata', [
	'middleware' => 'authjwt',
    'uses' => 'RegistrasiController@getData'
]);

$app->get('cekpeserta/{noaskes}', [
    'uses' => 'BpjsController@cekpeserta'
]);

$app->get('cekrujukan/{noaskes}', [
    'uses' => 'BpjsController@cekrujukan'
]);

$app->get('eis', [
    'uses' => 'EisController@postKamar'
]);

$app->get('key', function() {
    return str_random(32);
});
